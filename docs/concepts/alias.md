Alias는 데이터를 직렬화와 역직렬화(deserialization)할 때 사용되는 필드의 다른 이름이다.

다음과 같은 방법으로 alias를 지정할 수 있다.

- [`Field`](https://docs.pydantic.dev/latest/api/fields/#pydantic.fields.Field)의 `alias`
- [`Field`](https://docs.pydantic.dev/latest/api/fields/#pydantic.fields.Field)의 `validation_alias`
- [`Field`](https://docs.pydantic.dev/latest/api/fields/#pydantic.fields.Field)의 `serialization_alias`
- [`Config`](https://docs.pydantic.dev/latest/api/config/#pydantic.config.ConfigDict.alias_generator)의 `alias_generator`

## Alias Precedence
[`Field`](https://docs.pydantic.dev/latest/api/fields/#pydantic.fields.Field)에 Field를 지정하면 기본적으로 생성된 alias보다 해당 alias가 우선한다.

```python
from pydantic import BaseModel, ConfigDict, Field


def to_camel(string: str) -> str:
    return ''.join(word.capitalize() for word in string.split('_'))


class Voice(BaseModel):
    model_config = ConfigDict(alias_generator=to_camel)

    name: str
    language_code: str = Field(alias='lang')


voice = Voice(Name='Filiz', lang='tr-TR')
print(voice.language_code)
#> tr-TR
print(voice.model_dump(by_alias=True))
#> {'Name': 'Filiz', 'lang': 'tr-TR'}
```

### Alias Priority
이 동작을 변경하려면 필드에 `alias_priority`를 설정하면 된다.

- `alias_priority=2`이면 alias 생성기가 alias를 재정의하지 않는다.
- `alias_priority==1`이면 alias 생성기가 alias를 재정의한다.
- `alias_priority`를 설정하지 않으면 alias 생성기가 alias를 재정의한다.

`validation-alias`와 `serialization_alias`에도 동일한 우선순위가 적용된다. [field aliases](./field.md#필드-aliases)에서 다양한 필드 alias에 대해 자세히 알아보시오.
