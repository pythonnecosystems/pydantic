가능한 경우 Pydantic은 [표준 라이브러리 타입](https://docs.pydantic.dev/latest/api/standard_library_types/)을 사용하여 필드를 정의하므로 학습 곡선을 원활하게 한다. 그러나 많은 유용한 어플리케이션의 경우 표준 라이브러리 타입이 존재하지 않으므로 Pydantic은 일반적으로 사용되는 많은 타입을 구현하였다.

[Pydantic 추가 타입](https://github.com/pydantic/pydantic-extra-types) 패키지에서 찾을 수 있는 더 복잡한 타입도 있다.

목적에 맞는 기존 타입이 없는 커스텀 속성과 유효성 검사를 사용하여 [자체 Pydantic 호환 타입](https://docs.pydantic.dev/latest/concepts/types/#custom-types)을 구현할 수도 있다.

다음 섹션에서는 Pydantic에서 지원하는 타입에 대해 설명한다.

- [표준 라이브러리 타입](https://docs.pydantic.dev/latest/api/standard_library_types/) - Python 표준 라이브러리의 타입이다.
- [엄격한 타입](#엄격한-타입) - 호환되는 타입을 강제로 방지할 수 있는 타입이다.
- [커스텀 데이터 타입](#커스텀-타입) - 사용자가 정의하는 데이터 타입을 생성한다.
- [필드 타입 변환](./conversion-table.md) - 서로 다른 필드 타입 간의 엄격하고 느슨한 변환.

## 타입 변환
유효성 검사 중에 Pydantic은 데이터를 예상 타입으로 강제 변환할 수 있다.

강제 모드에는 엄격한 모드와 느슨한 모드가 있다. Pydantic이 엄격한 모드와 느슨한 모드에서 데이터를 변환하는 방법에 대한 자세한 내용은 [변환 표](./conversion-table.md)를 참조하시오.

엄격한 강제 변환를 활성화하는 방법에 대한 자세한 내용은 [엄격한 모드](./strict-mode.md)와 [엄격한 타입](#엄격한-타입)을 참조하시오.

## 엄격한 타입
Pydantic은 다음과 같은 엄격한 타입을 제공한다.

- [`StrictBool`](https://docs.pydantic.dev/latest/api/types/#pydantic.types.StrictBool)
- [`StrictBytes`](https://docs.pydantic.dev/latest/api/types/#pydantic.types.StrictBytes)
- [`StrictFloat`](https://docs.pydantic.dev/latest/api/types/#pydantic.types.StrictFloat)
- [`StrictInt`](https://docs.pydantic.dev/latest/api/types/#pydantic.types.StrictInt)
- [`StrictStr`](https://docs.pydantic.dev/latest/api/types/#pydantic.types.StrictStr)

이러한 타입은 유효성 검사된 값이 해당 타입이거나 해당 타입의 하위 타입인 경우에만 유효성 검사를 통과한다.

### 제약된 타입 (Constrained types)
이 동작은 제약된 타입의 `strict` 필드를 통해서도 노출되며 여러 가지 복잡한 유효성 검사 규칙과 결합될 수 있습다. 지원되는 인자에 대한 개별 유형 서명을 참조하시오.

- [`conbytes()`](https://docs.pydantic.dev/latest/api/types/#pydantic.types.conbytes)
- [`condate()`](https://docs.pydantic.dev/latest/api/types/#pydantic.types.condate)
- [`condecimal()`](https://docs.pydantic.dev/latest/api/types/#pydantic.types.condecimal)
- [`confloat()`](https://docs.pydantic.dev/latest/api/types/#pydantic.types.confloat)
- [`confrozenset()`](https://docs.pydantic.dev/latest/api/types/#pydantic.types.confrozenset)
- [`conint()`](https://docs.pydantic.dev/latest/api/types/#pydantic.types.conint)
- [`conlist()`](https://docs.pydantic.dev/latest/api/types/#pydantic.types.conlist)
- [`conset()`](https://docs.pydantic.dev/latest/api/types/#pydantic.types.conset)
- [`constr()`](https://docs.pydantic.dev/latest/api/types/#pydantic.types.constr)

다음과 같은 사항에 주의하여야 한다. 

- `StrictBytes`(와 c`onbytes()`의 `strict` 옵션)는 `bytes`과 `bytearray` 타입 모두 허용한다.
- `StrictInt`(와 `conint()`의 `strict` 옵션)는 Python에서 `bool`이 `int`의 서브클래스임에도 불구하고 `bool` 타입을 허용하지 않는다. 다른 서브클래스에서는 작동한다.
- `StrictFloat`(와 `confloat()`의 `strict` 옵션)는 `int`를 허용하지 않는다.

위의 것 외에도 유한한 값(즉, `inf`, `-inf` 또는 `nan`이 아닌)만 허용하는 [`FiniteFloat`](https://docs.pydantic.dev/latest/api/types/#pydantic.types.FiniteFloat) 타입을 가질 수도 있다.

## 커스텀 타입
커스텀 데이터 타입을 직접 정의할 수도 있다. 이를 달성하는 여러 방법이 있다.

### `Annotated`를 통한 타입 작성
[PEP 593](https://peps.python.org/pep-0593/)에서는 타입 검사기가 타입을 해석하는 방식을 변경하지 않고 런타임 메타데이터를 타입에 첨부하는 방법으로 Annotated를 도입했다. Pydantic은 이를 활용하여 유형 검사기에 관한 한 원본 타입과 동일하지만 유효성 검사를 추가하고 직렬화 방식을 달리하는 등의 타입을 생성할 수 있다.

예를 들어 양수 `int`를 나타내는 타입을 만들 수 있다.

```python
# or `from typing import Annotated` for Python 3.9+
from typing_extensions import Annotated

from pydantic import Field, TypeAdapter, ValidationError

PositiveInt = Annotated[int, Field(gt=0)]

ta = TypeAdapter(PositiveInt)

print(ta.validate_python(1))
#> 1

try:
    ta.validate_python(-1)
except ValidationError as exc:
    print(exc)
    """
    1 validation error for constrained-int
      Input should be greater than 0 [type=greater_than, input_value=-1, input_type=int]
    """
```

[`annotated-types`](https://github.com/annotated-types/annotated-types)의 제약 조건을 사용하여 Pydantic에 구애받지 않게 만들 수도 있다.

```python
from annotated_types import Gt
from typing_extensions import Annotated

from pydantic import TypeAdapter, ValidationError

PositiveInt = Annotated[int, Gt(0)]

ta = TypeAdapter(PositiveInt)

print(ta.validate_python(1))
#> 1

try:
    ta.validate_python(-1)
except ValidationError as exc:
    print(exc)
    """
    1 validation error for constrained-int
      Input should be greater than 0 [type=greater_than, input_value=-1, input_type=int]
    """
```

#### 유효성 검사와 직렬화 추가
Pydantic이 내보내는 마커를 사용하여 유효성 검사, 직렬화와 JSON 스키마를 임의의 타입에 추가하거나 재정의할 수 있다.

```python
from typing_extensions import Annotated

from pydantic import (
    AfterValidator,
    PlainSerializer,
    TypeAdapter,
    WithJsonSchema,
)

TruncatedFloat = Annotated[
    float,
    AfterValidator(lambda x: round(x, 1)),
    PlainSerializer(lambda x: f'{x:.1e}', return_type=str),
    WithJsonSchema({'type': 'string'}, mode='serialization'),
]


ta = TypeAdapter(TruncatedFloat)

input = 1.02345
assert input != 1.0

assert ta.validate_python(input) == 1.0

assert ta.dump_json(input) == b'"1.0e+00"'

assert ta.json_schema(mode='validation') == {'type': 'number'}
assert ta.json_schema(mode='serialization') == {'type': 'string'}
```

#### 제네릭
`Annoated` 내에서 타입 변수를 사용하여 타입을 재사용 가능한 방식으로 수정할 수 있다.

```python
from typing import Any, List, Sequence, TypeVar

from annotated_types import Gt, Len
from typing_extensions import Annotated

from pydantic import ValidationError
from pydantic.type_adapter import TypeAdapter

SequenceType = TypeVar('SequenceType', bound=Sequence[Any])


ShortSequence = Annotated[SequenceType, Len(max_length=10)]


ta = TypeAdapter(ShortSequence[List[int]])

v = ta.validate_python([1, 2, 3, 4, 5])
assert v == [1, 2, 3, 4, 5]

try:
    ta.validate_python([1] * 100)
except ValidationError as exc:
    print(exc)
    """
    1 validation error for list[int]
      List should have at most 10 items after validation, not 100 [type=too_long, input_value=[1, 1, 1, 1, 1, 1, 1, 1, ... 1, 1, 1, 1, 1, 1, 1, 1], input_type=list]
    """


T = TypeVar('T')  # or a bound=SupportGt

PositiveList = List[Annotated[T, Gt(0)]]

ta = TypeAdapter(PositiveList[float])

v = ta.validate_python([1])
assert type(v[0]) is float


try:
    ta.validate_python([-1])
except ValidationError as exc:
    print(exc)
    """
    1 validation error for list[constrained-float]
    0
      Input should be greater than 0 [type=greater_than, input_value=-1, input_type=int]
    """
```

### 명명된 타입 aliases
위의 예에서는 암시적 타입 alias을 사용한다. 즉, JSON 스키마는 `title`을 가질 수 없으며 필드 간에 스키마가 복사된다. [typing-extensions](https://github.com/python/typing_extensions) 백포트를 통해 [PEP 695](https://peps.python.org/pep-0695/)의 `TypeAliasType`을 사용하여 명명된 alias을 만들 수 있으므로 하위 클래스를 만들지 않고도 새 타입을 정의할 수 있다. 이 새 타입은 이름처럼 단순할 수도 있고 복잡한 유효성 검사 로직이 첨부될 수도 있다.

```python
from typing import List, TypeVar

from annotated_types import Gt
from typing_extensions import Annotated, TypeAliasType

from pydantic import BaseModel

T = TypeVar('T')  # or a `bound=SupportGt`

ImplicitAliasPositiveIntList = List[Annotated[int, Gt(0)]]


class Model1(BaseModel):
    x: ImplicitAliasPositiveIntList
    y: ImplicitAliasPositiveIntList


print(Model1.model_json_schema())
"""
{
    'properties': {
        'x': {
            'items': {'exclusiveMinimum': 0, 'type': 'integer'},
            'title': 'X',
            'type': 'array',
        },
        'y': {
            'items': {'exclusiveMinimum': 0, 'type': 'integer'},
            'title': 'Y',
            'type': 'array',
        },
    },
    'required': ['x', 'y'],
    'title': 'Model1',
    'type': 'object',
}
"""

PositiveIntList = TypeAliasType('PositiveIntList', List[Annotated[int, Gt(0)]])


class Model2(BaseModel):
    x: PositiveIntList
    y: PositiveIntList


print(Model2.model_json_schema())
"""
{
    '$defs': {
        'PositiveIntList': {
            'items': {'exclusiveMinimum': 0, 'type': 'integer'},
            'type': 'array',
        }
    },
    'properties': {
        'x': {'$ref': '#/$defs/PositiveIntList'},
        'y': {'$ref': '#/$defs/PositiveIntList'},
    },
    'required': ['x', 'y'],
    'title': 'Model2',
    'type': 'object',
}
"""
```

이러한 명명된 타입 alias는 제네릭일 수도 있다.

```python
from typing import Generic, List, TypeVar

from annotated_types import Gt
from typing_extensions import Annotated, TypeAliasType

from pydantic import BaseModel, ValidationError

T = TypeVar('T')  # or a bound=SupportGt

PositiveList = TypeAliasType(
    'PositiveList', List[Annotated[T, Gt(0)]], type_params=(T,)
)


class Model(BaseModel, Generic[T]):
    x: PositiveList[T]


assert Model[int].model_validate_json('{"x": ["1"]}').x == [1]

try:
    Model[int](x=[-1])
except ValidationError as exc:
    print(exc)
    """
    1 validation error for Model[int]
    x.0
      Input should be greater than 0 [type=greater_than, input_value=-1, input_type=int]
    """
```

### 명명된 타입 alias
`TypeAliasType`을 사용하여 재귀 타입을 만들 수도 있다.

```python
from typing import Any, Dict, List, Union

from pydantic_core import PydanticCustomError
from typing_extensions import Annotated, TypeAliasType

from pydantic import (
    TypeAdapter,
    ValidationError,
    ValidationInfo,
    ValidatorFunctionWrapHandler,
    WrapValidator,
)


def json_custom_error_validator(
    value: Any, handler: ValidatorFunctionWrapHandler, _info: ValidationInfo
) -> Any:
    """Simplify the error message to avoid a gross error stemming
    from exhaustive checking of all union options.
    """
    try:
        return handler(value)
    except ValidationError:
        raise PydanticCustomError(
            'invalid_json',
            'Input is not valid json',
        )


Json = TypeAliasType(
    'Json',
    Annotated[
        Union[Dict[str, 'Json'], List['Json'], str, int, float, bool, None],
        WrapValidator(json_custom_error_validator),
    ],
)


ta = TypeAdapter(Json)

v = ta.validate_python({'x': [1], 'y': {'z': True}})
assert v == {'x': [1], 'y': {'z': True}}

try:
    ta.validate_python({'x': object()})
except ValidationError as exc:
    print(exc)
    """
    1 validation error for function-wrap[json_custom_error_validator()]
      Input is not valid json [type=invalid_json, input_value={'x': <object object at 0x0123456789ab>}, input_type=dict]
    """
```

### `__get_pydantic_core_schema__`로 유효성 검사 커스터마이징
Pydantic이 커스텀 클래스를 처리하는 방법을 보다 광범위하게 커스마이징하려면, 특히 클래스에 대한 액세스 권한이 있거나 클래스를 서브클래싱할 수 있는 경우, 특수 `__get_pydantic_core_schema__`를 구현하여 Pydantic에 `pydantic-core` 스키마를 생성하는 방법을 알려줄 수 있다.

pydantic은 유효성 검사와 직렬화를 처리하기 위해 내부적으로 `pydantic-core`를 사용하지만, Pydantic V2의 새로운 API이므로 향후 조정될 가능성이 가장 높은 영역 중 하나이며 `annotated-types`, `pydantic.Field` 또는 `BeforeValidator` 등에서 제공하는 것과 같은 내장 구성을 고수해야 한다.

커스텀 타입과 `Annotated`에 넣으려는 메타데이터 모두에 `__get_pydantic_core_schema__`를 구현할 수 있다. 두 경우 모두 API는 미들웨어와 유사하며 "랩" 유효성 검사기와 유사하다. `source-type`(특히 제네릭의 경우 클래스와 반드시 같을 필요는 없음)과 한 타입으로 호출할 수 있는 `handler`를 가져와 `Annotated`에서 다음 메타데이터를 호출하거나 Pydantic의 내부 스키마 생성으로 호출할 수 있다.

가장 간단한 no-op 구현은 주어진 타입으로 handler를 호출한 다음 이를 결과로 반환한다. handler를 호출하기 전에 타입을 수정하거나, handler가 반환하는 핵심 스키마를 수정하거나, handler를 전혀 호출하지 않도록 선택할 수도 있다.

#### 커스텀 타입의 메소드로
다음은 `__get_pydantic_core_schema__`를 사용하여 유효성 검사 방법을 커스터마이징하는 타입의 예이다. 이는 Pydantic V1에서 `__get_validators__`를 구현하는 것과 동일하다.

```python
from typing import Any

from pydantic_core import CoreSchema, core_schema

from pydantic import GetCoreSchemaHandler, TypeAdapter


class Username(str):
    @classmethod
    def __get_pydantic_core_schema__(
        cls, source_type: Any, handler: GetCoreSchemaHandler
    ) -> CoreSchema:
        return core_schema.no_info_after_validator_function(cls, handler(str))


ta = TypeAdapter(Username)
res = ta.validate_python('abc')
assert isinstance(res, Username)
assert res == 'abc'
```

JSON 스키마를 커스터마이징하는 방법에 대한 자세한 내용은 [JSON 스키마](https://docs.pydantic.dev/latest/concepts/json_schema/)를 참조하시오.

#### 어노테이션으로
종종 제네릭 타입 매개변수 이상으로 커스텀 타입을 매개변수화하고 싶을 때가 있다(이는 타입 시스템을 통해 할 수 있으며 나중에 설명하겠다). 또는 실제로는 서브클래스의 인스턴스를 만드는 데 신경 쓰지 않고 몇 가지 추가 유효성 검사만 수행한 원래 타입을 원할 수도 있다.

예를 들어, `pydantic.AfterValidator`([유효성 검사와 직렬화 추가하기](#유효성-검사와-직렬화-추가) 참조)를 직접 구현한다면 다음과 비슷한 작업을 수행할 수 있다.

```python
from dataclasses import dataclass
from typing import Any, Callable

from pydantic_core import CoreSchema, core_schema
from typing_extensions import Annotated

from pydantic import BaseModel, GetCoreSchemaHandler


@dataclass
class MyAfterValidator:
    func: Callable[[Any], Any]

    def __get_pydantic_core_schema__(
        self, source_type: Any, handler: GetCoreSchemaHandler
    ) -> CoreSchema:
        return core_schema.no_info_after_validator_function(
            self.func, handler(source_type)
        )


Username = Annotated[str, MyAfterValidator(str.lower)]


class Model(BaseModel):
    name: Username


assert Model(name='ABC').name == 'abc'
```

타입 검사기는 `Username`을 `str`과 별개의 타입으로 간주하지 않기 때문에 이전 예에서처럼 `Username`에 `'abc'`를 할당하는 것에 대해 불평하지 않는다.

#### 서드 파티 타입 처리
이전 섹션의 패턴에 대한 또 다른 사용 사례는 서드 파티 타입을 처리하는 것이다.

```python
from typing import Any

from pydantic_core import core_schema
from typing_extensions import Annotated

from pydantic import (
    BaseModel,
    GetCoreSchemaHandler,
    GetJsonSchemaHandler,
    ValidationError,
)
from pydantic.json_schema import JsonSchemaValue


class ThirdPartyType:
    """
    This is meant to represent a type from a third-party library that wasn't designed with Pydantic
    integration in mind, and so doesn't have a `pydantic_core.CoreSchema` or anything.
    """

    x: int

    def __init__(self):
        self.x = 0


class _ThirdPartyTypePydanticAnnotation:
    @classmethod
    def __get_pydantic_core_schema__(
        cls,
        _source_type: Any,
        _handler: GetCoreSchemaHandler,
    ) -> core_schema.CoreSchema:
        """
        We return a pydantic_core.CoreSchema that behaves in the following ways:

        * ints will be parsed as `ThirdPartyType` instances with the int as the x attribute
        * `ThirdPartyType` instances will be parsed as `ThirdPartyType` instances without any changes
        * Nothing else will pass validation
        * Serialization will always return just an int
        """

        def validate_from_int(value: int) -> ThirdPartyType:
            result = ThirdPartyType()
            result.x = value
            return result

        from_int_schema = core_schema.chain_schema(
            [
                core_schema.int_schema(),
                core_schema.no_info_plain_validator_function(validate_from_int),
            ]
        )

        return core_schema.json_or_python_schema(
            json_schema=from_int_schema,
            python_schema=core_schema.union_schema(
                [
                    # check if it's an instance first before doing any further work
                    core_schema.is_instance_schema(ThirdPartyType),
                    from_int_schema,
                ]
            ),
            serialization=core_schema.plain_serializer_function_ser_schema(
                lambda instance: instance.x
            ),
        )

    @classmethod
    def __get_pydantic_json_schema__(
        cls, _core_schema: core_schema.CoreSchema, handler: GetJsonSchemaHandler
    ) -> JsonSchemaValue:
        # Use the same schema that would be used for `int`
        return handler(core_schema.int_schema())


# We now create an `Annotated` wrapper that we'll use as the annotation for fields on `BaseModel`s, etc.
PydanticThirdPartyType = Annotated[
    ThirdPartyType, _ThirdPartyTypePydanticAnnotation
]


# Create a model class that uses this annotation as a field
class Model(BaseModel):
    third_party_type: PydanticThirdPartyType


# Demonstrate that this field is handled correctly, that ints are parsed into `ThirdPartyType`, and that
# these instances are also "dumped" directly into ints as expected.
m_int = Model(third_party_type=1)
assert isinstance(m_int.third_party_type, ThirdPartyType)
assert m_int.third_party_type.x == 1
assert m_int.model_dump() == {'third_party_type': 1}

# Do the same thing where an instance of ThirdPartyType is passed in
instance = ThirdPartyType()
assert instance.x == 0
instance.x = 10

m_instance = Model(third_party_type=instance)
assert isinstance(m_instance.third_party_type, ThirdPartyType)
assert m_instance.third_party_type.x == 10
assert m_instance.model_dump() == {'third_party_type': 10}

# Demonstrate that validation errors are raised as expected for invalid inputs
try:
    Model(third_party_type='a')
except ValidationError as e:
    print(e)
    """
    2 validation errors for Model
    third_party_type.is-instance[ThirdPartyType]
      Input should be an instance of ThirdPartyType [type=is_instance_of, input_value='a', input_type=str]
    third_party_type.chain[int,function-plain[validate_from_int()]]
      Input should be a valid integer, unable to parse string as an integer [type=int_parsing, input_value='a', input_type=str]
    """


assert Model.model_json_schema() == {
    'properties': {
        'third_party_type': {'title': 'Third Party Type', 'type': 'integer'}
    },
    'required': ['third_party_type'],
    'title': 'Model',
    'type': 'object',
}
```

이 접근 방식을 사용하여 예를 들어 Pandas나 Numpy 타입의 동작을 정의할 수 있다.

#### GetPydanticSchema를 사용하여 상용구 줄이기
마커 클래스를 생성하는 위의 예제에서 상용구가 상당히 많이 필요하다는 것을 알 수 있다. 대부분의 간단한 경우 `pydantic.GetPydanticSchema`를 사용하면 이를 크게 최소화할 수 있다.

```python
from pydantic_core import core_schema
from typing_extensions import Annotated

from pydantic import BaseModel, GetPydanticSchema


class Model(BaseModel):
    y: Annotated[
        str,
        GetPydanticSchema(
            lambda tp, handler: core_schema.no_info_after_validator_function(
                lambda x: x * 2, handler(tp)
            )
        ),
    ]


assert Model(y='ab').y == 'abab'
```

#### 요약
요약해 보면 ...

1. Pydantic은 `AfterValidator`와 `Field`같은 `Annotated`을 통해 타입을 커스터마이징할 수 있는 높은 수준의 훅을 제공gks다. 가능하면 이들을 사용하시오.
2. 내부적으로는 `pydantic-core`를 사용하여 유효성 검사를 커스터마이징하며, `__get_pydantic_core_schema__`가 포함된 `GetPydanticSchema` 또는 마커 클래스를 사용하여 직접 후크할 수 있다.
3. 커스텀 타입을 정말로 원한다면 타입 자체에 `__get_pydantic_core_schema__`를 구현할 수 있다.

### 커스텀 제네릭 클라스 처리

> **Warning**
>
> 이것은 처음에는 필요하지 않을 수도 있는 고급 기술이다. 대부분의 경우 표준 Pydantic 모델로도 충분할 것이다. 따라서 이후는 향 후 필요 따라 편역을 하도록 하겠다.
