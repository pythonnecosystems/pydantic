Pydantic의 동작은 [`BaseModel.model_config`](https://docs.pydantic.dev/latest/api/base_model/#pydantic.main.BaseModel.model_config)를 통해, 그리고 [`TypeAdapter`](https://docs.pydantic.dev/latest/api/type_adapter/#pydantic.type_adapter.TypeAdapter)의 인수로 제어할 수 있다.

> **Note**
>
> **v2.0** 이전에는 `Config` 클래스가 사용되었습니다. 이 클래스는 여전히 지원되지만 **더 이상 사용되지 않는다**.

```python
from pydantic import BaseModel, ConfigDict, ValidationError


class Model(BaseModel):
    model_config = ConfigDict(str_max_length=10)

    v: str


try:
    m = Model(v='x' * 20)
except ValidationError as e:
    print(e)
    """
    1 validation error for Model
    v
      String should have at most 10 characters [type=string_too_long, input_value='xxxxxxxxxxxxxxxxxxxx', input_type=str]
    """
```

또한 구성 옵션을 모델 클래스 kwargs로 지정할 수도 있다.

```python
from pydantic import BaseModel, ValidationError


class Model(BaseModel, extra='forbid'):  # See the Extra Attributes section for more details.
    a: str


try:
    Model(a='spam', b='oh no')
except ValidationError as e:
    print(e)
    """
    1 validation error for Model
    b
      Extra inputs are not permitted [type=extra_forbidden, input_value='oh no', input_type=str]
    """
```

마찬가지로 Pydantic의 `@dataclass` 데코레이터를 사용하는 경우에도 마찬가지이다.

```python
from datetime import datetime

from pydantic import ConfigDict, ValidationError
from pydantic.dataclasses import dataclass

config = ConfigDict(str_max_length=10, validate_assignment=True)


@dataclass(config=config)  
class User:
    id: int
    name: str = 'John Doe'
    signup_ts: datetime = None


user = User(id='42', signup_ts='2032-06-21T12:00')
try:
    user.name = 'x' * 20
except ValidationError as e:
    print(e)
    """
    1 validation error for User
    name
      String should have at most 10 characters [type=string_too_long, input_value='xxxxxxxxxxxxxxxxxxxx', input_type=str]
    """
```

> **Note**
>
> 표준 라이브러리 또는 `TypedDict`의 `dataclass`를 사용하는 경우, 대신 `__pydantic_config__`를 사용해야 한다. 참조:
>
> ```python
> from dataclasses import dataclass
> from datetime import datetime
> 
> from pydantic import ConfigDict
> 
> 
> @dataclass
> class User:
>     __pydantic_config__ = ConfigDict(strict=True)
> 
>     id: int
>     name: str = 'John Doe'
>     signup_ts: datetime = None
> ```

## 전연적 동작 변화
Pydantic의 동작을 전역적으로 변경하려는 경우, 구성이 상속되므로 커스텀 `model_config`를 사용하여 커스텀 `BaseModel`을 만들 수 있다.

```python
from pydantic import BaseModel, ConfigDict


class Parent(BaseModel):
    model_config = ConfigDict(extra='allow')


class Model(Parent):
    x: str


m = Model(x='foo', y='bar')
print(m.model_dump())
#> {'x': 'foo', 'y': 'bar'}
```

`Model` 클래스에 `model_config`를 추가하면 `Parent` 클래스의 `model_config`와 병합된다.

```python
from pydantic import BaseModel, ConfigDict


class Parent(BaseModel):
    model_config = ConfigDict(extra='allow')


class Model(Parent):
    model_config = ConfigDict(str_to_lower=True)  # (1)!

    x: str


m = Model(x='FOO', y='bar')
print(m.model_dump())
#> {'x': 'foo', 'y': 'bar'}
print(m.model_config)
#> {'extra': 'allow', 'str_to_lower': True}
```
