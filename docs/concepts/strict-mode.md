기본적으로 Pydantic은 가능한 경우 값을 원하는 유형으로 강제로 변환하려고 시도한다. 예를 들어, 문자열 `"123"`을 `int` 필드에 입력으로 전달하면 `123`으로 변환된다. 이러한 강제 동작은 UUID, URL 매개변수, HTTP 헤더, 환경 변수, 사용자 입력 등 많은 시나리오에서 유용하다.

그러나 이것이 바람직하지 않은 상황도 있으며, 데이터를 강제로 변환하는 대신 오류를 발생시키고자 하는 경우도 있다.

이러한 사용 사례를 더 잘 지원하기 위해 Pydantic은 모델별, 필드별 또는 유효성 검사 호출별로 활성화할 수 있는 "엄격 모드"를 제공한다. 엄격 모드를 활성화하면 Pydantic은 데이터를 강제로 변환할 때 훨씬 덜 관대해지며, 대신 데이터가 올바른 타입이 아닌 경우 오류를 발생시킨다.

다음은 엄격 모드와 기본/"느슨한(lax)" 모드에서의 유효성 검사 동작의 차이를 보여주는 간단한 예이다.

```python
from pydantic import BaseModel, ValidationError


class MyModel(BaseModel):
    x: int


print(MyModel.model_validate({'x': '123'}))  # lax mode
#> x=123

try:
    MyModel.model_validate({'x': '123'}, strict=True)  # strict mode
except ValidationError as exc:
    print(exc)
    """
    1 validation error for MyModel
    x
      Input should be a valid integer [type=int_type, input_value='123', input_type=str]
    """
```

Pydantic을 사용하면서 엄격 모드 유효성 검사를 수행하는 방법에는 여러 가지가 있으며, 아래에서 자세히 설명하겠다.
- [`strict=True`를 `BaseModel.model_validate`, `TypeAdapter.validate_python`과 JSON의 경우 유사한 유효성 검사 메서드에 전달](#메서드-호출에서-엄격-모드) 
- `BaseModel`, `dataclass` 또는 `TypedDict`의 필드에 [`Field(strict=True)` 사용](#field에서-엄격-모드) 
- [`pydantic.types.Strict`를 필드에서 타입 어노테이션으로 사용](#annotated-strict에서-엄격-모드)
- Pydantic은 `pydantic.types.StrictInt`처럼 이미 `Strict`로 어노테이션된 일부 타입 alias를 제공
- [`ConfigDict(strict=True)` 사용](#configdict에서-엄격-모드)

## 엄격 모드에서 타입 강요
대부분의 유형에 대해, 엄격 모드로 Python의 데이터 유효성을 검사할 때 정확한 타입의 인스턴스만 허용된다. 예를 들어 `int` 필드의 유효성을 검사할 때는 `int`의 인스턴스만 허용되며, `float` 또는 `str`의 인스턴스를 전달하면 `ValidationError`가 발생한다.

엄격 모드에서 JSON의 데이터 유효성을 검사할 때는 더 느슨하다. 예를 들어, `UUID` 필드의 유효성을 검사할 때 JSON에서 유효성을 검사할 때는 `str`의 인스턴스가 허용되지만 Python에서는 허용되지 않는다.

```python
import json
from uuid import UUID

from pydantic import BaseModel, ValidationError


class MyModel(BaseModel):
    guid: UUID


data = {'guid': '12345678-1234-1234-1234-123456789012'}

print(MyModel.model_validate(data))  # OK: lax
#> guid=UUID('12345678-1234-1234-1234-123456789012')

print(
    MyModel.model_validate_json(json.dumps(data), strict=True)
)  # OK: strict, but from json
#> guid=UUID('12345678-1234-1234-1234-123456789012')

try:
    MyModel.model_validate(data, strict=True)  # Not OK: strict, from python
except ValidationError as exc:
    print(exc.errors(include_url=False))
    """
    [
        {
            'type': 'is_instance_of',
            'loc': ('guid',),
            'msg': 'Input should be an instance of UUID',
            'input': '12345678-1234-1234-1234-123456789012',
            'ctx': {'class': 'UUID'},
        }
    ]
    """
```

엄격 모드에서 입력으로 허용되는 타입에 대한 자세한 내용은 [변환 테이블](./conversion-table.md)를 참조한다.

## 메서드 호출에서 엄격 모드
지금까지 모든 예제는 유효성 검사 메서드에 대한 키워드 인수로 `strict=True`를 사용하여 엄격 모드 유효성 검사를 수행한다. `BaseModel.model_validate`에 대해 이 방법을 보였지만 이는 `TypeAdapter`를 사용하여 임의의 타입에서도 작동한다.

```python
from pydantic import TypeAdapter, ValidationError

print(TypeAdapter(bool).validate_python('yes'))  # OK: lax
#> True

try:
    TypeAdapter(bool).validate_python('yes', strict=True)  # Not OK: strict
except ValidationError as exc:
    print(exc)
    """
    1 validation error for bool
      Input should be a valid boolean [type=bool_type, input_value='yes', input_type=str]
    """
```

`TypeAdapter`에서 보다 "복잡한" 타입을 사용하는 경우에도 이 기능이 작동된다.

```python
from dataclasses import dataclass

from pydantic import TypeAdapter, ValidationError


@dataclass
class MyDataclass:
    x: int


try:
    TypeAdapter(MyDataclass).validate_python({'x': '123'}, strict=True)
except ValidationError as exc:
    print(exc)
    """
    1 validation error for MyDataclass
      Input should be an instance of MyDataclass [type=dataclass_exact_type, input_value={'x': '123'}, input_type=dict]
    """
```

이는 `TypeAdapter.validate_json`과 `BaseModel.model_validate_json` 메서드에서도 작동한다.

```python
import json
from typing import List
from uuid import UUID

from pydantic import BaseModel, TypeAdapter, ValidationError

try:
    TypeAdapter(List[int]).validate_json('["1", 2, "3"]', strict=True)
except ValidationError as exc:
    print(exc)
    """
    2 validation errors for list[int]
    0
      Input should be a valid integer [type=int_type, input_value='1', input_type=str]
    2
      Input should be a valid integer [type=int_type, input_value='3', input_type=str]
    """


class Model(BaseModel):
    x: int
    y: UUID


data = {'x': '1', 'y': '12345678-1234-1234-1234-123456789012'}
try:
    Model.model_validate(data, strict=True)
except ValidationError as exc:
    # Neither x nor y are valid in strict mode from python:
    print(exc)
    """
    2 validation errors for Model
    x
      Input should be a valid integer [type=int_type, input_value='1', input_type=str]
    y
      Input should be an instance of UUID [type=is_instance_of, input_value='12345678-1234-1234-1234-123456789012', input_type=str]
    """

json_data = json.dumps(data)
try:
    Model.model_validate_json(json_data, strict=True)
except ValidationError as exc:
    # From JSON, x is still not valid in strict mode, but y is:
    print(exc)
    """
    1 validation error for Model
    x
      Input should be a valid integer [type=int_type, input_value='1', input_type=str]
    """
```

## `Field`에서 엄격 모드
모델의 개별 필드에 대해 [필드에 `strict=True`를 설정](https://docs.pydantic.dev/latest/api/fields/#pydantic.fields.Field)할 수 있다. 이렇게 하면 `strict=True` 없이 유효성 검사 메서드를 호출하는 경우에도 해당 필드에 대해 엄격 모드 유효성 검사를 수행한다.

`strict=True`가 설정된 필드만 영향을 받는다.

```python
from pydantic import BaseModel, Field, ValidationError


class User(BaseModel):
    name: str
    age: int
    n_pets: int


user = User(name='John', age='42', n_pets='1')
print(user)
#> name='John' age=42 n_pets=1


class AnotherUser(BaseModel):
    name: str
    age: int = Field(strict=True)
    n_pets: int


try:
    anotheruser = AnotherUser(name='John', age='42', n_pets='1')
except ValidationError as e:
    print(e)
    """
    1 validation error for AnotherUser
    age
      Input should be a valid integer [type=int_type, input_value='42', input_type=str]
    """
```

필드를 엄격하게 설정하면 모델 클래스를 인스턴스화할 때 수행되는 유효성 검사에도 영향을 미친다.

```python
from pydantic import BaseModel, Field, ValidationError


class Model(BaseModel):
    x: int = Field(strict=True)
    y: int = Field(strict=False)


try:
    Model(x='1', y='2')
except ValidationError as exc:
    print(exc)
    """
    1 validation error for Model
    x
      Input should be a valid integer [type=int_type, input_value='1', input_type=str]
    """
```

### 어노테이션으로 `Field` 사용
필요한 경우 (또는 다른 키워드 인자와 함께) `Field(strict=True)`를 어노테이션으로 사용할 수 있다 (예: `TypedDict`로 작업할 때).

```python
from typing_extensions import Annotated, TypedDict

from pydantic import Field, TypeAdapter, ValidationError


class MyDict(TypedDict):
    x: Annotated[int, Field(strict=True)]


try:
    TypeAdapter(MyDict).validate_python({'x': '1'})
except ValidationError as exc:
    print(exc)
    """
    1 validation error for typed-dict
    x
      Input should be a valid integer [type=int_type, input_value='1', input_type=str]
    """
```

## `Annotated[..., Strict()]`에서 엄격 모드
또한 Pydantic은 `typing.Annotated` 클래스와 함께 메타데이터로 사용하기 위한 [`Strict`](https://docs.pydantic.dev/latest/api/types/#pydantic.types.Strict) 클래스를 제공하며, 이 어노테이션은 어노테이트된 필드가 엄격 모드로 유효성을 검사해야 함을 나타낸다.

```python
from typing_extensions import Annotated

from pydantic import BaseModel, Strict, ValidationError


class User(BaseModel):
    name: str
    age: int
    is_active: Annotated[bool, Strict()]


User(name='David', age=33, is_active=True)
try:
    User(name='David', age=33, is_active='True')
except ValidationError as exc:
    print(exc)
    """
    1 validation error for User
    is_active
      Input should be a valid boolean [type=bool_type, input_value='True', input_type=str]
    """
```

실제로 이는 [`StrictInt`](https://docs.pydantic.dev/latest/api/types/#pydantic.types.StrictInt)와 같이 Pydantic에서 제공하는 일부 엄격한 기본 제공 타입을 구현하는 데 사용되는 방법이다.

## `ConfigDict`에서 엄격 모드

### `BaseModel`
복잡한 입력 타입의 모든 필드에 대해 엄격 모드를 사용하려면 `model_config`에서 [`ConfigDict(strict=True)`](https://docs.pydantic.dev/latest/api/config/#pydantic.config.ConfigDict)를 사용하면 된다.

```python
from pydantic import BaseModel, ConfigDict, ValidationError


class User(BaseModel):
    model_config = ConfigDict(strict=True)

    name: str
    age: int
    is_active: bool


try:
    User(name='David', age='33', is_active='yes')
except ValidationError as exc:
    print(exc)
    """
    2 validation errors for User
    age
      Input should be a valid integer [type=int_type, input_value='33', input_type=str]
    is_active
      Input should be a valid boolean [type=bool_type, input_value='yes', input_type=str]
    """
```

> **Note**
>
> 모델의 `model_config`를 통해 `strict=True`를 사용하는 경우에도 개별 필드에 `strict=False`를 설정하여 개별 필드의 엄격성을 재정의(override)할 수 있다.
>
> ```python
> from pydantic import BaseModel, ConfigDict, Field
>
>
> class User(BaseModel):
>     model_config = ConfigDict(strict=True)
> 
>     name: str
>     age: int = Field(strict=False)
> ```

중첩된 모델 필드에는 엄격 모드가 재귀적으로 적용되지 않는다는 점에 유의하시오.

```python
from pydantic import BaseModel, ConfigDict, ValidationError


class Inner(BaseModel):
    y: int


class Outer(BaseModel):
    model_config = ConfigDict(strict=True)

    x: int
    inner: Inner


print(Outer(x=1, inner=Inner(y='2')))
#> x=1 inner=Inner(y=2)

try:
    Outer(x='1', inner=Inner(y='2'))
except ValidationError as exc:
    print(exc)
    """
    1 validation error for Outer
    x
      Input should be a valid integer [type=int_type, input_value='1', input_type=str]
    """
```
(dataclasses와 `TypedDict`도 마찬가지이다.)

이것이 바람직하지 않은 경우 관련된 모든 타입에 대해 엄격 모드가 활성화되어 있는지 확인해야 한다. 예를 들어 모델 클래스의 경우 `model_config = ConfigDict(strict=True)`와 함께 공유 기본 클래스를 사용하여 이 작업을 수행할 수 있다.

```python
from pydantic import BaseModel, ConfigDict, ValidationError


class MyBaseModel(BaseModel):
    model_config = ConfigDict(strict=True)


class Inner(MyBaseModel):
    y: int


class Outer(MyBaseModel):
    x: int
    inner: Inner


try:
    Outer.model_validate({'x': 1, 'inner': {'y': '2'}})
except ValidationError as exc:
    print(exc)
    """
    1 validation error for Outer
    inner.y
      Input should be a valid integer [type=int_type, input_value='2', input_type=str]
    """
```

### Dataclasses와 `TypedDict`
Pydantic dataclasses는 위에 표시된 `BaseModel`의 예와 유사하게 동작하지만, `model_config` 대신 `@pydantic.dataclasses.dataclass` 데코레이터에 `config` 키워드 인수를 사용해야 한다는 점이 다를 뿐이다.

가능한 경우, [`pydantic.types.Strict` 어노테이션](#annotated-strict에서-엄격-모드)으로 필드에 어노테이트하여 바닐라 데이터 클래스 또는 `TypedDict` 서브클래스에 대해 중첩된 엄격 모드를 구현할 수 있다.

그러나 이것이 가능하지 *않은* 경우(예: 서드 파티 타입으로 작업하는 경우) 타입에 `__pydantic_config__` 어트리뷰트를 설정하여 Pydantic이 해당 타입에 사용해야 하는 구성을 설정할 수 있다.

```python
from typing_extensions import TypedDict

from pydantic import ConfigDict, TypeAdapter, ValidationError


class Inner(TypedDict):
    y: int


Inner.__pydantic_config__ = ConfigDict(strict=True)


class Outer(TypedDict):
    x: int
    inner: Inner


adapter = TypeAdapter(Outer)
print(adapter.validate_python({'x': '1', 'inner': {'y': 2}}))
#> {'x': 1, 'inner': {'y': 2}}


try:
    adapter.validate_python({'x': '1', 'inner': {'y': '2'}})
except ValidationError as exc:
    print(exc)
    """
    1 validation error for typed-dict
    inner.y
      Input should be a valid integer [type=int_type, input_value='2', input_type=str]
    """
```

### `TypeAdapter`
`TypeAdapter` 클래스에 config 키워드 인수를 사용하여 엄격한 모드를 얻을 수도 있다.

```python
from pydantic import ConfigDict, TypeAdapter, ValidationError

adapter = TypeAdapter(bool, config=ConfigDict(strict=True))

try:
    adapter.validate_python('yes')
except ValidationError as exc:
    print(exc)
    """
    1 validation error for bool
      Input should be a valid boolean [type=bool_type, input_value='yes', input_type=str]
    """
```

### `@validate_call`
엄격 모드는 `config` 키워드 인수를 전달하여 `@validate_call` 데코레이터와 함께 사용할 수도 있다.

```python
from pydantic import ConfigDict, ValidationError, validate_call


@validate_call(config=ConfigDict(strict=True))
def foo(x: int) -> int:
    return x


try:
    foo('1')
except ValidationError as exc:
    print(exc)
    """
    1 validation error for foo
    0
      Input should be a valid integer [type=int_type, input_value='1', input_type=str]
    """
```
