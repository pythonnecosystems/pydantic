> **API Documentation**
>
> [`pydantic.dataclasses.dataclass`](https://docs.pydantic.dev/latest/api/dataclasses/#pydantic.dataclasses.dataclass)

Pydantic의 BaseModel을 사용하지 않으려면 대신 (Python 3.7에 도입된) 표준 [dataclasses](https://docs.python.org/3/library/dataclasses.html)에서 동일한 데이터 유효성 검사를 수행할 수 있다.

```python
from datetime import datetime

from pydantic.dataclasses import dataclass


@dataclass
class User:
    id: int
    name: str = 'John Doe'
    signup_ts: datetime = None


user = User(id='42', signup_ts='2032-06-21T12:00')
print(user)
"""
User(id=42, name='John Doe', signup_ts=datetime.datetime(2032, 6, 21, 12, 0))
"""
```

> **Note**
>
> `pydantic.dataclasses.dataclass`는 `pydantic.BaseModel`을 대체하지 **않는다**는 점에 유의하시오. `pydantic.dataclasses.dataclass`는 Pydantic 유효성 검사를 추가하여 `dataclasses.dataclass`와 유사한 기능을 제공한다. `pydantic.BaseModel`을 서브 클래싱하는 것이 더 나은 선택인 경우가 있다.
>
> 자세한 정보와 논의는 [pydantic/pydantic#710](https://github.com/pydantic/pydantic/issues/710)을 참조하시오.

Pydantic dataclasses와 `BaseModel`의 차이점은 다음과 같다.

- [initialization hook](https://docs.pydantic.dev/latest/concepts/dataclasses/#initialization-hooks)의 작동 방식
- [JSON dumping](https://docs.pydantic.dev/latest/concepts/dataclasses/#json-dumping)

모든 표준 Pydantic 필드 유형을 사용할 수 있다. 그러나 생성자에 전달된 인수는 유효성 검사를 수행하고 필요한 경우 강제를 수행하기 위해 복사된다는 점을 유의하자.

Pydantic dataclass에 대해 유효성 검사를 수행하거나 JSON 스키마를 생성하려면 이제 데이터클래스를 `TypeAdapter`로 래핑하고 해당 메서드를 사용해야 한다.

`default_factory`가 필요한 필드는 `pydantic.Field` 또는 `dataclasses.field`로 지정할 수 있다.

```python
import dataclasses
from typing import List, Optional

from pydantic import Field, TypeAdapter
from pydantic.dataclasses import dataclass


@dataclass
class User:
    id: int
    name: str = 'John Doe'
    friends: List[int] = dataclasses.field(default_factory=lambda: [0])
    age: Optional[int] = dataclasses.field(
        default=None,
        metadata=dict(title='The age of the user', description='do not lie!'),
    )
    height: Optional[int] = Field(None, title='The height in cm', ge=50, le=300)


user = User(id='42')
print(TypeAdapter(User).json_schema())
"""
{
    'properties': {
        'id': {'title': 'Id', 'type': 'integer'},
        'name': {'default': 'John Doe', 'title': 'Name', 'type': 'string'},
        'friends': {
            'items': {'type': 'integer'},
            'title': 'Friends',
            'type': 'array',
        },
        'age': {
            'anyOf': [{'type': 'integer'}, {'type': 'null'}],
            'default': None,
            'description': 'do not lie!',
            'title': 'The age of the user',
        },
        'height': {
            'anyOf': [
                {'maximum': 300, 'minimum': 50, 'type': 'integer'},
                {'type': 'null'},
            ],
            'default': None,
            'title': 'The height in cm',
        },
    },
    'required': ['id'],
    'title': 'User',
    'type': 'object',
}
"""
```

`pydantic.dataclasses.dataclass`의 인수는 표준 데코레이터와 동일하지만, [model_config](https://docs.pydantic.dev/latest/api/config/#pydantic.config.ConfigDict)와 동일한 의미를 갖는 추가 키워드 인자 `config`가 하나 더 있다.

> **Warning##
>
> v1.2 이후에는 *pydantic* dataclasses를 입력하려면 [Mypy 플러그인](https://docs.pydantic.dev/latest/integrations/mypy/)을 설치해야 한다.

유효성 검사기와 dataclasses를 결합하는 방법에 대한 자세한 내용은 [dataclass validators](https://docs.pydantic.dev/latest/concepts/validators/#dataclass-validators)를 참조하시오.

## Dataclass config
`BaseModel`을 사용할 때와 같이 `config`을 수정하려면 두 가지 옵션이 있다.

- dataclass 데코레이터에 config를 딕셔너리로 적용한다.
- `ConfigDict`를 config으로 사용한다.

```python
from pydantic import ConfigDict
from pydantic.dataclasses import dataclass


# Option 1 - use directly a dict
# Note: `mypy` will still raise typo error
@dataclass(config=dict(validate_assignment=True))  
class MyDataclass1:
    a: int


# Option 2 - use `ConfigDict`
# (same as before at runtime since it's a `TypedDict` but with intellisense)
@dataclass(config=ConfigDict(validate_assignment=True))
class MyDataclass2:
    a: int
```

> **Note**
> 
> `validate_assignment`에 대한 자세한 내용은 [API reference](https://docs.pydantic.dev/latest/api/config/#pydantic.config.ConfigDict.validate_assignment)에서 확인할 수 있다.

> **Note**
>
> Pydantic dataclasses는 이니셜라이저에 전달된 추가 필드 `ignore`, `forbid` 또는 `allow`로 추가 구성을 지원한다. 그러나 stdlib dataclasses의 일부 기본 동작이 우선할 수 있다. 예를 들어, `extra='allow'`을 사용하는 Pydantic dataclass에 있는 모든 추가 필드는 dataclass를 `print`할 때 생략된다.

## 중첩된 dataclasses
중첩된 dataclasses는 dataclasses와 일반 모델 모두에서 지원된다.

```python
from pydantic import AnyUrl
from pydantic.dataclasses import dataclass


@dataclass
class NavbarButton:
    href: AnyUrl


@dataclass
class Navbar:
    button: NavbarButton


navbar = Navbar(button={'href': 'https://example.com'})
print(navbar)
#> Navbar(button=NavbarButton(href=Url('https://example.com/')))
```

필드로 사용되는 경우 dataclasses(Pydantic 또는 vanilla)는 유효성 검사 입력으로 딕셔너리를 사용해야 한다.

## 제네릭 dataclasses
Pydantic은 타입 변수가 있는 dataclasses를 포함한 제네릭 dataclasses를 지원한다.

```python
from typing import Generic, TypeVar

from pydantic import TypeAdapter
from pydantic.dataclasses import dataclass

T = TypeVar('T')


@dataclass
class GenericDataclass(Generic[T]):
    x: T


validator = TypeAdapter(GenericDataclass)

assert validator.validate_python({'x': None}).x is None
assert validator.validate_python({'x': 1}).x == 1
assert validator.validate_python({'x': 'a'}).x == 'a'
```

dataclasses를 `BaseModel`의 필드로 사용하거나 FastAPI를 통해 사용하는 경우 `TypeAdapter`가 필요하지 않다.

## Stdlib dataclasses와 Pydantic dataclasses

### stdlib dataclasses로부터 상속
중첩 여부와 관계없이 Stdlib dataclasses도 상속받을 수 있으며, Pydantic은 상속된 모든 필드의 유효성을 자동으로 검사다.

```python
import dataclasses

import pydantic


@dataclasses.dataclass
class Z:
    z: int


@dataclasses.dataclass
class Y(Z):
    y: int = 0


@pydantic.dataclasses.dataclass
class X(Y):
    x: int = 0


foo = X(x=b'1', y='2', z='3')
print(foo)
#> X(z=3, y=2, x=1)

try:
    X(z='pika')
except pydantic.ValidationError as e:
    print(e)
    """
    1 validation error for X
    z
      Input should be a valid integer, unable to parse string as an integer [type=int_parsing, input_value='pika', input_type=str]
    """
```

### `BaseModel`과 함께 stdlib dataclasses 사용
중첩 여부에 관계없이 stdlib dataclasses는 BaseModel과 혼합하면 Pydantic dataclasses로 **자동으로 변환된다**! 또한 생성된 Pydantic dataclass는 원본과 **정확히 동일한 구성**(`order`, `frozen`, ...)을 갖는다.

```python
import dataclasses
from datetime import datetime
from typing import Optional

from pydantic import BaseModel, ValidationError


@dataclasses.dataclass(frozen=True)
class User:
    name: str


@dataclasses.dataclass
class File:
    filename: str
    last_modification_time: Optional[datetime] = None


class Foo(BaseModel):
    file: File
    user: Optional[User] = None


file = File(
    filename=['not', 'a', 'string'],
    last_modification_time='2020-01-01T00:00',
)  # nothing is validated as expected
print(file)
"""
File(filename=['not', 'a', 'string'], last_modification_time='2020-01-01T00:00')
"""

try:
    Foo(file=file)
except ValidationError as e:
    print(e)
    """
    1 validation error for Foo
    file.filename
      Input should be a valid string [type=string_type, input_value=['not', 'a', 'string'], input_type=list]
    """

foo = Foo(file=File(filename='myfile'), user=User(name='pika'))
try:
    foo.user.name = 'bulbi'
except dataclasses.FrozenInstanceError as e:
    print(e)
    #> cannot assign to field 'name'
```

### 커스텀 타입 사용
유효성 검사를 추가하기 위해 stdlib dataclasses를 자동으로 변환하므로 커스텀 타입을 사용하면 예기치 않은 동작이 발생할 수 있다. 이 경우 config에 `arbitrary_types_allowed`를 추가하면 된다.

```python
import dataclasses

from pydantic import BaseModel, ConfigDict
from pydantic.errors import PydanticSchemaGenerationError


class ArbitraryType:
    def __init__(self, value):
        self.value = value

    def __repr__(self):
        return f'ArbitraryType(value={self.value!r})'


@dataclasses.dataclass
class DC:
    a: ArbitraryType
    b: str


# valid as it is a builtin dataclass without validation
my_dc = DC(a=ArbitraryType(value=3), b='qwe')

try:

    class Model(BaseModel):
        dc: DC
        other: str

    # invalid as it is now a pydantic dataclass
    Model(dc=my_dc, other='other')
except PydanticSchemaGenerationError as e:
    print(e.message)
    """
    Unable to generate pydantic-core schema for <class '__main__.ArbitraryType'>. Set `arbitrary_types_allowed=True` in the model_config to ignore this error or implement `__get_pydantic_core_schema__` on your type to fully support it.

    If you got this error by calling handler(<some type>) within `__get_pydantic_core_schema__` then you likely need to call `handler.generate_schema(<some type>)` since we do not call `__get_pydantic_core_schema__` on `<some type>` otherwise to avoid infinite recursion.
    """


class Model(BaseModel):
    model_config = ConfigDict(arbitrary_types_allowed=True)

    dc: DC
    other: str


m = Model(dc=my_dc, other='other')
print(repr(m))
#> Model(dc=DC(a=ArbitraryType(value=3), b='qwe'), other='other')
```

### dataclass가 Pydantic dataclass인지 확인하기
Pydantic dataclasses는 여전히 dataclasses로 간주되므로 `dataclasses.is_dataclass`를 사용하면 `True`가 반환된다. 타입이 특별히 pydantic dataclass인지 확인하려면 `pydantic.dataclasses.is_pydantic_dataclass`를 사용할 수 있다.

```python
import dataclasses

import pydantic


@dataclasses.dataclass
class StdLibDataclass:
    id: int


PydanticDataclass = pydantic.dataclasses.dataclass(StdLibDataclass)

print(dataclasses.is_dataclass(StdLibDataclass))
#> True
print(pydantic.dataclasses.is_pydantic_dataclass(StdLibDataclass))
#> False

print(dataclasses.is_dataclass(PydanticDataclass))
#> True
print(pydantic.dataclasses.is_pydantic_dataclass(PydanticDataclass))
#> True
```

## 초기화 후크 (Initialization hooks)
dataclass를 초기화할 때 [`@model_validator`](./validators.md#모델-유효성-검사기) 데코레이터 `mode` 매개변수를 사용하여 유효성 검사 *before* 또는 *after*에 코드를 실행할 수 있다.

```python
from typing import Any, Dict

from pydantic import model_validator
from pydantic.dataclasses import dataclass


@dataclass
class Birth:
    year: int
    month: int
    day: int


@dataclass
class User:
    birth: Birth

    @model_validator(mode='before')
    def pre_root(cls, values: Dict[str, Any]) -> Dict[str, Any]:
        print(f'First: {values}')
        """
        First: ArgsKwargs((), {'birth': {'year': 1995, 'month': 3, 'day': 2}})
        """
        return values

    @model_validator(mode='after')
    def post_root(self) -> 'User':
        print(f'Third: {self}')
        #> Third: User(birth=Birth(year=1995, month=3, day=2))
        return self

    def __post_init__(self):
        print(f'Second: {self.birth}')
        #> Second: Birth(year=1995, month=3, day=2)


user = User(**{'birth': {'year': 1995, 'month': 3, 'day': 2}})
```

Pydantic dataclasses의 `__post_init__`은 유효성 검사기 *중간*에 호출된다. 다음은 호출 순서이다:

- `model_validator(mode='before')`
- `field_validator(mode='before')`
- `field_validator(mode='after')`
- 내부 유효성 검사기(예: `int`, `str`, ... 같은 타입에 대한 유효성 검사)
- `__post_init__`.
- `model_validator(mode='after')`

```python
from dataclasses import InitVar
from pathlib import Path
from typing import Optional

from pydantic.dataclasses import dataclass


@dataclass
class PathData:
    path: Path
    base_path: InitVar[Optional[Path]]

    def __post_init__(self, base_path):
        print(f'Received path={self.path!r}, base_path={base_path!r}')
        #> Received path=PosixPath('world'), base_path=PosixPath('/hello')
        if base_path is not None:
            self.path = base_path / self.path


path_data = PathData('world', base_path='/hello')
# Received path='world', base_path='/hello'
assert path_data.path == Path('/hello/world')
```

### stdlib dataclasses와 차이
Python stdlib의 `dataclasses.dataclass`는 유효성 검사 단계를 실행하지 않으므로 `__post_init__` 메서드만 구현한다는 점에 유의하시오.

## JSON dumping
Pydantic dataclasses에는 `.model_dump_json()` 함수가 없다. JSON으로 덤프하려면 다음과 같이 [RootModel](./models.md#rootmodel과-커스텀-루트-타입)을 사용해야 한다.

```python
import dataclasses
from typing import List

from pydantic import RootModel
from pydantic.dataclasses import dataclass


@dataclass
class User:
    id: int
    name: str = 'John Doe'
    friends: List[int] = dataclasses.field(default_factory=lambda: [0])


user = User(id='42')
print(RootModel[User](User(id='42')).model_dump_json(indent=4))
```

JSON 출력 ...

```json
{
  "id": 42,
  "name": "John Doe",
  "friends": [
    0
  ]
}
```
