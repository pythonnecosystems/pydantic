> **API Documentation**
>
> [pydantic.main.BaseModel](https://docs.pydantic.dev/latest/api/base_model/#pydantic.BaseModel)

Pydantic에서 스키마를 정의하는 주요 방법 중 하나는 모델을 사용하는 것이다. 모델은 [pydantic.BaseModel](https://docs.pydantic.dev/latest/api/base_model/#pydantic.BaseModel)을 상속하고 필드를 annotation된 어트리뷰트로 정의하는 단순한 클래스이다.

모델을 C와 같은 언어의 구조체 또는 API의 단일 엔드포인트의 요구 사항과 유사하게 생각할 수 있다.

모델은 Python의 데이터클래스와 많은 유사점을 공유하지만, 유효성 검사, 직렬화 및 JSON 스키마 생성과 관련된 특정 워크플로우를 간소화하는 몇 가지 미묘하지만 중요한 차이점을 가지고 설계되었다. 이에 대한 자세한 설명은 문서의 [데이터클래스](./dataclasses.md) 섹션에서 확인할 수 있다.

신뢰할 수 없는 데이터를 모델로 전달할 수 있으며, 구문 분석과 유효성 검사 후 결과 모델 인스턴스의 필드가 모델에 정의된 필드 유형과 일치하도록 Pydantic이 보장한다.

> **Note**
>
> Pydantic은 주로 구문 분석과 변환 라이브러리이지 **유효성 검사 라이브러리가 아니다**. 유효성 검사는 제공된 타입과 제약 조건에 부합하는 모델을 구축하는 최종 목적의 수단이다.
> 
> 다시 말해, Pydantic은 입력 데이터가 아닌 출력 모델의 유형과 제약 조건을 보장한다.
> 
> 이것은 난해한 구분처럼 들릴 수 있지만 그렇지 않다. 이것이 무엇을 의미하는지 또는 사용에 어떤 영향을 미칠 수 있는지 잘 모르겠다면 아래의 [데이터 변환]() 섹션을 읽어보시기 바란다.
> 
> 유효성 검사는 Pydantic의 주 목적은 아니지만, 이 라이브러리를 사용하여 커스텀 유효성 검사를 수행할 수 있다.

## 기본 모델 사용
```python
from pydantic import BaseModel


class User(BaseModel):
    id: int
    name: str = 'Jane Doe'
```

이 예에서 `User`는 두 필드가 있는 모델이다.

- `id`는 정수이며 필수이다.
- `name`는 문자열이며 필수는 아니다 (기본값이 있음).

```python
user = User(id='123')
```

이 예에서 `user`는 `User`의 인스턴스이다. 객체를 초기화하면 모든 구문 분석과 유효성 검사가 수행된다. `ValidationError`가 발생하지 않으면 결과 모델 인스턴스가 유효하다는 것을 알 수 있다.

```python
assert user.id == 123
assert isinstance(user.id, int)
# Note that '123' was coerced to an int and its value is 123
```

pydantic의 강제 로직에 대한 자세한 내용은 [데이터 변환](#데이터-변환)에서 확인할 수 있다. 모델의 필드는 `user` 객체의 일반 속성으로 액세스할 수 있다. 문자열 '123'은 필드 타입에 따라 정수로 변환되었다.

```python
assert user.name == 'Jane Doe'
```

`name`은 `user`가 초기화될 때 설정되지 않았으므로 기본값을 갖는다.

```python
assert user.model_fields_set == {'id'}
```

사용자가 초기화될 때 제공된 필드이다.

```python
assert user.model_dump() == {'id': 123, 'name': 'Jane Doe'}
```

`.model_dump()` 또는 `dict(user)`는 필드 딕셔너리를 제공하지만 `.model_dump()`는 다른 많은 인수를 받을 수 있다. (`dict(user)`는 중첩된 모델을 딕셔너리로 재귀적으로 변환하지 못 하지만 `.model_dump()`는 변환한다.)

```python
user.id = 321
assert user.id == 321
```

기본적으로 모델은 변경 가능하며 어트리뷰트 할당을 통해 필드 값을 변경할 수 있다.

### 모델 메소드와 속성
위의 예는 모델이 할 수 있는 작업의 빙산의 일각에 불과하다. 모델에는 다음과 같은 메서드와 어트리뷰트가 있다.

- `model_computed_fields`: 이 모델 인스턴스의 계산된 필드에 대한 딕셔너리.
- `model_construct()`: 유효성 검사를 실행하지 않고 모델을 생성하기 위한 클래스 메서드. [유효성 검사 없이 모델 생성](#유효성-검사-없이-모델-생성)을 참조한다.
- `model_copy()`: 모델의 사본(기본적으로 얕은 사본)을 반환합니다. [직렬화](./serialization.md#model_copy)를 참조하시오.
- `model_dump()`: 모델의 필드 및 값의 딕셔너리를 반환합니다. [직렬화](./serialization.md#modelmodel_dump)를 참조하시오.
- `model_dump_json()`: `model_dump()` 결과를 JSON 문자열 표현으로 반환한다. [직렬화](./serialization.md#modelmodel_dump_json)를 참조하시오.
- `model_extra`: 유효성 검사 중에 설정된 추가 필드를 가져온다.
- `model_fields_set`: 모델 인스턴스가 초기화될 때 설정된 필드 집합이다.
- `model_json_schema()`: 모델을 JSON 스키마로 표현하는 딕셔너리를 반환한다. [JSON 스키마](./json-schema.md)를 참조하시요.
- `model_modify_json_schema()`: JSON 스키마의 "일반" 속성을 채우는 방법에 대한 메서드이다. [JSON 스키마](./json-schema.md)를 참조하시요.
- `model_parametrized_name()`: 일반 클래스의 매개변수화에 대한 클래스 이름을 계산한다.
- `model_post_init()`: 모델이 초기화된 후 추가로 초기화를 수행한다.
- `model_rebuild()`: 모델 스키마를 다시 빌드하며, 재귀적 제네릭 모델 빌드도 지원한다. [모델 스키마 재구성](#모델-스키마-재구성)을 참조하시오.
- `model_validate()`: 모델에 객체를 로드하기 위한 유틸리티이다. [도우미 함수](#도우미-함수)를 참조하시오.
- `model_validate_json()`: Pydantic 모델에 대해 주어진 JSON 데이터의 유효성을 검사하기 위한 유틸리티. [도우미 함수](#도우미-함수)를 참조하시오.

> **Note**
>
> 메서드와 어트리뷰트의 전체 리스트를 포함한 클래스 정의는 [BaseModel](https://docs.pydantic.dev/latest/api/base_model/#pydantic.BaseModel)을 참조하시오.

## 중첩 모델 (Nested Model)
더 복잡한 계층적 데이터 구조는 모델 자체를 어노테이션의 타입으로 사용하여 정의할 수 있다.

```python
from typing import List, Optional

from pydantic import BaseModel


class Foo(BaseModel):
    count: int
    size: Optional[float] = None


class Bar(BaseModel):
    apple: str = 'x'
    banana: str = 'y'


class Spam(BaseModel):
    foo: Foo
    bars: List[Bar]


m = Spam(foo={'count': 4}, bars=[{'apple': 'x1'}, {'apple': 'x2'}])
print(m)
"""
foo=Foo(count=4, size=None) bars=[Bar(apple='x1', banana='y'), Bar(apple='x2', banana='y')]
"""
print(m.model_dump())
"""
{
    'foo': {'count': 4, 'size': None},
    'bars': [{'apple': 'x1', 'banana': 'y'}, {'apple': 'x2', 'banana': 'y'}],
}
"""
```

자체 참조 모델의 경우 [postponed annotation](./postponed-annotation.md#자체-참조-모델)을 참조하시오.

## 모델 스키마 재구성
모델 스키마는 `model_rebuild()`를 사용하여 다시 빌드할 수 있다. 이는 재귀적 제네릭 모델을 빌드할 때 유용하다.

```python
from pydantic import BaseModel, PydanticUserError


class Foo(BaseModel):
    x: 'Bar'


try:
    Foo.model_json_schema()
except PydanticUserError as e:
    print(e)
    """
    `Foo` is not fully defined; you should define `Bar`, then call `Foo.model_rebuild()`.

    For further information visit https://errors.pydantic.dev/2/u/class-not-fully-defined
    """


class Bar(BaseModel):
    pass


Foo.model_rebuild()
print(Foo.model_json_schema())
"""
{
    '$defs': {'Bar': {'properties': {}, 'title': 'Bar', 'type': 'object'}},
    'properties': {'x': {'$ref': '#/$defs/Bar'}},
    'required': ['x'],
    'title': 'Foo',
    'type': 'object',
}
"""
```

Pydantic은 이 작업이 필요한 시점을 자동으로 판단하고 완료되지 않은 경우 오류를 발생시키지만, 재귀 모델이나 제네릭을 처리할 때는 `model_rebuild()`을 선제적으로 호출하는 것이 좋다.

## 임의(arbitrary) 클래스 인스턴스
(이전에는 "ORM Mode"/`from_orm`으로 알려짐).

모델 필드 이름에 해당하는 인스턴스 어트리뷰트를 읽어 임의의 클래스 인스턴스에서 Pydantic 모델을 생성할 수도 있다. 이 기능의 일반적인 적용 사례 중 하나는 객체 관계형 매핑(ORM)과의 통합이다.

이렇게 하려면 구성 어트리뷰트 `model_config['from_attributes'] = True`를 설정한다. 자세한 내용은 [모델 구성](https://docs.pydantic.dev/latest/api/config/#pydantic.config.ConfigDict.from_attributes)과 [ConfigDict](https://docs.pydantic.dev/latest/api/config/#pydantic.config.ConfigDict)를 참조하시오.

여기 예에서는 [SQLAlchemy](https://www.sqlalchemy.org/)를 사용하지만 동일한 접근 방식이 모든 ORM에서 작동한다.

```python
from typing import List

from sqlalchemy import Column, Integer, String
from sqlalchemy.dialects.postgresql import ARRAY
from sqlalchemy.orm import declarative_base

from pydantic import BaseModel, ConfigDict, constr

Base = declarative_base()


class CompanyOrm(Base):
    __tablename__ = 'companies'

    id = Column(Integer, primary_key=True, nullable=False)
    public_key = Column(String(20), index=True, nullable=False, unique=True)
    name = Column(String(63), unique=True)
    domains = Column(ARRAY(String(255)))


class CompanyModel(BaseModel):
    model_config = ConfigDict(from_attributes=True)

    id: int
    public_key: constr(max_length=20)
    name: constr(max_length=63)
    domains: List[constr(max_length=255)]


co_orm = CompanyOrm(
    id=123,
    public_key='foobar',
    name='Testing',
    domains=['example.com', 'foobar.com'],
)
print(co_orm)
#> <__main__.CompanyOrm object at 0x0123456789ab>
co_model = CompanyModel.model_validate(co_orm)
print(co_model)
"""
id=123 public_key='foobar' name='Testing' domains=['example.com', 'foobar.com']
"""
```

### 예약된 이름
`Column` 이름을 예약된 SQLAlchemy 필드의 이름을 따서 지정할 수 있다. 이 경우 필드 별칭(alias)을 사용하면 편리하다.

```python
import typing

import sqlalchemy as sa
from sqlalchemy.orm import declarative_base

from pydantic import BaseModel, ConfigDict, Field


class MyModel(BaseModel):
    model_config = ConfigDict(from_attributes=True)

    metadata: typing.Dict[str, str] = Field(alias='metadata_')


Base = declarative_base()


class SQLModel(Base):
    __tablename__ = 'my_table'
    id = sa.Column('id', sa.Integer, primary_key=True)
    # 'metadata' is reserved by SQLAlchemy, hence the '_'
    metadata_ = sa.Column('metadata', sa.JSON)


sql_model = SQLModel(metadata_={'key': 'val'}, id=1)

pydantic_model = MyModel.model_validate(sql_model)

print(pydantic_model.model_dump())
#> {'metadata': {'key': 'val'}}
print(pydantic_model.model_dump(by_alias=True))
#> {'metadata_': {'key': 'val'}}
```

> **Note**
>
> 위의 예는 별칭이 필드 모집단에서 필드 이름보다 우선하기 때문에 작동한다. SQLModel의 메타데이터 속성을 액세스하면 유효성 검사 오류가 발생한다.

### 중첩 어트리뷰트
어트리뷰트를 사용하여 모델을 구문 분석할 때 모델 인스턴스는 최상위 수준 어트리뷰트와 더 깊게 중첩된 어트리뷰트 모두에 적절하게 생성된다.

다음은 이 원리를 보여주는 예이다.

```python
from typing import List

from pydantic import BaseModel, ConfigDict


class PetCls:
    def __init__(self, *, name: str, species: str):
        self.name = name
        self.species = species


class PersonCls:
    def __init__(self, *, name: str, age: float = None, pets: List[PetCls]):
        self.name = name
        self.age = age
        self.pets = pets


class Pet(BaseModel):
    model_config = ConfigDict(from_attributes=True)

    name: str
    species: str


class Person(BaseModel):
    model_config = ConfigDict(from_attributes=True)

    name: str
    age: float = None
    pets: List[Pet]


bones = PetCls(name='Bones', species='dog')
orion = PetCls(name='Orion', species='cat')
anna = PersonCls(name='Anna', age=20, pets=[bones, orion])
anna_model = Person.model_validate(anna)
print(anna_model)
"""
name='Anna' age=20.0 pets=[Pet(name='Bones', species='dog'), Pet(name='Orion', species='cat')]
"""
```

## 오류 처리
Pydantic은 유효성을 검사하는 데이터에서 오류를 발견할 때마다 `ValidationError`를 발생시킨다.

발견된 오류의 수에 관계없이 `ValidationError` 타입의 단일 예외가 발생하며, 해당 `ValidationError`에는 모든 오류와 오류 발생 방식에 대한 정보가 포함된다.

표준과 커스텀 오류에 대한 자세한 내용은 [오류 처리](../error-messages/error-handling.md)를 참조하시오.

데모로 ...

```python
from typing import List

from pydantic import BaseModel, ValidationError


class Model(BaseModel):
    list_of_ints: List[int]
    a_float: float


data = dict(
    list_of_ints=['1', 2, 'bad'],
    a_float='not a float',
)

try:
    Model(**data)
except ValidationError as e:
    print(e)
    """
    2 validation errors for Model
    list_of_ints.2
      Input should be a valid integer, unable to parse string as an integer [type=int_parsing, input_value='bad', input_type=str]
    a_float
      Input should be a valid number, unable to parse string as a number [type=float_parsing, input_value='not a float', input_type=str]
    """
```

## 도우미 함수
*Pydantic*은 데이터 구문 분석을 위해 모델에 두 개의 `classmethod` 도우미(helper) 함수를 제공한다.

- **model_validate**: 키워드 인자가 아닌 딕셔너리 또는 객체를 받는다는 점을 제외하면 모델의 `__init__` 메서드와 매우 유사하다. 전달된 객체가 딕셔너리가 아닌 경우 `ValidationError`가 발생한다.
- **model_validate_json**: *str* 또는 *bytes*를 받아 json으로 구문 분석한 다음 그 결과를 `model_validate`에 전달한다.

```python
from datetime import datetime
from typing import Optional

from pydantic import BaseModel, ValidationError


class User(BaseModel):
    id: int
    name: str = 'John Doe'
    signup_ts: Optional[datetime] = None


m = User.model_validate({'id': 123, 'name': 'James'})
print(m)
#> id=123 name='James' signup_ts=None

try:
    User.model_validate(['not', 'a', 'dict'])
except ValidationError as e:
    print(e)
    """
    1 validation error for User
      Input should be a valid dictionary or instance of User [type=model_type, input_value=['not', 'a', 'dict'], input_type=list]
    """

m = User.model_validate_json('{"id": 123, "name": "James"}')
print(m)
#> id=123 name='James' signup_ts=None

try:
    m = User.model_validate_json('{"id": 123, "name": 123}')
except ValidationError as e:
    print(e)
    """
    1 validation error for User
    name
      Input should be a valid string [type=string_type, input_value=123, input_type=int]
    """

try:
    m = User.model_validate_json('Invalid JSON')
except ValidationError as e:
    print(e)
    """
    1 validation error for User
      Invalid JSON: expected value at line 1 column 1 [type=json_invalid, input_value='Invalid JSON', input_type=str]
    """
```

JSON 이외의 형식으로 직렬화된 데이터의 유효성을 검사하려면 데이터를 직접 딕셔너리에 로드한 다음 `model_validate`에 전달해야 한다.

> **Note**
>
> 관련된 타입과 모델 구성에 따라, `model_validate`와 `model_validate_json`은 서로 다른 유효성 검사 동작을 가질 수 있다. JSON이 아닌 소스에서 데이터를 가져왔지만 `model_validate_json`에서 발생하는 것과 동일한 유효성 검사 동작과 오류를 원하는 경우, 현재로서는 `model_validate_json(json.dumps(data))`을 사용하는 것을 권장한다.

### 유효성 검사 없이 모델 생성
Pydantic은 유효성 검사 없이 모델을 생성할 수 있는 `model_construct()` 메서드도 제공한다. 이 메서드는 적어도 몇 가지 경우에 유용할 수 있다.

- (성능상의 이유로) 이미 유효한 것으로 알려진 복잡한 데이터로 작업할 때
- 하나 이상의 유효성 검사기 함수가 무능한 경우, 또는
- 하나 이상의 유효성 검사기 함수에 트리거되고 싶지 않은 부작용이 있는 경우.

> **Warning**
>
> `model_construct()`는 유효성 검사를 수행하지 않으므로 유효하지 않은 모델을 생성할 수 있다. **이미 유효성 검사가 완료되었거나 확실히 신뢰할 수 있는 데이터에 대해서만 `model_construct()` 메서드를 사용해야 한다**.

```python
from pydantic import BaseModel


class User(BaseModel):
    id: int
    age: int
    name: str = 'John Doe'


original_user = User(id=123, age=32)

user_data = original_user.model_dump()
print(user_data)
#> {'id': 123, 'age': 32, 'name': 'John Doe'}
fields_set = original_user.model_fields_set
print(fields_set)
#> {'age', 'id'}

# ...
# pass user_data and fields_set to RPC or save to the database etc.
# ...

# you can then create a new instance of User without
# re-running validation which would be unnecessary at this point:
new_user = User.model_construct(_fields_set=fields_set, **user_data)
print(repr(new_user))
#> User(id=123, age=32, name='John Doe')
print(new_user.model_fields_set)
#> {'age', 'id'}

# construct can be dangerous, only use it with validated data!:
bad_user = User.model_construct(id='dog')
print(repr(bad_user))
#> User(id='dog', name='John Doe')
```

`model_construct()`에 대한 `_fields_set` 키워드 인수는 선택 사항이지만 원래 설정된 필드와 그렇지 않은 필드를 더 정확하게 지정할 수 있다. 이 인수를 생략하면 `model_fields_set`은 제공된 데이터의 키가 된다.

예를 들어, 위 예에서 `_fields_set`이 제공되지 않았다면 `new_user.model_fields_set`은 `{'id', 'age', 'name'}`이 될 것이다.

[RootModel](#rootmodel과-커스텀-루트-타입)의 서브클래스 경우 키워드 인수를 사용하는 대신 루트 값을 `model_construct`에 위치에 따라 전달할 수 있다.

다음은 `model_construct`의 동작에 대한 몇 가지 추가 참고 사항이다.

- "유효성 검사를 수행하지 않음"이라고 할 때 여기에는 딕셔너리를 모델 인스턴스로 변환하는 것이 포함된다. 따라서 `Model` 타입이 있는 필드가 있는 경우, 내부 딕셔너리를 `model_construct`로 전달하기 전에 직접 모델로 변환해야 한다.
- 특히, `model_construct` 메서드는 딕셔너리에서 모델을 재귀적으로 구성하는 것을 지원하지 않는다.
- 기본값이 있는 필드에 대한 키워드 인수를 전달하지 않으면 기본값이 계속 사용된다.
- `model_config['extra'] == 'allow'`인 모델의 경우 필드에 해당하지 않는 데이터는 `__pydantic_extra__` 딕셔너리에 올바르게 저장된다.
- 비공개 속성이 있는 모델의 경우, `__init__`을 호출할 때와 동일하게 `__pydantic_private__` 딕셔너리를 초기화한다.
- `model_construct()`를 사용하여 인스턴스를 생성할 때 커스텀 `__init__` 메서드가 정의되어 있더라도 모델 또는 그 부모 클래스의 `__init__` 메서드가 호출되지 않는다.

## 제네릭(Generic) 모델
Pydantic은 공통 모델 구조를 쉽게 재사용할 수 있도록 제네릭 모델 생성을 지원한다.

제네릭 모델을 선언하려면 다음 단계를 수행하여야 한다:

- 모델을 매개변수화하는 데 사용할 하나 이상의 t`yping.TypeVar` 인스턴스를 선언한다.
- `pydantic.BaseModel`과 `typing.Generic`에서 상속하는 pydantic 모델을 선언하고, 여기서 `TypeVar` 인스턴스를 `typing.Generic`에 매개변수로 전달한다.
- `TypeVar` 인스턴스를 다른 타입이나 pydantic 모델로 바꾸고 싶은 경우 어노테이션으로 사용한다.

다음은 재사용하기 쉬운 HTTP 응답 페이로드 래퍼를 생성하기 위해 제네릭 `BaseModel` 서브클래스를 사용하는 예이다.

```python
from typing import Generic, List, Optional, TypeVar

from pydantic import BaseModel, ValidationError

DataT = TypeVar('DataT')


class Error(BaseModel):
    code: int
    message: str


class DataModel(BaseModel):
    numbers: List[int]
    people: List[str]


class Response(BaseModel, Generic[DataT]):
    data: Optional[DataT] = None


data = DataModel(numbers=[1, 2, 3], people=[])
error = Error(code=404, message='Not found')

print(Response[int](data=1))
#> data=1
print(Response[str](data='value'))
#> data='value'
print(Response[str](data='value').model_dump())
#> {'data': 'value'}
print(Response[DataModel](data=data).model_dump())
#> {'data': {'numbers': [1, 2, 3], 'people': []}}
try:
    Response[int](data='value')
except ValidationError as e:
    print(e)
    """
    1 validation error for Response[int]
    data
      Input should be a valid integer, unable to parse string as an integer [type=int_parsing, input_value='value', input_type=str]
    """
```

제네릭 모델 정의에서 `model_config`를 설정하거나 `@field_validator` 또는 기타 Pydantic 데코레이터를 사용하는 경우, 매개변수화된 서브클래스에 `BaseModel` 서브클래스로부터 상속할 때와 동일한 방식으로 적용된다. 제네릭 클래스에 정의된 모든 메서드도 상속된다.

Pydantic의 제네릭은 유형 검사기와도 제대로 통합되므로 각 매개변수화에 대해 고유한 타입을 선언할 때 기대할 수 있는 모든 유형 검사를 수행할 수 있다.

> **Note**
>
> 내부적으로 Pydantic은 제네릭 모델이 매개변수화될 때 런타임에 `BaseModel`의 서브클래스를 생성한다. 이러한 클래스는 캐시되므로 제네릭 모델 사용으로 인한 오버헤드를 최소화시킨다.

제네릭 모델에서 상속받고, 그것이 제네릭이라는 사실을 유지하려면 서브클래스도 `typing.Generic`에서 상속받아야 한다.

```python
from typing import Generic, TypeVar

from pydantic import BaseModel

TypeX = TypeVar('TypeX')


class BaseClass(BaseModel, Generic[TypeX]):
    X: TypeX


class ChildClass(BaseClass[TypeX], Generic[TypeX]):
    # Inherit from Generic[TypeX]
    pass


# Replace TypeX by int
print(ChildClass[int](X=1))
#> X=1
```

또한 슈퍼클래스의 타입 매개변수를 부분적으로 또는 전체를 대체하는 `BaseModel`의 제네릭 서브클래스를 생성할 수도 있다.

```python
from typing import Generic, TypeVar

from pydantic import BaseModel

TypeX = TypeVar('TypeX')
TypeY = TypeVar('TypeY')
TypeZ = TypeVar('TypeZ')


class BaseClass(BaseModel, Generic[TypeX, TypeY]):
    x: TypeX
    y: TypeY


class ChildClass(BaseClass[int, TypeY], Generic[TypeY, TypeZ]):
    z: TypeZ


# Replace TypeY by str
print(ChildClass[str, int](x='1', y='y', z='3'))
#> x=1 y='y' z=3
```

구체적인 하위 클래스의 이름이 중요한 경우 기본 이름 생성을 재정의할 수도 있다.

```python
from typing import Any, Generic, Tuple, Type, TypeVar

from pydantic import BaseModel

DataT = TypeVar('DataT')


class Response(BaseModel, Generic[DataT]):
    data: DataT

    @classmethod
    def model_parametrized_name(cls, params: Tuple[Type[Any], ...]) -> str:
        return f'{params[0].__name__.title()}Response'


print(repr(Response[int](data=1)))
#> IntResponse(data=1)
print(repr(Response[str](data='a')))
#> StrResponse(data='a')
```

중첩된 모델에서 동일한 `TypeVar`를 사용하면 모델의 여러 지점에서 타이핑 관계를 적용할 수 있다.

```python
from typing import Generic, TypeVar

from pydantic import BaseModel, ValidationError

T = TypeVar('T')


class InnerT(BaseModel, Generic[T]):
    inner: T


class OuterT(BaseModel, Generic[T]):
    outer: T
    nested: InnerT[T]


nested = InnerT[int](inner=1)
print(OuterT[int](outer=1, nested=nested))
#> outer=1 nested=InnerT[int](inner=1)
try:
    nested = InnerT[str](inner='a')
    print(OuterT[int](outer='a', nested=nested))
except ValidationError as e:
    print(e)
    """
    2 validation errors for OuterT[int]
    outer
      Input should be a valid integer, unable to parse string as an integer [type=int_parsing, input_value='a', input_type=str]
    nested
      Input should be a valid dictionary or instance of InnerT[int] [type=model_type, input_value=InnerT[str](inner='a'), input_type=InnerT[str]]
    """
```

바인딩된 타입 매개변수를 사용할 때, 그리고 타입 매개변수를 지정하지 않은 채로 둘 때 Pydantic은 제네릭 모델을 `List`와 `Dict`같은 내장 제네릭 타입을 처리하는 방식과 유사하게 처리한다.

- 제네릭 모델을 인스턴스화하기 전에 매개변수를 지정하지 않으면 해당 매개변수를 `TypeVar`의 바인딩된 것으로 취급한다.
- 관련된 `TypeVar` s에 바운드가 없는 경우 `Any`로 처리된다.

또한 `List`와 `Dict`처럼 `TypeVar`를 사용하여 지정된 모든 매개변수는 나중에 구체적인 타입으로 대체할 수 있다.

> **Note**
>
> 직렬화의 경우 이는 `TypeVar`가 부모 모델 `ParentModel`을 사용하여 제약되거나 바인딩되고 자식 모델 `ChildModel`이 구체적인 값으로 사용되는 경우 Pydantic은 `ChildModel`을 `ParentModel`로 직렬화한다는 것을 의미한다. Pydantic이 `ChildModel`을 `ChildModel`로 직렬화하려면 `TypeVar`를 [`SerializeAsAny`](./serialization.md#덕-타이핑으로-직렬화) 안에 감싸야 한다.

```python
from typing import Generic, TypeVar

from pydantic import BaseModel, ValidationError

AT = TypeVar('AT')
BT = TypeVar('BT')


class Model(BaseModel, Generic[AT, BT]):
    a: AT
    b: BT


print(Model(a='a', b='a'))
#> a='a' b='a'

IntT = TypeVar('IntT', bound=int)
typevar_model = Model[int, IntT]
print(typevar_model(a=1, b=1))
#> a=1 b=1
try:
    typevar_model(a='a', b='a')
except ValidationError as exc:
    print(exc)
    """
    2 validation errors for Model[int, TypeVar]
    a
      Input should be a valid integer, unable to parse string as an integer [type=int_parsing, input_value='a', input_type=str]
    b
      Input should be a valid integer, unable to parse string as an integer [type=int_parsing, input_value='a', input_type=str]
    """

concrete_model = typevar_model[int]
print(concrete_model(a=1, b=1))
#> a=1 b=1
```

> **Warning**
>
> 오류가 발생하지 않을 수도 있지만, 매개변수화된 제네릭을 isinstance 검사에 사용하지 않는 것이 좋다.
> 
> 예를 들어, `isinstance(my_model, MyGenericModel[int])`를 수행해서는 안 된다. 그러나 `isinstance(my_model, MyGenericModel)`를 수행해도 괜찮다. (표준 제네릭의 경우 매개변수화된 제네릭으로 서브클래스 검사를 수행하면 에러가 발생할 수 있다).
> 
> 매개변수화된 제네릭에 대해 인스턴스 검사를 수행해야 하는 경우 매개변수화된 제네릭 클래스를 서브클래싱하여 수행하면 된다. 이는 `class MyIntModel(MyGenericModel[int]): ...`과 `isinstance(my_model, MyIntModel)`처럼 보인다.

Pydantic 모델이 `TypeVar` 바인딩에 사용되고 제네릭 타입이 매개변수화되지 않은 경우 Pydantic은 유효성 검사에 바인딩을 사용하지만 직렬화 측면에서는 값을 `Any`로 처리한다.

```python
from typing import Generic, Optional, TypeVar

from pydantic import BaseModel


class ErrorDetails(BaseModel):
    foo: str


ErrorDataT = TypeVar('ErrorDataT', bound=ErrorDetails)


class Error(BaseModel, Generic[ErrorDataT]):
    message: str
    details: Optional[ErrorDataT]


class MyErrorDetails(ErrorDetails):
    bar: str


# serialized as Any
error = Error(
    message='We just had an error',
    details=MyErrorDetails(foo='var', bar='var2'),
)
assert error.model_dump() == {
    'message': 'We just had an error',
    'details': {
        'foo': 'var',
        'bar': 'var2',
    },
}

# serialized using the concrete parametrization
# note that `'bar': 'var2'` is missing
error = Error[ErrorDetails](
    message='We just had an error',
    details=ErrorDetails(foo='var'),
)
assert error.model_dump() == {
    'message': 'We just had an error',
    'details': {
        'foo': 'var',
    },
}
```

`default=...` (Python >= 3.13 또는 `typing-extensions`을 통해 사용 가능) 또는 제약 조건(`TypeVar('T', str, int)` 형식의 `TypeVar`를 사용하는 경우는 거의 없지만)을 사용하는 경우, 타입 변수가 매개 변수화되지 않은 경우 기본값 또는 제약 조건이 유효성 검사와 직렬화 모두에 사용된다. 이 동작은 `pydantic.SerializeAsAny`를 사용하여 재정의(override)할 수 있다.

```python
from typing import Generic, Optional

from typing_extensions import TypeVar

from pydantic import BaseModel, SerializeAsAny


class ErrorDetails(BaseModel):
    foo: str


ErrorDataT = TypeVar('ErrorDataT', default=ErrorDetails)


class Error(BaseModel, Generic[ErrorDataT]):
    message: str
    details: Optional[ErrorDataT]


class MyErrorDetails(ErrorDetails):
    bar: str


# serialized using the default's serializer
error = Error(
    message='We just had an error',
    details=MyErrorDetails(foo='var', bar='var2'),
)
assert error.model_dump() == {
    'message': 'We just had an error',
    'details': {
        'foo': 'var',
    },
}


class SerializeAsAnyError(BaseModel, Generic[ErrorDataT]):
    message: str
    details: Optional[SerializeAsAny[ErrorDataT]]


# serialized as Any
error = SerializeAsAnyError(
    message='We just had an error',
    details=MyErrorDetails(foo='var', bar='baz'),
)
assert error.model_dump() == {
    'message': 'We just had an error',
    'details': {
        'foo': 'var',
        'bar': 'baz',
    },
}
```

## 동적 모델 생성
필드를 지정하기 위해 런타임 정보를 사용하여 모델을 생성하는 것이 바람직한 경우가 있다. 이를 위해 Pydantic은 `create_model` 함수를 제공하여 모델을 즉시 생성할 수 있다.

```python
from pydantic import BaseModel, create_model

DynamicFoobarModel = create_model(
    'DynamicFoobarModel', foo=(str, ...), bar=(int, 123)
)


class StaticFoobarModel(BaseModel):
    foo: str
    bar: int = 123
```

여기서 `StaticFoobarModel`과 `DynamicFoobarModel`은 동일하다.

필드를 `(<type>, <default value>)` 튜플 형식으로 정의한다. 특수 키워드 인수 `__config__`와 `__base__`를 사용하여 새 모델을 커스터마이즈할 수 있습니다. 추가 필드를 사용하여 기본 모델을 확장할 수도 있다.

```python
from pydantic import BaseModel, create_model


class FooModel(BaseModel):
    foo: str
    bar: int = 123


BarModel = create_model(
    'BarModel',
    apple=(str, 'russet'),
    banana=(str, 'yellow'),
    __base__=FooModel,
)
print(BarModel)
#> <class 'pydantic.main.BarModel'>
print(BarModel.model_fields.keys())
#> dict_keys(['foo', 'bar', 'apple', 'banana'])
```

__validators__ 인수로 딕셔너리를 전달하여 유효성 검사기를 추가할 수도 있다.

```python
from pydantic import ValidationError, create_model, field_validator


def username_alphanumeric(cls, v):
    assert v.isalnum(), 'must be alphanumeric'
    return v


validators = {
    'username_validator': field_validator('username')(username_alphanumeric)
}

UserModel = create_model(
    'UserModel', username=(str, ...), __validators__=validators
)

user = UserModel(username='scolvin')
print(user)
#> username='scolvin'

try:
    UserModel(username='scolvi%n')
except ValidationError as e:
    print(e)
    """
    1 validation error for UserModel
    username
      Assertion failed, must be alphanumeric [type=assertion_error, input_value='scolvi%n', input_type=str]
    """
```

> **Note**
>
> 동적으로 생성된 모델을 피클하려면
> 
> - 모델이 전역적으로 정의되어야 한다.
> - `__module__`을 제공해야 한다.



## `RootModel`과 커스텀 루트 타입
> **API Documentation**
>
> [pydantic.root_model.RootModel](https://docs.pydantic.dev/latest/api/root_model/#pydantic.root_model.RootModel)

`pydantic.RootModel`을 서브클래싱하여 "커스텀 루트 타입(custom root type)"으로 Pydantic 모델을 정의할 수 있다.

루트 타입은 Pydantic에서 지원하는 모든 타입이 될 수 있으며, `RootModel`에 제네릭 매개변수로 지정된다. 루트 값은 첫 번째이자 유일한 인수를 통해 모델 `__init__` 또는 `model_validate`로 전달할 수 있다.

다음은 이것이 어떻게 작동하는지에 대한 예이다.

```python
from typing import Dict, List

from pydantic import RootModel

Pets = RootModel[List[str]]
PetsByName = RootModel[Dict[str, str]]


print(Pets(['dog', 'cat']))
#> root=['dog', 'cat']
print(Pets(['dog', 'cat']).model_dump_json())
#> ["dog","cat"]
print(Pets.model_validate(['dog', 'cat']))
#> root=['dog', 'cat']
print(Pets.model_json_schema())
"""
{'items': {'type': 'string'}, 'title': 'RootModel[List[str]]', 'type': 'array'}
"""

print(PetsByName({'Otis': 'dog', 'Milo': 'cat'}))
#> root={'Otis': 'dog', 'Milo': 'cat'}
print(PetsByName({'Otis': 'dog', 'Milo': 'cat'}).model_dump_json())
#> {"Otis":"dog","Milo":"cat"}
print(PetsByName.model_validate({'Otis': 'dog', 'Milo': 'cat'}))
#> root={'Otis': 'dog', 'Milo': 'cat'}
```

루트 필드에 있는 항목에 직접 액세스하거나 항목을 반복하려면 다음 예와 같이 커스텀 `__iter__`와 `__getitem__` 함수를 구현할 수 있다.

```python
from typing import List

from pydantic import RootModel


class Pets(RootModel):
    root: List[str]

    def __iter__(self):
        return iter(self.root)

    def __getitem__(self, item):
        return self.root[item]


pets = Pets.model_validate(['dog', 'cat'])
print(pets[0])
#> dog
print([pet for pet in pets])
#> ['dog', 'cat']
```

매개변수화된 루트 모델의 서브클래스를 직접 생성할 수도 있다.

```python
from typing import List

from pydantic import RootModel


class Pets(RootModel[List[str]]):
    root: List[str]

    def describe(self) -> str:
        return f'Pets: {", ".join(self.root)}'


my_pets = Pets.model_validate(['dog', 'cat'])

print(my_pets.describe())
#> Pets: dog, cat
```

## 허위 불변성 (Faux immutability)
`model_config['frozen'] = True`를 통해 모델을 불변(immutable)으로 구성할 수 있다. 이 설정이 되어 있으면 인스턴스 속성 값을 변경하려고 하면 오류가 발생한다. 자세한 내용은 [API Reference](https://docs.pydantic.dev/latest/api/config/#pydantic.config.ConfigDict.frozen)를 참조하시오.

> **Warning**
>
> Python의 불변성은 결코 엄격하지 않다. 개발자가 결단력이 있거나 어리석다면 소위 "불변" 객체를 언제든지 수정할 수 있다.

```python
from pydantic import BaseModel, ConfigDict, ValidationError


class FooBarModel(BaseModel):
    model_config = ConfigDict(frozen=True)

    a: str
    b: dict


foobar = FooBarModel(a='hello', b={'apple': 'pear'})

try:
    foobar.a = 'different'
except ValidationError as e:
    print(e)
    """
    1 validation error for FooBarModel
    a
      Instance is frozen [type=frozen_instance, input_value='different', input_type=str]
    """

print(foobar.a)
#> hello
print(foobar.b)
#> {'apple': 'pear'}
foobar.b['apple'] = 'grape'
print(foobar.b)
#> {'apple': 'grape'}
```

`a`를 변경하려고 하면 오류가 발생하고 `a`는 변경되지 않는다. 그러나 딕셔너리 `b`는 변경 가능하며, `foobar`의 불변성으로 인해 `b`가 변경되는 것을 막지는 못한다.

## 추상 베이스 클래스
Pydantic 모델은 Python의 [추상 베이스 클래스(Abstract Base Classes, ABC)](https://docs.python.org/3/library/abc.html)와 함께 사용될 수 있다.

```python
import abc

from pydantic import BaseModel


class FooBarModel(BaseModel, abc.ABC):
    a: str
    b: int

    @abc.abstractmethod
    def my_abstract_method(self):
        pass
```

## 필드 순서
필드 순서는 다음과 같은 방식으로 모델에 영향을 끼친다.

- 필드 순서는 모델 [스키마](./json-schema.md)에 보존된다.
- [유효성 검사 오류](#오류-처리)에서 필드 순서는 보존된다.
- `.model_dump()`와 `.model_dump_json()` 등은 순서를 보존한다.

```python
from pydantic import BaseModel, ValidationError


class Model(BaseModel):
    a: int
    b: int = 2
    c: int = 1
    d: int = 0
    e: float


print(Model.model_fields.keys())
#> dict_keys(['a', 'b', 'c', 'd', 'e'])
m = Model(e=2, a=1)
print(m.model_dump())
#> {'a': 1, 'b': 2, 'c': 1, 'd': 0, 'e': 2.0}
try:
    Model(a='x', b='x', c='x', d='x', e='x')
except ValidationError as err:
    error_locations = [e['loc'] for e in err.errors()]

print(error_locations)
#> [('a',), ('b',), ('c',), ('d',), ('e',)]
```

## 필수 필드
필드를 필수로 선언하려면 어노테이션만 사용하여 선언하거나 값으로 `Ellipsis/...`를 사용할 수 있다.

```python
from pydantic import BaseModel, Field


class Model(BaseModel):
    a: int
    b: int = ...
    c: int = Field(...)
```

여기서 `Field`는 [필드 함수](./json-schema.md#필드-커스터마이제이션)를 나타낸다.

여기서 `a`, `b`와 `c`는 모두 필수이다. 그러나 `b: int = ...`의 사용은 [`mypy`](../integrations/mypy.md)에서 정확히 작동하지 않으므로 v1.0부터는 대부분의 경우 피해야 한다.

> **Note**
>
> Pydantic V1에서는 기본값이 명시적으로 지정되지 않은 경우에도 `Option` 또는 `Any`로 어노테이션된 필드에 암시적 기본값인 `None`을 지정지정할 수 있었다. 이는 Pydantic V2에서 변경되었으며, 더 이상 필드에 암시적 기본값을 갖게 하는 타입 어노테이션은 없다.

## 해시할 수 없는 기본값이 있는 필드
Python에서 버그의 일반적인 원인은 변경 가능한 객체를 함수 또는 메서드 인수의 기본값으로 사용하는 것인데, 이는 동일한 인스턴스가 각 호출에서 재사용되기 때문이다.

이 경우 dataclasses 모듈은 실제로 오류를 발생시켜 `dataclasses.field`에 `default_factory` 인수를 사용해야 함을 나타낸다.

Pydantic은 해시할 수 없는 기본값에 [`default_factory`](#동적-기분-값을-갖는-필드)를 사용하는 것도 지원하지만 필수는 아니다. 기본값을 해시할 수 없는 경우 Pydantic은 모델의 각 인스턴스를 생성할 때 기본값을 deepcopy한다.

```python
from typing import Dict, List

from pydantic import BaseModel


class Model(BaseModel):
    item_counts: List[Dict[str, int]] = [{}]


m1 = Model()
m1.item_counts[0]['a'] = 1
print(m1.item_counts)
#> [{'a': 1}]

m2 = Model()
print(m2.item_counts)
#> [{}]
```

## 동적 기분 값을 갖는 필드
기본값이 있는 필드를 선언할 때 동적(즉, 각 모델마다 다르게)으로 만들 수 있다. 이를 위해 `default_factory`를 사용할 수 있다.

다음은 그 예이다:

```python
from datetime import datetime, timezone
from uuid import UUID, uuid4

from pydantic import BaseModel, Field


def datetime_now() -> datetime:
    return datetime.now(timezone.utc)


class Model(BaseModel):
    uid: UUID = Field(default_factory=uuid4)
    updated: datetime = Field(default_factory=datetime_now)


m1 = Model()
m2 = Model()
assert m1.uid != m2.uid
```

자세한 내용은 [필드 함수](./field.md) 문서에서 확인할 수 있다.

## 자동 제외 어트리뷰트

### Class vars
`typing.ClassVar`로 어노테이트된 어트리뷰트는 Pydantic에서 클래스 변수로 정확히 처리되며, 모델 인스턴스의 필드가 될 수 없다.

```python
from typing import ClassVar

from pydantic import BaseModel


class Model(BaseModel):
    x: int = 2
    y: ClassVar[int] = 1


m = Model()
print(m)
#> x=2
print(Model.y)
#> 1
```

### 비공개 모델 어트리뷰트
이름 앞에 밑줄이 있는 어트리뷰트는 Pydantic에서 필드로 취급되지 않으며 모델 스키마에 포함되지 않는다. 대신, 이러한 속어트리뷰트은 "비공개 어트리뷰트"으로 변환되며, `__init__`, `model_validate` 등을 호출하는 동안 유효성이 검사되지 않거나 설정되지도 않는다.

> **Note**
>
> Pydantic v2.1.0부터 비공개 어트리뷰트에 필드 함수를 사용하려고 하면 `NameError`가 발생한다. 비공개 어트리뷰트는 필드로 취급되지 않으므로 `Field()` 함수를 적용할 수 없다.

다음은 사용 예이다.

```python
from datetime import datetime
from random import randint

from pydantic import BaseModel, PrivateAttr


class TimeAwareModel(BaseModel):
    _processed_at: datetime = PrivateAttr(default_factory=datetime.now)
    _secret_value: str

    def __init__(self, **data):
        super().__init__(**data)
        # this could also be done with default_factory
        self._secret_value = randint(1, 5)


m = TimeAwareModel()
print(m._processed_at)
#> 2032-01-02 03:04:05.000006
print(m._secret_value)
#> 3
```

비공개 어트리뷰트 이름은 모델 필드와의 충돌을 방지하기 위해 밑줄로 시작해야 한다. 그러나 dunder 이름(예: `__attr__`)은 지원되지 않는다.

## 데이터 변환
Pydantic은 입력 데이터를 모델 필드 타입에 맞도록 강제로 캐스팅할 수 있으며, 경우에 따라 정보가 손실될 수 있다. 예를 들면 ...

```python
from pydantic import BaseModel


class Model(BaseModel):
    a: int
    b: float
    c: str


print(Model(a=3.000, b='2.72', c=b'binary data').model_dump())
#> {'a': 3, 'b': 2.72, 'c': 'binary data'}
```

이는 Pydantic의 고의적인 결정이며, 종종 가장 유용한 접근 방식이다. 이 주제에 대한 자세한 내용은 [여기](https://github.com/pydantic/pydantic/issues/578)를 참조하시오.

그럼에도 불구하고 [엄격한 타입 검사](./strict-mode.md)도 지원된다.

## 모델 시그니처
모든 Pydantic 모델은 해당 필드에 따라 서명이 생성된다.

```python
import inspect

from pydantic import BaseModel, Field


class FooModel(BaseModel):
    id: int
    name: str = None
    description: str = 'Foo'
    apple: int = Field(alias='pear')


print(inspect.signature(FooModel))
#> (*, id: int, name: str = None, description: str = 'Foo', pear: int) -> None
```

정확한 서명은 인트로스펙션 목적과 `FastAPI` 또는 `hypothesis`같은 라이브러리에 유용하다.

생성된 서명은 커스텀 `__init__` 함수도 존중한다.

```python
import inspect

from pydantic import BaseModel


class MyModel(BaseModel):
    id: int
    info: str = 'Foo'

    def __init__(self, id: int = 1, *, bar: str, **data) -> None:
        """My custom init!"""
        super().__init__(id=id, bar=bar, **data)


print(inspect.signature(MyModel))
#> (id: int = 1, *, bar: str, info: str = 'Foo') -> None
```

시그니처에 포함되려면 필드의 alias 또는 이름이 유효한 Python 식별자이어야 한다. Pydantic은 시그니처를 생성할 때 이름보다 필드의 alias를 우선시하지만, alias가 유효한 Python 식별자가 아닌 경우 필드 이름을 사용할 수도 있다.

필드의 alias와 이름이 모두 유효한 식별자가 아닌 경우(`create_model`의 이색적인 사용을 통해 가능할 수 있음) `**data` 인수가 추가된다. 또한 `model_config['extra'] == 'allow'`인 경우 `**data` 인자는 항상 시그니처에 존재한다.

## 구조적 패턴 매칭
Python 3.10의 [PEP 636](https://peps.python.org/pep-0636/)에서 도입된 모델에 대한 구조적 패턴 매칭을 지원한다.

```python
from pydantic import BaseModel


class Pet(BaseModel):
    name: str
    species: str


a = Pet(name='Bones', species='dog')

match a:
    # match `species` to 'dog', declare and initialize `dog_name`
    case Pet(species='dog', name=dog_name):
        print(f'{dog_name} is a dog')
#> Bones is a dog
    # default case
    case _:
        print('No dog matched')
```

> **Note**
>
> `match-case` 구문은 새로운 모델을 생성하는 것처럼 보일 수 있지만 어트리뷰트를 가져와 비교하거나 선언하고 초기화하기 위한 구문일 뿐이므로 괘념치 마시오.

## 어트리뷰트 복사본
대부분의 경우 생성자에 전달된 인수는 유효성 검사를 수행하기 위해 복사되며, 필요한 경우 강제로 수행된다.

아래 예에서는 유효성 검사 중에 복사되었기 때문에 클래스가 생성된 후 리스트의 ID가 변경된다는 점에 유의하자.

```python
from typing import List

from pydantic import BaseModel


class C1:
    arr = []

    def __init__(self, in_arr):
        self.arr = in_arr


class C2(BaseModel):
    arr: List[int]


arr_orig = [1, 9, 10, 3]


c1 = C1(arr_orig)
c2 = C2(arr=arr_orig)
print('id(c1.arr) == id(c2.arr):', id(c1.arr) == id(c2.arr))
#> id(c1.arr) == id(c2.arr): False
```

## 추가 필드
기본적으로 Pydantic 모델은 인식할 수 없는 필드에 대한 데이터를 제공해도 오류를 발생시키지 않고 무시한다.

```python
from pydantic import BaseModel


class Model(BaseModel):
    x: int


m = Model(x=1, y='a')
assert m.model_dump() == {'x': 1}
```

오류를 발생시키려면 `model_config`를 통해 이 작업을 수행할 수 있다.

```python
from pydantic import BaseModel, ConfigDict, ValidationError


class Model(BaseModel):
    x: int

    model_config = ConfigDict(extra='forbid')


try:
    Model(x=1, y='a')
except ValidationError as exc:
    print(exc)
    """
    1 validation error for Model
    y
      Extra inputs are not permitted [type=extra_forbidden, input_value='a', input_type=str]
    """
```

대신 제공된 추가 데이터를 보존하려면 `extra='allow'`을 설정하면 된다. 그러면 추가 필드가 `BaseModel.__pydantic_extra__`에 저장된다.

```python
from pydantic import BaseModel, ConfigDict


class Model(BaseModel):
    x: int

    model_config = ConfigDict(extra='allow')


m = Model(x=1, y='a')
assert m.__pydantic_extra__ == {'y': 'a'}
```

기본적으로 이러한 추가 항목에는 유효성 검사가 적용되지 않지만 `__pydantic_extra__`에 대한 타입 어노테이션을 재정의하여 값의 타입을 설정할 수 있다.

```python
from typing import Dict

from pydantic import BaseModel, ConfigDict, ValidationError


class Model(BaseModel):
    __pydantic_extra__: Dict[str, int]

    x: int

    model_config = ConfigDict(extra='allow')


try:
    Model(x=1, y='a')
except ValidationError as exc:
    print(exc)
    """
    1 validation error for Model
    y
      Input should be a valid integer, unable to parse string as an integer [type=int_parsing, input_value='a', input_type=str]
    """

m = Model(x=1, y='2')
assert m.x == 1
assert m.y == 2
assert m.model_dump() == {'x': 1, 'y': 2}
assert m.__pydantic_extra__ == {'y': 2}
```

클래스의 `__pydantic_config__` 어트리뷰트를 유효한 `ConfigDict`로 설정하여 구성을 제어한다는 점을 제외하면 `TypedDict`와 `dataclasses`에도 동일한 구성이 적용된다.
