Pydantic을 사용하면 모델에서 JSON 스키마를 자동으로 생성할 수 있다.

Pydantic을 사용하여 필드 또는 모델에서 JSON 스키마 또는 JSON 표현을 생성하는 여러 방법이 있다.

- [`BaseModel.model_json_schema`](https://docs.pydantic.dev/latest/api/base_model/#pydantic.main.BaseModel.model_json_schema)는 스키마의 딕셔너리를 반환한다.
- [`BaseModel.model_dump_json`](https://docs.pydantic.dev/latest/api/base_model/#pydantic.main.BaseModel.model_dump_json)은 스키마의 딕셔너리에 대한 JSON 문자열 표현을 반환한다.
- [`TypeAdapter.dump_json`](https://docs.pydantic.dev/latest/api/type_adapter/#pydantic.type_adapter.TypeAdapter.dump_json)은 적응된 타입의 인스턴스를 JSON으로 직렬화한다.
- [`TypeAdapter.json_schema`](https://docs.pydantic.dev/latest/api/type_adapter/#pydantic.type_adapter.TypeAdapter.json_schema)는 적응된 타입에 대한 JSON 스키마를 생성한다.

생성된 JSON 스키마는 다음 사양을 준수한다.

- [JSON Schema Draft 2020-12](https://json-schema.org/draft/2020-12/release-notes.html)
- [OpenAPI extension](https://github.com/OAI/OpenAPI-Specification)

```python
import json
from enum import Enum
from typing import Union

from typing_extensions import Annotated

from pydantic import BaseModel, Field
from pydantic.config import ConfigDict


class FooBar(BaseModel):
    count: int
    size: Union[float, None] = None


class Gender(str, Enum):
    male = 'male'
    female = 'female'
    other = 'other'
    not_given = 'not_given'


class MainModel(BaseModel):
    """
    This is the description of the main model
    """

    model_config = ConfigDict(title='Main')

    foo_bar: FooBar
    gender: Annotated[Union[Gender, None], Field(alias='Gender')] = None
    snap: int = Field(
        42,
        title='The Snap',
        description='this is the value of snap',
        gt=30,
        lt=50,
    )


print(json.dumps(MainModel.model_json_schema(), indent=2))
```

```json
{
  "$defs": {
    "FooBar": {
      "properties": {
        "count": {
          "title": "Count",
          "type": "integer"
        },
        "size": {
          "anyOf": [
            {
              "type": "number"
            },
            {
              "type": "null"
            }
          ],
          "default": null,
          "title": "Size"
        }
      },
      "required": [
        "count"
      ],
      "title": "FooBar",
      "type": "object"
    },
    "Gender": {
      "enum": [
        "male",
        "female",
        "other",
        "not_given"
      ],
      "title": "Gender",
      "type": "string"
    }
  },
  "description": "This is the description of the main model",
  "properties": {
    "foo_bar": {
      "$ref": "#/$defs/FooBar"
    },
    "Gender": {
      "anyOf": [
        {
          "$ref": "#/$defs/Gender"
        },
        {
          "type": "null"
        }
      ],
      "default": null
    },
    "snap": {
      "default": 42,
      "description": "this is the value of snap",
      "exclusiveMaximum": 50,
      "exclusiveMinimum": 30,
      "title": "The Snap",
      "type": "integer"
    }
  },
  "required": [
    "foo_bar"
  ],
  "title": "Main",
  "type": "object"
}
```

## JSON 스키마 생성에 대한 일반 참고 사항
- `Optional` 필드에 대한 JSON 스키마는 `null` 값이 허용됨을 나타낸다.
- `Decimal` 타입은 JSON 스키마에서 문자열로 노출(및 직렬화)된다.
- JSON 스키마는 `namedtuple` s를 `namedtuple` s로 보존하지 않는다.
- 서로 다른 경우 JSON 스키마가 유효성 검사에 대한 입력을 나타낼지, 직렬화의 출력을 나타낼지 지정할 수 있다.
- 사용된 하위 모델은 사양에 따라 `$defs` JSON 어트리뷰트에 추가되고 참조된다.
- 사용자 지정 제목, 설명 또는 기본값과 같은 (`Field` 클래스를 통한) 수정 사항이 있는 하위 모델은 참조되는 대신 재귀적으로 포함된다.
- 모델에 대한 `description`은 클래스의 docstring 또는 `Field` 클래스에 대한 인수 `description`에서 가져온다.
- 스키마는 기본적으로 alias를 키로 사용하여 생성되지만, `by_alias=False` 키워드 인수를 사용하여 `model_json_schema()` 또는 `model_dump_json()`을 호출하여 모델 속성 이름을 대신 사용하여 생성할 수 있다.
- `$refs`의 형식은 `ref_template` 키워드 인수를 사용하여 `model_json_schema()` 또는 `model_dump_json()`을 호출하여 변경할 수 있다.

## 지정된 타입의 스키마 가져오기
`TypeAdapter` 클래스를 사용하면 임의의 타입에 대한 JSON 스키마의 유효성을 검사, 직렬화 및 생성하는 메서드가 있는 객체를 만들 수 있다. 이 클래스는 현재 더 이상 사용되지 않는 Pydantic V1의 `schema_of`를 완전히 대체한다.

```python
from typing import List

from pydantic import TypeAdapter

adapter = TypeAdapter(List[int])
print(adapter.json_schema())
#> {'items': {'type': 'integer'}, 'type': 'array'}
```

## 필드 커스터마이제이션
선택 사항으로 [`Field`](https://docs.pydantic.dev/latest/api/fields/#pydantic.fields.Field) 함수를 사용하여 필드와 유효성 검사에 대한 추가 정보를 제공할 수 있다.

생성된 JSON 스키마를 커스터마이즈하는 데만 사용되는 필드 매개변수에 대한 자세한 내용은 [JSON 스키마 커스터마이징](./field.md#json-스티마-커스터마이징)을 참조하시오.

[모델 구성](https://docs.pydantic.dev/latest/api/config/#pydantic.config.ConfigDict)을 사용하여 모델에서 JSON 직렬화와 추가 스키마 속성을 커스터마이즈할 수도 있다. 특히 다음 구성 옵션이 관련된다.

- `title`
- `use_enum_values`
- `json_schema_extra`
- `Ser_json_timedelta`
- `ser_json_bytes`

이러한 옵션에 대한 자세한 내용은 [ConfigDict](https://docs.pydantic.dev/latest/api/config/#pydantic.config.ConfigDict)를 참조하시오.

### 적용되지 않은 `Field` 제약 조건
Pydantic이 적용되지 않는 제약 조건을 발견하면 오류가 발생한다. 구문 분석 시 확인되지 않더라도 제약 조건을 스키마에 강제로 표시하려면 원시 스키마 어트리뷰트 이름과 함께 `Field`에 가변 인수를 사용할 수 있다.

```python
from pydantic import BaseModel, Field, PositiveInt

try:
    # this won't work since `PositiveInt` takes precedence over the
    # constraints defined in `Field`, meaning they're ignored
    class Model(BaseModel):
        foo: PositiveInt = Field(..., lt=10)

except ValueError as e:
    print(e)


# if you find yourself needing this, an alternative is to declare
# the constraints in `Field` (or you could use `conint()`)
# here both constraints will be enforced:
class ModelB(BaseModel):
    # Here both constraints will be applied and the schema
    # will be generated correctly
    foo: int = Field(..., gt=0, lt=10)


print(ModelB.model_json_schema())
"""
{
    'properties': {
        'foo': {
            'exclusiveMaximum': 10,
            'exclusiveMinimum': 0,
            'title': 'Foo',
            'type': 'integer',
        }
    },
    'required': ['foo'],
    'title': 'ModelB',
    'type': 'object',
}
"""
```

### `typing.Annotated` 필드
`Field` 값을 할당하는 대신 타입 힌트에서 `typing.Annotated`를 사용하여 지정할 수 있다.

```python
from uuid import uuid4

from typing_extensions import Annotated

from pydantic import BaseModel, Field


class Foo(BaseModel):
    id: Annotated[str, Field(default_factory=lambda: uuid4().hex)]
    name: Annotated[str, Field(max_length=256)] = Field('Bar', title='te')


print(Foo.model_json_schema())
"""
{
    'properties': {
        'id': {'title': 'Id', 'type': 'string'},
        'name': {
            'default': 'Bar',
            'maxLength': 256,
            'title': 'te',
            'type': 'string',
        },
    },
    'title': 'Foo',
    'type': 'object',
}
"""
```

> **Note**
> 
> `Annotated` 외부에서 할당된 값으로 기본값을 설정하거나 `Annotated` 내부에서 `Field.default_factory`를 사용하여 설정할 수 있다. `Field.default` 인수는 `Annotated` 내부에서는 지원되지 않는다.

Python 3.9 이전 버전에서는 `typing_extensions.Annotated`를 사용할 수 있다.

## 스키마 변경
커스텀 타입(`field_name: TheType` 또는 `field_name: Annotated[TheType, ...]`로 사용)과 `Annotated` 메타데이터(`field_name: Annotated[int, SomeMetadata]`로 사용)는 `__get_pydantic_core_schema__`를 구현하여 생성된 스키마를 수정하거나 재정의할 수 있다. 이 메서드는 두 개의 위치 인자를 받는다.

1. 이 타입에 해당하는 타입 어노테이션(따라서 `TheType[T][int]`의 경우 `TheType[int]`가 됨).
2. `__get_pydantic_core_schema__`의 다음 구현자를 호출하기 위한 처리기/콜백이다.

핸들러 시스템은 [`mode='wrap'` 유효성 검사기](./validators.md#어노테이트된-유효성-검사기)처럼 작동한다. 이 경우 입력은 타입이고 출력은 `core_schema`이다.

다음은 생성된 `core_schema`를 *재정의*하는 커스텀 타입의 예이다.

```python
from dataclasses import dataclass
from typing import Any, Dict, List, Type

from pydantic_core import core_schema

from pydantic import BaseModel, GetCoreSchemaHandler


@dataclass
class CompressedString:
    dictionary: Dict[int, str]
    text: List[int]

    def build(self) -> str:
        return ' '.join([self.dictionary[key] for key in self.text])

    @classmethod
    def __get_pydantic_core_schema__(
        cls, source: Type[Any], handler: GetCoreSchemaHandler
    ) -> core_schema.CoreSchema:
        assert source is CompressedString
        return core_schema.no_info_after_validator_function(
            cls._validate,
            core_schema.str_schema(),
            serialization=core_schema.plain_serializer_function_ser_schema(
                cls._serialize,
                info_arg=False,
                return_schema=core_schema.str_schema(),
            ),
        )

    @staticmethod
    def _validate(value: str) -> 'CompressedString':
        inverse_dictionary: Dict[str, int] = {}
        text: List[int] = []
        for word in value.split(' '):
            if word not in inverse_dictionary:
                inverse_dictionary[word] = len(inverse_dictionary)
            text.append(inverse_dictionary[word])
        return CompressedString(
            {v: k for k, v in inverse_dictionary.items()}, text
        )

    @staticmethod
    def _serialize(value: 'CompressedString') -> str:
        return value.build()


class MyModel(BaseModel):
    value: CompressedString


print(MyModel.model_json_schema())
"""
{
    'properties': {'value': {'title': 'Value', 'type': 'string'}},
    'required': ['value'],
    'title': 'MyModel',
    'type': 'object',
}
"""
print(MyModel(value='fox fox fox dog fox'))
"""
value = CompressedString(dictionary={0: 'fox', 1: 'dog'}, text=[0, 0, 0, 1, 0])
"""

print(MyModel(value='fox fox fox dog fox').model_dump(mode='json'))
#> {'value': 'fox fox fox dog fox'}
```

Pydantic은 `CompressedString`에 대한 스키마를 생성하는 방법을 모르기 때문에 `__get_pydantic_core_schema__` 메서드에서 `handler(source)`를 호출하면 `pydantic.errors.PydanticSchemaGenerationError` 에러가 발생한다. 이것은 대부분의 커스텀 타입에 해당하므로 커스텀 타입에 대한 `handler`로 호출하는 경우는 거의 없다.

`handler`로 호출하여 Pydantic이 스키마 생성을 처리하도록 할 수 있다는 점을 제외하면 일반적으로 `Annotated` 메타데이터의 프로세스와 거의 동일하다.

```python
from dataclasses import dataclass
from typing import Any, Sequence, Type

from pydantic_core import core_schema
from typing_extensions import Annotated

from pydantic import BaseModel, GetCoreSchemaHandler, ValidationError


@dataclass
class RestrictCharacters:
    alphabet: Sequence[str]

    def __get_pydantic_core_schema__(
        self, source: Type[Any], handler: GetCoreSchemaHandler
    ) -> core_schema.CoreSchema:
        if not self.alphabet:
            raise ValueError('Alphabet may not be empty')
        schema = handler(
            source
        )  # get the CoreSchema from the type / inner constraints
        if schema['type'] != 'str':
            raise TypeError('RestrictCharacters can only be applied to strings')
        return core_schema.no_info_after_validator_function(
            self.validate,
            schema,
        )

    def validate(self, value: str) -> str:
        if any(c not in self.alphabet for c in value):
            raise ValueError(
                f'{value!r} is not restricted to {self.alphabet!r}'
            )
        return value


class MyModel(BaseModel):
    value: Annotated[str, RestrictCharacters('ABC')]


print(MyModel.model_json_schema())
"""
{
    'properties': {'value': {'title': 'Value', 'type': 'string'}},
    'required': ['value'],
    'title': 'MyModel',
    'type': 'object',
}
"""
print(MyModel(value='CBA'))
#> value='CBA'

try:
    MyModel(value='XYZ')
except ValidationError as e:
    print(e)
    """
    1 validation error for MyModel
    value
      Value error, 'XYZ' is not restricted to 'ABC' [type=value_error, input_value='XYZ', input_type=str]
    """
```

지금까지 스키마를 래핑했지만 스키마를 수정하거나 무시하고 싶을 때도 가능하다.

스키마를 수정하려면 먼저 핸들러를 호출한 다음 결과를 변경한다.

```python
from typing import Any, Type

from pydantic_core import ValidationError, core_schema
from typing_extensions import Annotated

from pydantic import BaseModel, GetCoreSchemaHandler


class SmallString:
    def __get_pydantic_core_schema__(
        self,
        source: Type[Any],
        handler: GetCoreSchemaHandler,
    ) -> core_schema.CoreSchema:
        schema = handler(source)
        assert schema['type'] == 'str'
        schema['max_length'] = 10  # modify in place
        return schema


class MyModel(BaseModel):
    value: Annotated[str, SmallString()]


try:
    MyModel(value='too long!!!!!')
except ValidationError as e:
    print(e)
    """
    1 validation error for MyModel
    value
      String should have at most 10 characters [type=string_too_long, input_value='too long!!!!!', input_type=str]
    """
```

스키마를 완전히 재정의하려면 핸들러를 호출하지 말고 자체 `CoreSchema`를 반환하자.

```python
from typing import Any, Type

from pydantic_core import ValidationError, core_schema
from typing_extensions import Annotated

from pydantic import BaseModel, GetCoreSchemaHandler


class AllowAnySubclass:
    def __get_pydantic_core_schema__(
        self, source: Type[Any], handler: GetCoreSchemaHandler
    ) -> core_schema.CoreSchema:
        # we can't call handler since it will fail for arbitrary types
        def validate(value: Any) -> Any:
            if not isinstance(value, source):
                raise ValueError(
                    f'Expected an instance of {source}, got an instance of {type(value)}'
                )

        return core_schema.no_info_plain_validator_function(validate)


class Foo:
    pass


class Model(BaseModel):
    f: Annotated[Foo, AllowAnySubclass()]


print(Model(f=Foo()))
#> f=None


class NotFoo:
    pass


try:
    Model(f=NotFoo())
except ValidationError as e:
    print(e)
    """
    1 validation error for Model
    f
      Value error, Expected an instance of <class '__main__.Foo'>, got an instance of <class '__main__.NotFoo'> [type=value_error, input_value=<__main__.NotFoo object at 0x0123456789ab>, input_type=NotFoo]
    """
```

## JSON 스키마 타입
타입, 커스텀 필드 타입 및 제약 조건(예: `max_length`)은 다음과 같은 우선 순위에 따라 해당 사양 형식에 매핑된다(사용 가능한 동등한 형식이 있는 경우).

1. [JSON Schema Core](http://json-schema.org/latest/json-schema-core.html#rfc.section.4.3.1)
2. [JSON Schema Validation](http://json-schema.org/latest/json-schema-validation.html)
3. [OpenAPI 데이터 Type](https://github.com/OAI/OpenAPI-Specification/blob/master/versions/3.0.2.md#data-types)
4. 표준 `foramt` JSON 필드는 보다 복잡한 `string` 하위 타입에 대한 Pydantic 확장을 정의하는 데 사용된다.

Python 또는 Pydantic에서 JSON 스키마로의 필드 스키마 매핑은 다음과 같이 수행된다.

## 최상위 스키마 생성
또한 `$defs`에 모델 및 관련 하위 모델 목록만 포함하는 최상위 JSON 스키마를 생성할 수도 있다.

```python
import json

from pydantic import BaseModel
from pydantic.json_schema import models_json_schema


class Foo(BaseModel):
    a: str = None


class Model(BaseModel):
    b: Foo


class Bar(BaseModel):
    c: int


_, top_level_schema = models_json_schema(
    [(Model, 'validation'), (Bar, 'validation')], title='My Schema'
)
print(json.dumps(top_level_schema, indent=2))
```

JSON 출력 ...

```json
{
  "$defs": {
    "Bar": {
      "properties": {
        "c": {
          "title": "C",
          "type": "integer"
        }
      },
      "required": [
        "c"
      ],
      "title": "Bar",
      "type": "object"
    },
    "Foo": {
      "properties": {
        "a": {
          "default": null,
          "title": "A",
          "type": "string"
        }
      },
      "title": "Foo",
      "type": "object"
    },
    "Model": {
      "properties": {
        "b": {
          "$ref": "#/$defs/Foo"
        }
      },
      "required": [
        "b"
      ],
      "title": "Model",
      "type": "object"
    }
  },
  "title": "My Schema"
}
```

## 스키마 커스터마이제이션
생성된 `$ref` JSON 위치를 커스터마이즈할 수 있다. 정의는 항상 `$defs` 키 아래에 저장되지만 지정된 접두사를 참조에 사용할 수 있다.

이 기능은 JSON 스키마 기본 정의 위치를 확장하거나 수정해야 하는 경우에 유용하다. OpenAPI의 예를 들면,

```python
import json

from pydantic import BaseModel
from pydantic.type_adapter import TypeAdapter


class Foo(BaseModel):
    a: int


class Model(BaseModel):
    a: Foo


adapter = TypeAdapter(Model)

print(
    json.dumps(
        adapter.json_schema(ref_template='#/components/schemas/{model}'),
        indent=2,
    )
)
```

JSON 출력은 ...

```json
{
  "$defs": {
    "Foo": {
      "properties": {
        "a": {
          "title": "A",
          "type": "integer"
        }
      },
      "required": [
        "a"
      ],
      "title": "Foo",
      "type": "object"
    }
  },
  "properties": {
    "a": {
      "$ref": "#/components/schemas/Foo"
    }
  },
  "required": [
    "a"
  ],
  "title": "Model",
  "type": "object"
}
```

모델에 `__get_pydantic_json_schema__`를 구현하여 모델에서 생성된 JSON 스키마를 확장하거나 재정의할 수도 있다.

예를 들어 JSON 스키마에 `examples`를 추가할 수 있다.

```python
import json

from pydantic_core import CoreSchema

from pydantic import BaseModel, GetJsonSchemaHandler
from pydantic.json_schema import JsonSchemaValue


class Person(BaseModel):
    name: str
    age: int

    @classmethod
    def __get_pydantic_json_schema__(
        cls, core_schema: CoreSchema, handler: GetJsonSchemaHandler
    ) -> JsonSchemaValue:
        json_schema = handler(core_schema)
        json_schema = handler.resolve_ref_schema(json_schema)
        json_schema['examples'] = [
            {
                'name': 'John Doe',
                'age': 25,
            }
        ]
        return json_schema


print(json.dumps(Person.model_json_schema(), indent=2))
```

JSON 출력은 ...

```json
{
  "examples": [
    {
      "age": 25,
      "name": "John Doe"
    }
  ],
  "properties": {
    "name": {
      "title": "Name",
      "type": "string"
    },
    "age": {
      "title": "Age",
      "type": "integer"
    }
  },
  "required": [
    "name",
    "age"
  ],
  "title": "Person",
  "type": "object"
}
```

스키마를 제자리에서 변경하는 경우에도 스키마를 *반환해야* 한다는 점에 유의하자.

## JSON 스키마 생성 프로세스 커스터마이징
커스텀 스키마 생성이 필요한 경우, `schema_generator`를 사용하여 어플리케이션에 필요에 따라 [`GenerateJsonSchema`](https://docs.pydantic.dev/latest/api/json_schema/#pydantic.json_schema.GenerateJsonSchema) 클래스를 수정할 수 있다.

JSON 스키마를 생성하는 데 사용할 수 있는 다양한 메서드는 키워드 인자 `schema_generator: type[GenerateJsonSchema] = GenerateJsonSchema`를 허용하며, 커스텀 서브클래스를 이러한 메서드에 전달하여 자신만의 접근 방식을 사용하여 JSON 스키마를 생성할 수 있다.

`GenerateJsonSchema`는 타입의 `pydantic-core` 스키마를 JSON 스키마로 변환하는 것을 구현한다. 설계상 이 클래스는 JSON 스키마 생성 프로세스를 하위 클래스에서 쉽게 재정의할 수 있도록 작은 메서드로 분할하여 JSON 스키마 생성에 대한 "global" 접근 방식을 수정한다.

```python
from pydantic import BaseModel
from pydantic.json_schema import GenerateJsonSchema


class MyGenerateJsonSchema(GenerateJsonSchema):
    def generate(self, schema, mode='validation'):
        json_schema = super().generate(schema, mode=mode)
        json_schema['title'] = 'Customize title'
        json_schema['$schema'] = self.schema_dialect
        return json_schema


class MyModel(BaseModel):
    x: int


print(MyModel.model_json_schema(schema_generator=MyGenerateJsonSchema))
"""
{
    'properties': {'x': {'title': 'X', 'type': 'integer'}},
    'required': ['x'],
    'title': 'Customize title',
    'type': 'object',
    '$schema': 'https://json-schema.org/draft/2020-12/schema',
}
"""
```
