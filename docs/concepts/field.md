> **API Documentation**
>
> [pydantic.fields.Field](https://docs.pydantic.dev/latest/api/fields/#pydantic.fields.Field)

`Field` 함수는 모델의 필드에 메타데이터를 커스터마이즈하고 추가하는 데 사용된다.

## 기본 값(Default Value)
`default` 매개 변수는 필드의 기본값을 정의하는 데 사용된다.

```python
from pydantic import BaseModel, Field


class User(BaseModel):
    name: str = Field(default='John Doe')


user = User()
print(user)
#> name='John Doe'
```

기본값을 생성하기 위해 호출할 콜러블을 정의하기 위해 `default_factory`를 사용할 수도 있다.

```python
from uuid import uuid4

from pydantic import BaseModel, Field


class User(BaseModel):
    id: str = Field(default_factory=lambda: uuid4().hex)
```

> **Info**
>
> `default`와 `default_factory` 매개 변수는 상호 배타적이다.

> **Note**
>
> `typing.Optional`을 사용한다고 해서 필드의 기본값이 `None`으로 설정되는 것은 아니다!

## 어노테이션 사용
`Field` 함수를 어노테이션과 함께 사용할 수도 있다.

```python
from uuid import uuid4

from typing_extensions import Annotated

from pydantic import BaseModel, Field


class User(BaseModel):
    id: Annotated[str, Field(default_factory=lambda: uuid4().hex)]
```

## 필드 aliases
유효성 검사와 직렬화를 위해 필드에 alias를 정의할 수 있다.

alias를 정의하는 세 방법이 있다.

- `Field(..., alias='foo')`
- `Field(..., validation_alias='foo')`
- `Field(..., serialization_alias='foo')`

alias 매개변수는 유효성 검사와 직렬화 모두에 사용된다. 유효성 검사와 직렬화에 각각 다른 alias를 사용하려는 경우, 해당 사용 사례에만 적용되는 `validation_alias`와 `serialization_alias` 매개 변수를 사용할 수 있다.

다음은 alias 매개변수의 몇 가지 사용 예이다.

```python
from pydantic import BaseModel, Field


class User(BaseModel):
    name: str = Field(..., alias='username')


user = User(username='johndoe')  
print(user)
#> name='johndoe'
print(user.model_dump(by_alias=True))  
#> {'username': 'johndoe'}
```

유효성 검사에만 alias를 사용하려는 경우 `validation_alias` 매개 변수를 사용하면 된다.

```python
from pydantic import BaseModel, Field


class User(BaseModel):
    name: str = Field(..., validation_alias='username')


user = User(username='johndoe')  
print(user)
#> name='johndoe'
print(user.model_dump(by_alias=True))  
#> {'name': 'johndoe'}
```

직렬화에 대한 alias만 정의하려는 경우 `serialization_alias` 매개변수를 사용할 수 있다.

```python
from pydantic import BaseModel, Field


class User(BaseModel):
    name: str = Field(..., serialization_alias='username')


user = User(name='johndoe')  
print(user)
#> name='johndoe'
print(user.model_dump(by_alias=True))  
#> {'username': 'johndoe'}
```

> **Alias precedence and priority**
>
> `alias`를 `validation_alias` 또는 `serialization_alias`와 동시에 사용하는 경우 유효성 검사에는 `validation_alias`가, 직렬화에는 `serialization_alias`가 alias보다 우선순위(priority)아 있다.
> 
> 필드에 `alias_priority`를 설정하여 이 동작을 변경할 수도 있다.
> 
> [Alias Precedence](./alias.md#alias-precedence)에 대한 자세한 내용은 [모델 구성](https://docs.pydantic.dev/latest/api/config/#pydantic.config.ConfigDict) 문서에서 확인할 수 있다.

> **VSCode와 Pyright 사용자**
> 
> VSCode에서 [Pylance](https://marketplace.visualstudio.com/items?itemName=ms-python.vscode-pylance) 확장을 사용하는 경우 필드 alias을 사용하여 모델을 인스턴스화할 때 경고가 표시되지 않는다:
> ```python
> from pydantic import BaseModel, Field
> 
> 
> class User(BaseModel):
>     name: str = Field(..., alias='username')
> 
> 
> user = User(username='johndoe')  # VSCode will NOT show a warning here.
> ```
>
> `alias` 키워드 인수를 지정하면 [모델 구성](https://docs.pydantic.dev/latest/api/config/#pydantic.config.ConfigDict.populate_by_name)에서 `populate_by_name`을 `True`로 설정하더라도 런타임에는 작동하지만 필드 이름(이 경우 'name')을 사용하여 모델을 인스턴스화할 때 VSCode에 경고가 표시된다.
> 
> ```python
> from pydantic import BaseModel, ConfigDict, Field
> 
> 
> class User(BaseModel):
>     model_config = ConfigDict(populate_by_name=True)
> 
>     name: str = Field(..., alias='username')
> 
> 
> user = User(name='johndoe')  # VSCode will show a warning here.
> ```
> 
> 필드 이름을 선호하도록 VSCode를 "trick"하려면 `str` 함수를 사용하여 alias 값을 래핑할 수 있다.
> 
> ```python
> from pydantic import BaseModel, ConfigDict, Field
> 
> 
> class User(BaseModel):
>     model_config = ConfigDict(populate_by_name=True)
> 
>     name: str = Field(..., alias='username')
> ```
> 이에 대해서는 [이 이슈](https://github.com/pydantic/pydantic/issues/5893)에서 자세히 설명한다.
>

### 유효성 검사 alias

Pydantic은 모델 인스턴스를 생성할 때 `alias`와 `validation_alias`를 동일하게 취급하지만, VSCode는 클래스 이니셜라이저 시그니처에서 `validation_alias`를 사용하지 않는다. VSCode가 클래스 이니셜라이저에서 `validation_alias`을 사용하도록 하려면 직렬화 중에 `serialization_alias`가 `alias`을 재정의하므로 대신 `alias`와 `serialization_alias`를 모두 지정할 수 있다.

```python
from pydantic import BaseModel, Field


class MyModel(BaseModel):
    my_field: int = Field(..., validation_alias='myValidationAlias')
```

같이 ...

```python
from pydantic import BaseModel, Field


class MyModel(BaseModel):
    my_field: int = Field(
        ...,
        alias='myValidationAlias',
        serialization_alias='my_serialization_alias',
    )


m = MyModel(myValidationAlias=1)
print(m.model_dump(by_alias=True))
#> {'my_serialization_alias': 1}
```

위의 모든 내용은 Pyright와 같이 `@typing.dataclass_transform` 데코레이터를 존중하는 다른 도구에도 적용될 수 있다.

### `AliasPath`와 `AliasChoices`
Pydantic은 `validation_alias`를 사용할 때 편의를 위해 `AliasPath`와 `AliasChoices` 특수 타입을 제공한다.

`AliasPath`는 alias을 사용하여 필드에 대한 경로를 지정하는 데 사용된다. 예를 들어,

```python
from pydantic import BaseModel, Field, AliasPath


class User(BaseModel):
    first_name: str = Field(validation_alias=AliasPath('names', 0))
    last_name: str = Field(validation_alias=AliasPath('names', 1))

user = User.model_validate({'names': ['John', 'Doe']})  # to validate a dictionary using the field aliases.
print(user)
#> first_name='John' last_name='Doe'
```

`first_name` 필드에서는 alias `names`와 인덱스 `0`을 사용하여 이름에 대한 경로를 지정한다. `last_name` 필드에서는 alias `names`와 인덱스 `1`을 사용하여 성의 경로를 지정한다.

`AliasChoices`는 alias을 지정하는 데 사용된다. 예를 들어,

```python
from pydantic import BaseModel, Field, AliasChoices


class User(BaseModel):
    first_name: str = Field(validation_alias=AliasChoices('first_name', 'fname'))
    last_name: str = Field(validation_alias=AliasChoices('last_name', 'lname'))

user = User.model_validate({'fname': 'John', 'lname': 'Doe'})   # the second alias choice for both fields.  
print(user)
#> first_name='John' last_name='Doe'
user = User.model_validate({'first_name': 'John', 'lname': 'Doe'})  # the second alias choice for both fields.
print(user)
#> first_name='John' last_name='Doe'
```

`AliasChoices`를` AliasPath`와 함께 사용할 수도 있다.

```python
from pydantic import BaseModel, Field, AliasPath, AliasChoices


class User(BaseModel):
    first_name: str = Field(validation_alias=AliasChoices('first_name', AliasPath('names', 0)))
    last_name: str = Field(validation_alias=AliasChoices('last_name', AliasPath('names', 1)))


user = User.model_validate({'first_name': 'John', 'last_name': 'Doe'})
print(user)
#> first_name='John' last_name='Doe'
user = User.model_validate({'names': ['John', 'Doe']})
print(user)
#> first_name='John' last_name='Doe'
user = User.model_validate({'names': ['John'], 'last_name': 'Doe'})
print(user)
#> first_name='John' last_name='Doe'
```

## 숫자 제약 조건
숫자 값을 제한하는 데 사용할 수 있는 몇몇 키워드 인수가 있다.

- `gt` - 보다 크다
- `lt` - 보다 작다
- `ge` - 보다 크거나 같다
- `le` - 보다 작거나 같다
- `multiple_of` - 주어진 숫자의 배수
- `allow_inf_nan` - `'inf'`, `'-inf'`, `'nan'` 값 허용

다음은 한 예이다.

```python
from pydantic import BaseModel, Field


class Foo(BaseModel):
    positive: int = Field(gt=0)
    non_negative: int = Field(ge=0)
    negative: int = Field(lt=0)
    non_positive: int = Field(le=0)
    even: int = Field(multiple_of=2)
    love_for_pydantic: float = Field(allow_inf_nan=True)


foo = Foo(
    positive=1,
    non_negative=0,
    negative=-1,
    non_positive=0,
    even=2,
    love_for_pydantic=float('inf'),
)
print(foo)
"""
positive=1 non_negative=0 negative=-1 non_positive=0 even=2 love_for_pydantic=inf
"""
```

> **JSON 스키마**
>
> 생성된 JSON 스키마에서:
> 
> - `gt`와 `lt` 제약 조건은 `exclusiveMinimum`과 `exclusiveMaximum`으로 변환된다.
> - `ge`과 `le` 제약 조건은 `minimum`와 `maximum`으로 변환된다.
> - `multiple_of` 제약 조건은 `multipleOf`로 변환된다.
> 
> 위의 스니펫은 다음과 같은 JSON 스키마를 생성한다.
>
> ```json
> 
>   "type": "object",
>   "properties": {
>     "positive": {
>       "title": "Positive",
>       "type": "integer",
>       "exclusiveMinimum": 0
>     },
>     "non_negative": {
>       "title": "Non Negative",
>       "type": "integer",
>       "minimum": 0
>     },
>     "negative": {
>       "title": "Negative",
>       "type": "integer",
>       "exclusiveMaximum": 0
>     },
>     "non_positive": {
>       "title": "Non Positive",
>       "type": "integer",
>       "maximum": 0
>     },
>     "even": {
>       "title": "Even",
>       "type": "integer",
>       "multipleOf": 2
>     },
>     "love_for_pydantic": {
>       "title": "Love For Pydantic",
>       "type": "number"
>     }
>   },
>   "required": [
>     "positive",
>     "non_negative",
>     "negative",
>     "non_positive",
>     "even",
>     "love_for_pydantic"
>   ]
> }
> ```
>
> 자세한 내용은 [JSON Schema Draft 2020-12](https://json-schema.org/understanding-json-schema/reference/numeric.html#numeric-types)를 참조하시오.

> **복합(compound) 타입에 대한 제약 조건** 
> 
> 복합 타입과 함께 필드 제약 조건을 사용하는 경우, 경우에 따라 오류가 발생할 수 있다. 잠재적인 문제를 방지하려면 `Annotated`을 사용할 수 있다.
>
> ```python
> from typing import Optional
> 
> from typing_extensions import Annotated
> 
> from pydantic import BaseModel, Field
> 
> 
> class Foo(BaseModel):
>     positive: Optional[Annotated[int, Field(gt=0)]]
>     # Can error in some cases, not recommended:
>     non_negative: Optional[int] = Field(ge=0)
> ```

## 문자열 제약 조건
문자열을 제약하는 데 사용할 수 있는 필드가 있다.

- `min_length`: 문자열의 최소 길이이다.
- `max_length`: 문자열의 최대 길이이다.
- `pattern`: 문자열이 일치해야 하는 정규식이다.

다음은 예이다.

```python
from pydantic import BaseModel, Field


class Foo(BaseModel):
    short: str = Field(min_length=3)
    long: str = Field(max_length=10)
    regex: str = Field(pattern=r'^\d*$')  


foo = Foo(short='foo', long='foobarbaz', regex='123')
print(foo)
#> short='foo' long='foobarbaz' regex='123'
```

> **JSON 스키마**
>
> 생성된 JSON 스키마에서:
> 
> - `min_length` 제약 조건은 `minLength`로 변환된다.
> - `max_length` 제약 조건은 `maxLength`로 변환된다.
> - `pattern` 제약 조건은 `pattern`으로 변환된다.
> 
> 위의 스니펫은 다음과 같은 JSON 스키마를 생성한다.
> 
> ```python
> {
>   "title": "Foo",
>   "type": "object",
>   "properties": {
>     "short": {
>       "title": "Short",
>       "type": "string",
>       "minLength": 3
>     },
>     "long": {
>       "title": "Long",
>       "type": "string",
>       "maxLength": 10
>     },
>     "regex": {
>       "title": "Regex",
>       "type": "string",
>       "pattern": "^\\d*$"
>     }
>   },
>   "required": [
>     "short",
>     "long",
>     "regex"
>   ]
> }
> ```

## 십진수 제약 조건
십진수을 제한하는 데 사용할 수 있는 필드가 있다.

- `max_digits`: `Decimal`의 최대 자릿수이다. 소수점 앞의 0이나 끝 자리의의 0은 포함되지 않는다.
- `decimal_places`: 허용되는 소수점 이하 최대 자리 수이다. 끝 자리의 0은 포함되지 않는다.

다음은 한 예이다.

```python
from decimal import Decimal

from pydantic import BaseModel, Field


class Foo(BaseModel):
    precise: Decimal = Field(max_digits=5, decimal_places=2)


foo = Foo(precise=Decimal('123.45'))
print(foo)
#> precise=Decimal('123.45')
```

## Dataclasses 제약 조건
dataclass를 제약하는 데 사용할 수 있는 필드가 있다.

- `init_var`: 필드를 데이터클래스에서 [초기화 전용(init-only)](https://docs.python.org/3/library/dataclasses.html#init-only-variables) 필드로 볼지 여부이다.
- `kw_only`: 필드가 데이터클래스 생성자에서 키워드 전용(keyword-only) 인수가 되어야 하는지 여부이다.

다음은 한 예시이다.

```python
from pydantic import BaseModel, Field
from pydantic.dataclasses import dataclass


@dataclass
class Foo:
    bar: str
    baz: str = Field(init_var=True)
    qux: str = Field(kw_only=True)


class Model(BaseModel):
    foo: Foo


model = Model(foo=Foo('bar', baz='baz', qux='qux'))
print(model.model_dump())  
#> {'foo': {'bar': 'bar', 'qux': 'qux'}}
```

## 기본 값 유효성 검사
매개변수 `validate_default`를 사용하여 필드의 기본값을 유효성 검사할지 여부를 제어할 수 있다.

기본적으로 필드의 기본값에 대하여는 유효성 검사를 하지 않는다.

```python
from pydantic import BaseModel, Field, ValidationError


class User(BaseModel):
    age: int = Field(default='twelve', validate_default=True)


try:
    user = User()
except ValidationError as e:
    print(e)
    """
    1 validation error for User
    age
      Input should be a valid integer, unable to parse string as an integer [type=int_parsing, input_value='twelve', input_type=str]
    """
```

## 필드 표현
매개변수 `repr`을 사용하여 필드를 모델의 문자열 표현에 포함할지 여부를 제어할 수 있다.

```python
from pydantic import BaseModel, Field


class User(BaseModel):
    name: str = Field(repr=True)  
    age: int = Field(repr=False)


user = User(name='John', age=42)
print(user)
#> name='John'
```

## 판별기(Discriminator)
매개변수 `discreminator`는 합집합에서 서로 다른 모델을 판별하는 데 사용되는 필드를 제어하는 데 사용할 수 있다.

**Python 3.8 이상**
```python
from typing import Literal, Union

from pydantic import BaseModel, Field


class Cat(BaseModel):
    pet_type: Literal['cat']
    age: int


class Dog(BaseModel):
    pet_type: Literal['dog']
    age: int


class Model(BaseModel):
    pet: Union[Cat, Dog] = Field(discriminator='pet_type')


print(Model.model_validate({'pet': {'pet_type': 'cat', 'age': 12}}))  
#> pet=Cat(pet_type='cat', age=12)
```

**Python 3.10 이상**
```python
from typing import Literal

from pydantic import BaseModel, Field


class Cat(BaseModel):
    pet_type: Literal['cat']
    age: int


class Dog(BaseModel):
    pet_type: Literal['dog']
    age: int


class Model(BaseModel):
    pet: Cat | Dog = Field(discriminator='pet_type')


print(Model.model_validate({'pet': {'pet_type': 'cat', 'age': 12}}))  
#> pet=Cat(pet_type='cat', age=12)
```

자세한 내용은 [Discreminated Union](https://docs.pydantic.dev/latest/api/standard_library_types/#discriminated-unions-aka-tagged-unions)을 참조하시오.

## 엄격한 모드
`Field`의 `strict` 매개 변수는 필드가 "엄격한 모드"에서 유효성을 검사해야 하는지 여부를 지정한다. 엄격한 모드에서 Pydantic은 `strict=True`인 필드에 데이터를 강제로 적용하는 대신 유효성 검사 중에 오류를 생성한다.

```python
from pydantic import BaseModel, Field


class User(BaseModel):
    name: str = Field(strict=True)  # This is the default value
    age: int = Field(strict=False)


user = User(name='John', age='42')  
# The age field is not validated in the strict mode. Therefore, it can be assigned a string.
print(user)
#> name='John' age=42
```

자세한 내용은 [Strict Mode](https://docs.pydantic.dev/latest/concepts/strict_mode/)를 참조하시오.

Pydantic이 엄격한 모드와 느슨한 모드에서 데이터를 변환하는 방법에 대한 자세한 내용은 [변환 표](https://docs.pydantic.dev/latest/concepts/conversion_table/)를 참조하시오.

## 불변성
매개변수 `frozen`은 [frozen dataclass] 동작을 에뮬레이트하는 데 사용된다. 즉, 모델이 생성된 후 필드에 새 값이 할당되지 않도록 하는 데 사용된다(불변성).

자세한 내용은 [frozen dataclass documentation](https://docs.python.org/3/library/dataclasses.html#frozen-instances)를 참조하시오.

```python
from pydantic import BaseModel, Field, ValidationError


class User(BaseModel):
    name: str = Field(frozen=True)
    age: int


user = User(name='John', age=42)

try:
    user.name = 'Jane'  
except ValidationError as e:
    print(e)
    """
    1 validation error for User
    name
      Field is frozen [type=frozen_field, input_value='Jane', input_type=str]
    """
```

## 제외
`exclude` 매개 변수를 사용하여 모델을 내보낼(exporting) 때 모델에서 제외할 필드를 제어할 수 있다.

다음 예를 참조하시오.

```python
from pydantic import BaseModel, Field


class User(BaseModel):
    name: str
    age: int = Field(exclude=True)


user = User(name='John', age=42)
print(user.model_dump())  
#> {'name': 'John'}
```

자세한 내용은 [직렬화](./serialization.md#model--및-field-level-include와-exclude)를 참조하시오.

## JSON 스티마 커스터마이징
생성된 JSON 스키마를 커스터미이즈하기 위한 전용 필드가 있다.

- `title`: 필드의 제목이다.
- `description`: 필드의 설명이다.
- `examples`: 필드의 예이다.
- `json-schema-extra`: 필드에 추가할 추가 JSON 스키마 속성이다.

다음은 한 예이다.

```python
from pydantic import BaseModel, EmailStr, Field, SecretStr


class User(BaseModel):
    age: int = Field(description='Age of the user')
    email: EmailStr = Field(examples=['marcelo@mail.com'])
    name: str = Field(title='Username')
    password: SecretStr = Field(
        json_schema_extra={
            'title': 'Password',
            'description': 'Password of the user',
            'examples': ['123456'],
        }
    )


print(User.model_json_schema())
"""
{
    'properties': {
        'age': {
            'description': 'Age of the user',
            'title': 'Age',
            'type': 'integer',
        },
        'email': {
            'examples': ['marcelo@mail.com'],
            'format': 'email',
            'title': 'Email',
            'type': 'string',
        },
        'name': {'title': 'Username', 'type': 'string'},
        'password': {
            'description': 'Password of the user',
            'examples': ['123456'],
            'format': 'password',
            'title': 'Password',
            'type': 'string',
            'writeOnly': True,
        },
    },
    'required': ['age', 'email', 'name', 'password'],
    'title': 'User',
    'type': 'object',
}
"""
```
