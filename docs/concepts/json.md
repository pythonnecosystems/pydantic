
## JSON 구문 분석

> **API Documentation**
> 
> [`pydantic.main.BaseModel.model_validate_json`](https://docs.pydantic.dev/latest/api/base_model/#pydantic.main.BaseModel.model_validate_json)
> [`pydantic.type_adapter.TypeAdapter.validate_json`](https://docs.pydantic.dev/latest/api/type_adapter/#pydantic.type_adapter.TypeAdapter.validate_json) <br>
> [`pydantic_core.from_json`](https://docs.pydantic.dev/latest/api/pydantic_core/#pydantic_core.from_json) 

Pydantic은 JSON 구문 분석 기능을 내장하고 있어 아래 사항를 달성할 수 있다.

- 서트파티 라이브러리 사용 비용 없이 상당한 성능 향상
- 사용자 정의 오류 지원
- `strict` 사양 지원

[model_validate_json`](https://docs.pydantic.dev/latest/api/base_model/#pydantic.main.BaseModel.model_validate_json)메서드를 통한 Pydantic의 내장 JSON 구문 분석 예로, 모델의 타입 어노테이션과 일치하지 않는 JSON 데이터를 구문 분석하면서 `strict` 사양을 지원하는 것을 보인다.


```python
# Python 3.9 and above

from datetime import date
from typing import Tuple

from pydantic import BaseModel, ConfigDict, ValidationError


class Event(BaseModel):
    model_config = ConfigDict(strict=True)

    when: date
    where: Tuple[int, int]


json_data = '{"when": "1987-01-28", "where": [51, -1]}'
print(Event.model_validate_json(json_data))  
#> when=datetime.date(1987, 1, 28) where=(51, -1)

try:
    Event.model_validate({'when': '1987-01-28', 'where': [51, -1]})  
except ValidationError as e:
    print(e)
    """
    2 validation errors for Event
    when
      Input should be a valid date [type=date_type, input_value='1987-01-28', input_type=str]
    where
      Input should be a valid tuple [type=tuple_type, input_value=[51, -1], input_type=list]
    """
```

v2.5.0 이상에서 Pydantic은 빠르고 이터러블한 JSON 파서인 [`jiter`](https://docs.rs/jiter/latest/jiter/)를 사용하여 JSON 데이터를 파싱한다. `serde`에 비해 `jiter`를 사용하면 소폭의 성능 개선이 이루어지며, 향후에는 더욱 향상될 것이다.

`jiter` JSON 구문 분석기는 `serde` JSON 구문 분석기와 거의 완전히 호환되며, 한 가지 눈에 띄는 개선 사항은 `jiter`가 `inf`와 `NaN` 값의 역직렬화를 지원한다는 점이다. 앞으로 `jiter`는 유효성 검사 오류를 지원하여 유효하지 않은 값이 포함된 원본 JSON 입력의 위치를 포함할 수 있도록 할 예정이다.

## JSON 직렬화

> **API Documentation**
> 
> [`pydantic.main.BaseModel.model_dump_json`](https://docs.pydantic.dev/latest/api/base_model/#pydantic.main.BaseModel.model_dump_json)
> [`pydantic.type_adapter.TypeAdapter.dump_json`](https://docs.pydantic.dev/latest/api/type_adapter/#pydantic.type_adapter.TypeAdapter.dump_json)<br>
> [`pydantic_core.to_json`](https://docs.pydantic.dev/latest/api/pydantic_core/#pydantic_core.to_json) 

JSON 직렬화에 대한 자세한 내용은 [직렬화 개념](./serialization.md#modelmodel_dump_json) 페이지를 참조하세요.
