
유니온은 모든 필드/항목/값이 유효해야 하는 대신, 단 하나의 멤버만 유효하면 된다는 점에서 Pydantic이 유효성을 검사하는 다른 모든 유형과 근본적으로 다르다.

이로 인해 유니온의 유효성을 검사하는 방법에 약간의 뉘앙스가 생긴다.

- 유니온의 어떤 멤버를 기준으로 어떤 순서로 데이터의 유효성을 검사해야 할까?
- 유효성 검사에 실패하면 어떤 에러를 발생시켜야 할까?

유니온을 검증하는 것은 검증 프로세스에 또 다른 차원을 추가하는 것처럼 느껴진다.

이러한 문제를 해결하기 위해 Pydantic은 유니온 유효성 검사를 위한 세 가지 기본 접근 방식을 지원한다.

- [왼쪽에서 오른쪽 모드](#왼쪽에서-오른쪽-모드) - 가장 간단한 접근 방식으로, union의 각 멤버가 순서대로 시도되고 첫 번째 일치하는 것이 반환된다.
- [스마트 모드](#스마트-모드) - "왼쪽에서 오른쪽 모드"와 유사하게 멤버를 순서대로 시도하지만, 더 나은 일치를 찾기 위해 첫 번째 일치를 지나서 유효성 검사가 진행되며, 대부분의 union 유효성 검사에 대한 기본 모드이다.
- [차별 unions](#차별-unions) - 차별자를 기준으로 union의 구성원 한 명만 시도한다.

## Union 모드

### 왼쪽에서 오른쪽 모드

> **Note**<br>
> 이 모드는 종종 예기치 않은 유효성 검사 결과로 이어지기 때문에 Pydantic >=2에서는 기본값이 아니며, 대신 `union_mode='smart'`가 기본값이다.

이 접근 방식을 사용하면 정의된 순서대로 union의 각 멤버에 대해 유효성 검사가 시도되고 첫 번째 유효성 검사를 성공한 것을 입력으로 받는다.

모든 멤버에 대한 유효성 검사가 실패하면 유효성 검사 오류에 union의 모든 멤버에서 발생한 오류가 포함된다.

`union_mode='left_to_right'`를 사용하려는 union 필드에 필드 매개 변수로 설정해야 한다.

```python
# Union with left to right mode

from typing import Union

from pydantic import BaseModel, Field, ValidationError


class User(BaseModel):
    id: Union[str, int] = Field(union_mode='left_to_right')


print(User(id=123))
#> id=123
print(User(id='hello'))
#> id='hello'

try:
    User(id=[])
except ValidationError as e:
    print(e)
    """
    2 validation errors for User
    id.str
      Input should be a valid string [type=string_type, input_value=[], input_type=list]
    id.int
      Input should be a valid integer [type=int_type, input_value=[], input_type=list]
    """
```

이 경우 멤버의 순서는 위의 예를 조정하는 것에서 알 수 있듯이 매우 중요하다.

```python
# Union with left to right - unexpected results

from typing import Union

from pydantic import BaseModel, Field


class User(BaseModel):
    id: Union[int, str] = Field(union_mode='left_to_right')


print(User(id=123))  # from typing import Union

from pydantic import BaseModel, Field


class User(BaseModel):
    id: Union[int, str] = Field(union_mode='left_to_right')


print(User(id=123))  # As expected the input is validated against the int member and the result is as expected.
#> id=123
print(User(id='456'))  # We're in lax mode and the numeric string '123' is valid as input to the first member of the union, int. Since that is tried first, we get the surprising result of id being an int instead of a str.
#> id=456
```

### 스마트 모드
`union_mode='left_to_right'`의 놀라운 부작용으로 인해 Pydantic >=2에서는 `Union` 유효성 검사의 기본 모드가 `union_mode='smart'`이다.

이 모드에서 pydantic은 union 멤버의 일치 항목을 다음 세 그룹 중 하나로 (최고 점수부터 최저 점수까지) 점수를 매긴다.

- 정확한 타입 일치, 예를 들어 `float | int` union 유효성 검사의 `int` 입력은 `int` 멤버에 대한 정확한 타입 일치이다.
- [`strcit` 모드](./strict-mode.md)에서 유효성 검사가 성공했다.
- lax 모드에서 유효성 검사가 성공했다.

가장 높은 점수를 생성한 union 매치가 최상의 매치로 선택된다.

이 모드에서는 입력에 가장 잘 맞는 짝을 선택하기 위해 다음 단계가 수행된다.

1. union 멤버는 왼쪽에서 오른쪽으로 시도되며, 성공적으로 일치하는 항목은 위에서 설명된 세 가지 범주 중 하나로 점수가 매겨진다.
2. 유효성 검사에서 정확한 타입 일치가 성공하면 해당 멤버가 즉시 반환되고 다음 멤버는 시도되지 않는다.
3. 하나 이상의 멤버가 '엄격한' 일치로 유효성 검사에 성공하면 이러한 '엄격한' 일치 중 가장 왼쪽에 있는 멤버가 반환된다.
4. "느슨한" 모드에서 하나 이상의 멤버에 대해 유효성 검사를 성공하면 가장 왼쪽에 있는 일치 항목이 반환된다.
5. 모든 멤버에서 유효성 검사를 실패하면 모든 오류를 반환한다.

```python
from typing import Union
from uuid import UUID

from pydantic import BaseModel


class User(BaseModel):
    id: Union[int, str, UUID]
    name: str


user_01 = User(id=123, name='John Doe')
print(user_01)
#> id=123 name='John Doe'
print(user_01.id)
#> 123
user_02 = User(id='1234', name='John Doe')
print(user_02)
#> id='1234' name='John Doe'
print(user_02.id)
#> 1234
user_03_uuid = UUID('cf57432e-809e-4353-adbd-9d5c0d733868')
user_03 = User(id=user_03_uuid, name='John Doe')
print(user_03)
#> id=UUID('cf57432e-809e-4353-adbd-9d5c0d733868') name='John Doe'
print(user_03.id)
#> cf57432e-809e-4353-adbd-9d5c0d733868
print(user_03_uuid.int)
#> 275603287559914445491632874575877060712
```

> **Tip**<br>
> `Optional[x]` 타입은 `Union[x, None]`의 약어이다.
> 자세한 내용은 [필수 필드](./models.md#필수-필드)에서 확인하세요.

## 차별 Unions
**차별 union을 '태그된 union'이라고 부르기도 한다.**

차별 union을 사용하면 유효성 검사를 수행할 union 구성원을 선택하여 union 타입을 보다 효율적으로 검증할 수 있다.

이렇게 하면 유효성 검사를 더 효율적으로 수행할 수 있으며 유효성 검사 실패 시 오류가 확산되는 것을 방지할 수 있다.

union에 판별자를 추가하면 생성된 JSON 스키마가 [associated OpenAPI specification](https://github.com/OAI/OpenAPI-Specification/blob/main/versions/3.1.0.md#discriminator-object)을 구현한다는 의미이기도 하다.

### `str` 차별자가 있는 차별 union
여러 모델이 있는 union의 경우, 데이터를 유효성 검사를 해야 하는 union 사례를 구분하는 데 사용할 수 있는 union의 모든 멤버에 공통 필드가 있는 경우가 많으며, 이를 [OpenAPI](https://swagger.io/docs/specification/data-models/inheritance-and-polymorphism/)에서는 "판별자"라고 한다.

이 정보를 기반으로 모델의 유효성 검사를 하려면 각 모델에 하나(또는 여러) 리터럴 값인 판별값을 사용하여 동일한 필드(my_discriminator'라고 하자)를 설정할 수 있다. union의 경우 해당 값에 `Field(discreminator='my_discriminator')` 판별자를 설정할 수 있다.

```python
from typing import Literal, Union

from pydantic import BaseModel, Field, ValidationError


class Cat(BaseModel):
    pet_type: Literal['cat']
    meows: int


class Dog(BaseModel):
    pet_type: Literal['dog']
    barks: float


class Lizard(BaseModel):
    pet_type: Literal['reptile', 'lizard']
    scales: bool


class Model(BaseModel):
    pet: Union[Cat, Dog, Lizard] = Field(..., discriminator='pet_type')
    n: int


print(Model(pet={'pet_type': 'dog', 'barks': 3.14}, n=1))
#> pet=Dog(pet_type='dog', barks=3.14) n=1
try:
    Model(pet={'pet_type': 'dog'}, n=1)
except ValidationError as e:
    print(e)
    """
    1 validation error for Model
    pet.dog.barks
      Field required [type=missing, input_value={'pet_type': 'dog'}, input_type=dict]
    """
```

### 호출가능한 차별자가 있는 차별 union

> **API Documentation**<br>
> [`pydantic.types.Discriminator`](https://docs.pydantic.dev/latest/api/types/#pydantic.types.Discriminator)

여러 모델이 있는 union의 경우, 모든 모델에 걸쳐 판별자로 사용할 수 있는 하나의 균일한 필드가 없는 경우가 있다. 이것이 바로 호출 가능한 `Discriminator`의 완벽한 사용 사례이다.

```python
from typing import Any, Literal, Union

from typing_extensions import Annotated

from pydantic import BaseModel, Discriminator, Tag


class Pie(BaseModel):
    time_to_cook: int
    num_ingredients: int


class ApplePie(Pie):
    fruit: Literal['apple'] = 'apple'


class PumpkinPie(Pie):
    filling: Literal['pumpkin'] = 'pumpkin'


def get_discriminator_value(v: Any) -> str:
    if isinstance(v, dict):
        return v.get('fruit', v.get('filling'))
    return getattr(v, 'fruit', getattr(v, 'filling', None))


class ThanksgivingDinner(BaseModel):
    dessert: Annotated[
        Union[
            Annotated[ApplePie, Tag('apple')],
            Annotated[PumpkinPie, Tag('pumpkin')],
        ],
        Discriminator(get_discriminator_value),
    ]


apple_variation = ThanksgivingDinner.model_validate(
    {'dessert': {'fruit': 'apple', 'time_to_cook': 60, 'num_ingredients': 8}}
)
print(repr(apple_variation))
"""
ThanksgivingDinner(dessert=ApplePie(time_to_cook=60, num_ingredients=8, fruit='apple'))
"""

pumpkin_variation = ThanksgivingDinner.model_validate(
    {
        'dessert': {
            'filling': 'pumpkin',
            'time_to_cook': 40,
            'num_ingredients': 6,
        }
    }
)
print(repr(pumpkin_variation))
"""
ThanksgivingDinner(dessert=PumpkinPie(time_to_cook=40, num_ingredients=6, filling='pumpkin'))
"""
```

`Discriminator`를 사용하여 모델과 기본 유형의 조합으로 union 타입의 유효성 검사를 할 수도 있다.

예를 들면, 

```python
from typing import Any, Union

from typing_extensions import Annotated

from pydantic import BaseModel, Discriminator, Tag, ValidationError


def model_x_discriminator(v: Any) -> str:
    if isinstance(v, int):
        return 'int'
    if isinstance(v, (dict, BaseModel)):
        return 'model'
    else:
        # return None if the discriminator value isn't found
        return None


class SpecialValue(BaseModel):
    value: int


class DiscriminatedModel(BaseModel):
    value: Annotated[
        Union[
            Annotated[int, Tag('int')],
            Annotated['SpecialValue', Tag('model')],
        ],
        Discriminator(model_x_discriminator),
    ]


model_data = {'value': {'value': 1}}
m = DiscriminatedModel.model_validate(model_data)
print(m)
#> value=SpecialValue(value=1)

int_data = {'value': 123}
m = DiscriminatedModel.model_validate(int_data)
print(m)
#> value=123

try:
    DiscriminatedModel.model_validate({'value': 'not an int or a model'})
except ValidationError as e:
    print(e)  
    """
    1 validation error for DiscriminatedModel
    value
      Unable to extract tag using discriminator model_x_discriminator() [type=union_tag_not_found, input_value='not an int or a model', input_type=str]
    """
```

> **Note**<br>
>
> [`typing.Annotated` fields 구문](./json-schema.md#typingannotated-필드)을 사용하면 `union`과 `discriminator` 정보를 재그룹화할 때 유용하다. 자세한 내용은 다음 예를 참조하자.
> 
> 필드에 대한 판별자를 설정하는 몇 가지 방법이 있으며, 모두 구문이 약간 다르다.
> 
> `str` 판별자의 경우,

```python
some_field: Union[...] = Field(discriminator='my_discriminator'
some_field: Annotated[Union[...], Field(discriminator='my_discriminator')]
```

> 호출 가능한 `discriminator`의 경우

```python
some_field: Union[...] = Field(discriminator=Discriminator(...))
some_field: Annotated[Union[...], Discriminator(...)]
some_field: Annotated[Union[...], Field(discriminator=Discriminator(...))]
```

> **Warning**<br>
>
> 차별 union은 `Union[Cat]`과 같은 단일 변형에만 사용할 수 없다.
> 
> Python은 해석 시점에 `Union[T]`를 `T`로 변경하므로 `pydantic`이 `Union[T]`의 필드를 `T`와 구별할 수 없다.

### 중첩된 차별 union
필드에는 하나의 판별자만 설정할 수 있지만 여러 판별자를 결합하고 싶을 때가 있다. 예를 들어 중첩된 `Annotated` 타입을 생성하여 이를 수행할 수 있다.

```python
from typing import Literal, Union

from typing_extensions import Annotated

from pydantic import BaseModel, Field, ValidationError


class BlackCat(BaseModel):
    pet_type: Literal['cat']
    color: Literal['black']
    black_name: str


class WhiteCat(BaseModel):
    pet_type: Literal['cat']
    color: Literal['white']
    white_name: str


Cat = Annotated[Union[BlackCat, WhiteCat], Field(discriminator='color')]


class Dog(BaseModel):
    pet_type: Literal['dog']
    name: str


Pet = Annotated[Union[Cat, Dog], Field(discriminator='pet_type')]


class Model(BaseModel):
    pet: Pet
    n: int


m = Model(pet={'pet_type': 'cat', 'color': 'black', 'black_name': 'felix'}, n=1)
print(m)
#> pet=BlackCat(pet_type='cat', color='black', black_name='felix') n=1
try:
    Model(pet={'pet_type': 'cat', 'color': 'red'}, n='1')
except ValidationError as e:
    print(e)
    """
    1 validation error for Model
    pet.cat
      Input tag 'red' found using 'color' does not match any of the expected tags: 'black', 'white' [type=union_tag_invalid, input_value={'pet_type': 'cat', 'color': 'red'}, input_type=dict]
    """
try:
    Model(pet={'pet_type': 'cat', 'color': 'black'}, n='1')
except ValidationError as e:
    print(e)
    """
    1 validation error for Model
    pet.cat.black.black_name
      Field required [type=missing, input_value={'pet_type': 'cat', 'color': 'black'}, input_type=dict]
    """
```

## Union 유효성 검사 오류
union 유효성 검사를 실패하면 union의 각 케이스에 대해 유효성 검사 오류가 발생하므로 오류 메시지가 상당히 장황할 수 있다. 이는 재귀 모델을 다룰 때 특히 두드러지au, 재귀의 각 수준에서 이유가 생성될 수 있다. 차별 union은 판별값이 일치하는 경우에만 유효성 검사 오류가 생성되므로 이 경우 오류 메시지를 간소화할 수 있다.

아래 예에서 볼 수 있듯이 이러한 사양을 `Discriminator` 생성자에 매개변수로 전달하여 `Discriminator`에 대한 오류 타입, 메시지 및 컨텍스트를 사용자 지정할 수도 있다.

```python
from typing import Union

from typing_extensions import Annotated

from pydantic import BaseModel, Discriminator, Tag, ValidationError


# Errors are quite verbose with a normal Union:
class Model(BaseModel):
    x: Union[str, 'Model']


try:
    Model.model_validate({'x': {'x': {'x': 1}}})
except ValidationError as e:
    print(e)
    """
    4 validation errors for Model
    x.str
      Input should be a valid string [type=string_type, input_value={'x': {'x': 1}}, input_type=dict]
    x.Model.x.str
      Input should be a valid string [type=string_type, input_value={'x': 1}, input_type=dict]
    x.Model.x.Model.x.str
      Input should be a valid string [type=string_type, input_value=1, input_type=int]
    x.Model.x.Model.x.Model
      Input should be a valid dictionary or instance of Model [type=model_type, input_value=1, input_type=int]
    """

try:
    Model.model_validate({'x': {'x': {'x': {}}}})
except ValidationError as e:
    print(e)
    """
    4 validation errors for Model
    x.str
      Input should be a valid string [type=string_type, input_value={'x': {'x': {}}}, input_type=dict]
    x.Model.x.str
      Input should be a valid string [type=string_type, input_value={'x': {}}, input_type=dict]
    x.Model.x.Model.x.str
      Input should be a valid string [type=string_type, input_value={}, input_type=dict]
    x.Model.x.Model.x.Model.x
      Field required [type=missing, input_value={}, input_type=dict]
    """


# Errors are much simpler with a discriminated union:
def model_x_discriminator(v):
    if isinstance(v, str):
        return 'str'
    if isinstance(v, (dict, BaseModel)):
        return 'model'


class DiscriminatedModel(BaseModel):
    x: Annotated[
        Union[
            Annotated[str, Tag('str')],
            Annotated['DiscriminatedModel', Tag('model')],
        ],
        Discriminator(
            model_x_discriminator,
            custom_error_type='invalid_union_member',  
            custom_error_message='Invalid union member',  
            custom_error_context={'discriminator': 'str_or_model'},  
        ),
    ]


try:
    DiscriminatedModel.model_validate({'x': {'x': {'x': 1}}})
except ValidationError as e:
    print(e)
    """
    1 validation error for DiscriminatedModel
    x.model.x.model.x
      Invalid union member [type=invalid_union_member, input_value=1, input_type=int]
    """

try:
    DiscriminatedModel.model_validate({'x': {'x': {'x': {}}}})
except ValidationError as e:
    print(e)
    """
    1 validation error for DiscriminatedModel
    x.model.x.model.x.model.x
      Field required [type=missing, input_value={}, input_type=dict]
    """

# The data is still handled properly when valid:
data = {'x': {'x': {'x': 'a'}}}
m = DiscriminatedModel.model_validate(data)
print(m.model_dump())
#> {'x': {'x': {'x': 'a'}}}
```

각 케이스에 [Tag](https://docs.pydantic.dev/latest/api/types/#pydantic.types.Tag)를 지정하여 오류 메시지를 단순화할 수도 있다. 이 예에서와 같이 복잡한 타입이 있을 때 특히 유용하다.

```python
from typing import Dict, List, Union

from typing_extensions import Annotated

from pydantic import AfterValidator, Tag, TypeAdapter, ValidationError

DoubledList = Annotated[List[int], AfterValidator(lambda x: x * 2)]
StringsMap = Dict[str, str]


# Not using any `Tag`s for each union case, the errors are not so nice to look at
adapter = TypeAdapter(Union[DoubledList, StringsMap])

try:
    adapter.validate_python(['a'])
except ValidationError as exc_info:
    print(exc_info)
    """
    2 validation errors for union[function-after[<lambda>(), list[int]],dict[str,str]]
    function-after[<lambda>(), list[int]].0
      Input should be a valid integer, unable to parse string as an integer [type=int_parsing, input_value='a', input_type=str]
    dict[str,str]
      Input should be a valid dictionary [type=dict_type, input_value=['a'], input_type=list]
    """

tag_adapter = TypeAdapter(
    Union[
        Annotated[DoubledList, Tag('DoubledList')],
        Annotated[StringsMap, Tag('StringsMap')],
    ]
)

try:
    tag_adapter.validate_python(['a'])
except ValidationError as exc_info:
    print(exc_info)
    """
    2 validation errors for union[DoubledList,StringsMap]
    DoubledList.0
      Input should be a valid integer, unable to parse string as an integer [type=int_parsing, input_value='a', input_type=str]
    StringsMap
      Input should be a valid dictionary [type=dict_type, input_value=['a'], input_type=list]
    """
```
