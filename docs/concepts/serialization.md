필드 이름(예: `model.foobar`)을 통해 모델 어트리뷰트에 직접 액세스하는 것 외에도 다양한 방법으로 모델을 변환, 덤프, 직렬화 및 내보낼 수 있다.

> ** **직렬화 vs. 덤프(dump)**
> 
> Pydantic은 "직렬화"와 "덤프"라는 용어를 혼용하여 사용한다. 둘 다 모델을 딕셔너리 또는 JSON 인코딩된 문자열로 변환하는 프로세스를 의미한다.
>
> Pydantic 외부에서 "직렬화"라는 단어는 일반적으로 인메모리 데이터를 문자열 또는 바이트로 변환하는 것을 의미한다. 그러나 Pydantic의 맥락에서는 객체를 Pydantic 모델, 데이터 클래스 등과 같은 보다 구조화된 형식에서 딕셔너리 같은 Python 내장 기능으로 구성된 덜 구조화된 형식으로 변환하는 것과는 매우 밀접한 관계가 있다.
>
> 프리미티브로 변환할 때는 "덤프"라는 단어를 사용하고 문자열로 변환할 때는 "직렬화"라는 단어를 사용하여 이러한 시나리오를 구분할 수 있지만(실제로 그렇게 하기도 한다), 실용적인 목적으로 항상 문자열이나 바이트로의 변환을 의미하지는 않더라도 이 두 가지 상황을 모두 지칭하기 위해 "직렬화"라는 단어를 자주 사용한다.

## `model.model_dump(...)`
> **API**
>
> [pydantic.main.BaseModel.model_dump](https://docs.pydantic.dev/latest/api/base_model/#pydantic.main.BaseModel.model_dump)

이는 모델을 딕셔너리로 변환하는 기본 방법이다. 하위 모델은 재귀적으로 딕셔너리로 변환된다.

> **Note**
>
> 하위 모델이 딕셔너리로 변환되는 한 가지 예외는 `RootModel`과 그 하위 클래스가 래핑 딕셔너리를 사용하지 않고 `root` 필드 값을 직접 덤프한다는 것이다. 이 작업도 재귀적으로 수행된다.

예 ...

```python
from typing import Any, List, Optional

from pydantic import BaseModel, Field, Json


class BarModel(BaseModel):
    whatever: int


class FooBarModel(BaseModel):
    banana: Optional[float] = 1.1
    foo: str = Field(serialization_alias='foo_alias')
    bar: BarModel


m = FooBarModel(banana=3.14, foo='hello', bar={'whatever': 123})

# returns a dictionary:
print(m.model_dump())
#> {'banana': 3.14, 'foo': 'hello', 'bar': {'whatever': 123}}
print(m.model_dump(include={'foo', 'bar'}))
#> {'foo': 'hello', 'bar': {'whatever': 123}}
print(m.model_dump(exclude={'foo', 'bar'}))
#> {'banana': 3.14}
print(m.model_dump(by_alias=True))
#> {'banana': 3.14, 'foo_alias': 'hello', 'bar': {'whatever': 123}}
print(
    FooBarModel(foo='hello', bar={'whatever': 123}).model_dump(
        exclude_unset=True
    )
)
#> {'foo': 'hello', 'bar': {'whatever': 123}}
print(
    FooBarModel(banana=1.1, foo='hello', bar={'whatever': 123}).model_dump(
        exclude_defaults=True
    )
)
#> {'foo': 'hello', 'bar': {'whatever': 123}}
print(
    FooBarModel(foo='hello', bar={'whatever': 123}).model_dump(
        exclude_defaults=True
    )
)
#> {'foo': 'hello', 'bar': {'whatever': 123}}
print(
    FooBarModel(banana=None, foo='hello', bar={'whatever': 123}).model_dump(
        exclude_none=True
    )
)
#> {'foo': 'hello', 'bar': {'whatever': 123}}


class Model(BaseModel):
    x: List[Json[Any]]


print(Model(x=['{"a": 1}', '[1, 2]']).model_dump())
#> {'x': [{'a': 1}, [1, 2]]}
print(Model(x=['{"a": 1}', '[1, 2]']).model_dump(round_trip=True))
#> {'x': ['{"a":1}', '[1,2]']}
```

## `model.model_dump_json(...)`

> **API**
>
> [pydantic.main.BaseModel.model_dump_json](https://docs.pydantic.dev/latest/api/base_model/#pydantic.main.BaseModel.model_dump_json)

`.model_dump_json()` 메서드는 모델을 [`.model_dump()`](https://docs.pydantic.dev/latest/concepts/serialization/#modelmodel_dump)에서 생성된 결과와 동일한 JSON 인코딩된 문자열로 직접 직렬화한다.

자세한 내용은 [인수](https://docs.pydantic.dev/latest/api/base_model/#pydantic.main.BaseModel.model_dump_json)를 참조하시오.

> **Note**
>
> Pydantic은 일반적으로 사용되는 많은 타입(예: `datetime`, `date` 또는 `UUID`)을 JSON으로 직렬화할 수 있으며, 이는 단순한 `json.dumps(foobar)`와 호환되지 않는다.

```python
from datetime import datetime

from pydantic import BaseModel


class BarModel(BaseModel):
    whatever: int


class FooBarModel(BaseModel):
    foo: datetime
    bar: BarModel


m = FooBarModel(foo=datetime(2032, 6, 1, 12, 13, 14), bar={'whatever': 123})
print(m.model_dump_json())
#> {"foo":"2032-06-01T12:13:14","bar":{"whatever":123}}
print(m.model_dump_json(indent=2))
"""
{
  "foo": "2032-06-01T12:13:14",
  "bar": {
    "whatever": 123
  }
}
"""
```

## `dict(model)`과 반복
Pydantic 모델을 `dict(model)`를 사용하여 딕셔너리로 변환할 수도 있으며, `for field_name, field_value in model:`를 사용하여 모델의 필드에 대해 반복할 수도 있다. 이 접근 방식을 사용하면 원시 필드 값이 반환되므로 하위 모델이 딕셔너리로 변환되지 않는다.

예 ...

```python
from pydantic import BaseModel


class BarModel(BaseModel):
    whatever: int


class FooBarModel(BaseModel):
    banana: float
    foo: str
    bar: BarModel


m = FooBarModel(banana=3.14, foo='hello', bar={'whatever': 123})

print(dict(m))
#> {'banana': 3.14, 'foo': 'hello', 'bar': BarModel(whatever=123)}
for name, value in m:
    print(f'{name}: {value}')
    #> banana: 3.14
    #> foo: hello
    #> bar: whatever=123
```

또한 [`루트모델`](https://docs.pydantic.dev/latest/concepts/models/#rootmodel-and-custom-root-types)은 `'root'` 키가 있는 딕셔너리로 변환된다는 점에 유의하시오.

## 커스텀 직렬화
Pydantic은 모델을 딕셔너리 또는 JSON으로 직렬화하는 방법을 커스터마이징할 수 있는 여러 [functional serializers](https://docs.pydantic.dev/latest/api/functional_serializers/#pydantic.functional_serializers)를 제공한다.

- [`@field-serializer`](https://docs.pydantic.dev/latest/api/functional_serializers/#pydantic.functional_serializers.field_serializer)
- [`@model-serializer`](https://docs.pydantic.dev/latest/api/functional_serializers/#pydantic.functional_serializers.model_serializer)
- [`PlainSerializer`](https://docs.pydantic.dev/latest/api/functional_serializers/#pydantic.functional_serializers.PlainSerializer)
- [`WrapSerializer`](https://docs.pydantic.dev/latest/api/functional_serializers/#pydantic.functional_serializers.WrapSerializer)

직렬화를 [`@field_serializer`](https://docs.pydantic.dev/latest/api/functional_serializers/#pydantic.functional_serializers.field_serializer) 데코레이터를 사용하여 필드와 [`@model_serializer`](https://docs.pydantic.dev/latest/api/functional_serializers/#pydantic.functional_serializers.model_serializer) 데코레이터를 사용하여 모델을 커스터마이징할 수 있다.

```python
from datetime import datetime, timedelta, timezone
from typing import Any, Dict

from pydantic import BaseModel, ConfigDict, field_serializer, model_serializer


class WithCustomEncoders(BaseModel):
    model_config = ConfigDict(ser_json_timedelta='iso8601')

    dt: datetime
    diff: timedelta

    @field_serializer('dt')
    def serialize_dt(self, dt: datetime, _info):
        return dt.timestamp()


m = WithCustomEncoders(
    dt=datetime(2032, 6, 1, tzinfo=timezone.utc), diff=timedelta(hours=100)
)
print(m.model_dump_json())
#> {"dt":1969660800.0,"diff":"P4DT14400S"}


class Model(BaseModel):
    x: str

    @model_serializer
    def ser_model(self) -> Dict[str, Any]:
        return {'x': f'serialized {self.x}'}


print(Model(x='test value').model_dump_json())
#> {"x":"serialized test value"}
```

또한 [`PlainSerializer`](https://docs.pydantic.dev/latest/api/functional_serializers/#pydantic.functional_serializers.PlainSerializer)와 [`WrapSerializer`](https://docs.pydantic.dev/latest/api/functional_serializers/#pydantic.functional_serializers.WrapSerializer)를 사용하면 함수를 사용하여 직렬화 출력을 수정할 수 있다.

두 직렬화기 모두 다음을 포함한 선택적 인수를 허용한다.

- `return_type`은 함수의 반환 타입을 지정한다. 생략하면 타입 어노테이션에서 유추된다.
- `when_used`는 이 직렬화기를 사용해야 하는 시기를 지정한다. 값이 'always', 'unless-none', 'json' 및 'json-unless-none'인 문자열을 허용한다. 기본값은 '항상'이다.

`PlainSerializer`는 간단한 함수를 사용하여 직렬화 출력을 수정한다.

```python
from typing_extensions import Annotated

from pydantic import BaseModel
from pydantic.functional_serializers import PlainSerializer

FancyInt = Annotated[
    int, PlainSerializer(lambda x: f'{x:,}', return_type=str, when_used='json')
]


class MyModel(BaseModel):
    x: FancyInt


print(MyModel(x=1234).model_dump())
#> {'x': 1234}

print(MyModel(x=1234).model_dump(mode='json'))
#> {'x': '1,234'}
```

`WrapSerializer`는 표준 직렬화 로직을 적용하는 handler 함수와 함께 원시 입력을 받고, 직렬화의 최종 출력으로 반환하기 전에 결과값을 수정할 수 있다.

```python
from typing import Any

from typing_extensions import Annotated

from pydantic import BaseModel, SerializerFunctionWrapHandler
from pydantic.functional_serializers import WrapSerializer


def ser_wrap(v: Any, nxt: SerializerFunctionWrapHandler) -> str:
    return f'{nxt(v + 1):,}'


FancyInt = Annotated[int, WrapSerializer(ser_wrap, when_used='json')]


class MyModel(BaseModel):
    x: FancyInt


print(MyModel(x=1234).model_dump())
#> {'x': 1234}

print(MyModel(x=1234).model_dump(mode='json'))
#> {'x': '1,235'}
```

### 모델을 덤프할 때 반환 타입 재정의하기
`.model_dump()`의 반환 값은 일반적으로 `dict[str, Any]`로 설명할 수 있지만, `@model_serializer`를 사용하면 실제로 이 시그니처와 일치하지 않는 값을 반환하도록 만들 수 있다.

```python
from pydantic import BaseModel, model_serializer


class Model(BaseModel):
    x: str

    @model_serializer
    def ser_model(self) -> str:
        return self.x


print(Model(x='not a dict').model_dump())
#> not a dict
```

이렇게 하면서도 이 메서드에 대한 적절한 형식 검사를 받으려면 `if TYPE_CHECKING:` 블록에서 `.model_dump()`를 재정의할 수 있다.

```python
from typing import TYPE_CHECKING, Any

from typing_extensions import Literal

from pydantic import BaseModel, model_serializer


class Model(BaseModel):
    x: str

    @model_serializer
    def ser_model(self) -> str:
        return self.x

    if TYPE_CHECKING:
        # Ensure type checkers see the correct return type
        def model_dump(
            self,
            *,
            mode: Literal['json', 'python'] | str = 'python',
            include: Any = None,
            exclude: Any = None,
            by_alias: bool = False,
            exclude_unset: bool = False,
            exclude_defaults: bool = False,
            exclude_none: bool = False,
            round_trip: bool = False,
            warnings: bool = True,
        ) -> str:
            ...
```

이 트릭은 실제로 [`RootModel`](https://docs.pydantic.dev/latest/concepts/models/#rootmodel-and-custom-root-types)에서 정확히 이러한 목적으로 사용된다.

## 서브클래스 직렬화

### 표준 타입의 서브클래스
표준 타입의 서브클래스는 슈퍼클래스와 마찬가지로 자동으로 덤프된다.

```python
from datetime import date, timedelta
from typing import Any, Type

from pydantic_core import core_schema

from pydantic import BaseModel, GetCoreSchemaHandler


class DayThisYear(date):
    """
    Contrived example of a special type of date that
    takes an int and interprets it as a day in the current year
    """

    @classmethod
    def __get_pydantic_core_schema__(
        cls, source: Type[Any], handler: GetCoreSchemaHandler
    ) -> core_schema.CoreSchema:
        return core_schema.no_info_after_validator_function(
            cls.validate,
            core_schema.int_schema(),
            serialization=core_schema.format_ser_schema('%Y-%m-%d'),
        )

    @classmethod
    def validate(cls, v: int):
        return date.today().replace(month=1, day=1) + timedelta(days=v)


class FooModel(BaseModel):
    date: DayThisYear


m = FooModel(date=300)
print(m.model_dump_json())
#> {"date":"2023-10-28"}
```



### `BaseModel`, dataclasses, `TypedDict`의 필드에 대한 서브 클래스 인스턴스
어노테이션 자체가 구조체와 유사한 타입(예: `BaseModel` 서브클래스, 데이터 클래스 등)인 필드를 사용하는 경우 기본 동작은 어노테이션이 서브클래스인 경우에도 어노테이션이 있는 타입의 인스턴스인 것처럼 어트리뷰트 값을 직렬화하는 것이다. 보다 구체적으로, *어노태이트된* 타입의 필드만 덤프된 객체에 포함된다.

```python
from pydantic import BaseModel


class User(BaseModel):
    name: str


class UserLogin(User):
    password: str


class OuterModel(BaseModel):
    user: User


user = UserLogin(name='pydantic', password='hunter2')

m = OuterModel(user=user)
print(m)
#> user=UserLogin(name='pydantic', password='hunter2')
print(m.model_dump())  # note: the password field is not included
#> {'user': {'name': 'pydantic'}}
```

### 덕 타이핑으로 직렬화
이전 덕 타이핑 직렬화 동작을 유지하려면 `SerializeAsAny`를 사용하여 수행하면 된다.

```python
from pydantic import BaseModel, SerializeAsAny


class User(BaseModel):
    name: str


class UserLogin(User):
    password: str


class OuterModel(BaseModel):
    as_any: SerializeAsAny[User]
    as_user: User


user = UserLogin(name='pydantic', password='password')

print(OuterModel(as_any=user, as_user=user).model_dump())
"""
{
    'as_any': {'name': 'pydantic', 'password': 'password'},
    'as_user': {'name': 'pydantic'},
}
"""
```

필드가 `SerializeAsAny[<SomeType>]`로 어노테이트된 경우 유효성 검사 동작은 `<SomeType>`으로 어노테이트된 경우와 동일하며, mypy와 같은 유형 검사기는 해당 속성에도 적절한 타입이 있는 것으로 간주한다. 그러나 직렬화할 때는 필드의 타입 힌트가 이름에서 유래한 `Any`인 것처럼 필드가 직렬화된다.

## `pickle.dumps(model)`
Pydantic 모델은 효율적인 pickling과 unpickling을 지원한다.

```python
# TODO need to get pickling to work
import pickle

from pydantic import BaseModel


class FooBarModel(BaseModel):
    a: str
    b: int


m = FooBarModel(a='hello', b=123)
print(m)
#> a='hello' b=123
data = pickle.dumps(m)
print(data[:20])
#> b'\x80\x04\x95\x95\x00\x00\x00\x00\x00\x00\x00\x8c\x08__main_'
m2 = pickle.loads(data)
print(m2)
#> a='hello' b=123
```

## 고급 include와 exclude
`model_dump`와 `model_dump-json` 메서드는 집합 또는 딕셔너리일 수 있는 인수를 `include`와 `exclude`할 수 있다. 이를 통해 내보낼(export) 필드를 중첩하여 선택할 수 있다.

```python
from pydantic import BaseModel, SecretStr


class User(BaseModel):
    id: int
    username: str
    password: SecretStr


class Transaction(BaseModel):
    id: str
    user: User
    value: int


t = Transaction(
    id='1234567890',
    user=User(id=42, username='JohnDoe', password='hashedpassword'),
    value=9876543210,
)

# using a set:
print(t.model_dump(exclude={'user', 'value'}))
#> {'id': '1234567890'}

# using a dict:
print(t.model_dump(exclude={'user': {'username', 'password'}, 'value': True}))
#> {'id': '1234567890', 'user': {'id': 42}}

print(t.model_dump(include={'id': True, 'user': {'id'}}))
#> {'id': '1234567890', 'user': {'id': 42}}
```

`True`는 집합에 포함시킨 것처럼 전체 키를 제외(exclude)하거나 포함(include)시키려는 것을 나타낸다. 이 작업은 모든 깊이 수준에서 수행할 수 있다.

하위 모델 또는 딕셔너리의 리스트나 튜플에서 필드를 포함하거나 제외할 때는 특별한 주의를 기울여야 한다. 이 시나리오에서 `model_dump`와 관련 메서드는 요소별 포함 또는 제외를 위해 정수 키를 기대한다. 리스트 또는 튜플의 **모든** 멤버에서 필드를 제외하려면 여기에 표시된 것처럼 딕셔너리 키 `'__all__'`을 사용할 수 있다.

```python
import datetime
from typing import List

from pydantic import BaseModel, SecretStr


class Country(BaseModel):
    name: str
    phone_code: int


class Address(BaseModel):
    post_code: int
    country: Country


class CardDetails(BaseModel):
    number: SecretStr
    expires: datetime.date


class Hobby(BaseModel):
    name: str
    info: str


class User(BaseModel):
    first_name: str
    second_name: str
    address: Address
    card_details: CardDetails
    hobbies: List[Hobby]


user = User(
    first_name='John',
    second_name='Doe',
    address=Address(
        post_code=123456, country=Country(name='USA', phone_code=1)
    ),
    card_details=CardDetails(
        number='4212934504460000', expires=datetime.date(2020, 5, 1)
    ),
    hobbies=[
        Hobby(name='Programming', info='Writing code and stuff'),
        Hobby(name='Gaming', info='Hell Yeah!!!'),
    ],
)

exclude_keys = {
    'second_name': True,
    'address': {'post_code': True, 'country': {'phone_code'}},
    'card_details': True,
    # You can exclude fields from specific members of a tuple/list by index:
    'hobbies': {-1: {'info'}},
}

include_keys = {
    'first_name': True,
    'address': {'country': {'name'}},
    'hobbies': {0: True, -1: {'name'}},
}

# would be the same as user.model_dump(exclude=exclude_keys) in this case:
print(user.model_dump(include=include_keys))
"""
{
    'first_name': 'John',
    'address': {'country': {'name': 'USA'}},
    'hobbies': [
        {'name': 'Programming', 'info': 'Writing code and stuff'},
        {'name': 'Gaming'},
    ],
}
"""

# To exclude a field from all members of a nested list or tuple, use "__all__":
print(user.model_dump(exclude={'hobbies': {'__all__': {'info'}}}))
"""
{
    'first_name': 'John',
    'second_name': 'Doe',
    'address': {
        'post_code': 123456,
        'country': {'name': 'USA', 'phone_code': 1},
    },
    'card_details': {
        'number': SecretStr('**********'),
        'expires': datetime.date(2020, 5, 1),
    },
    'hobbies': [{'name': 'Programming'}, {'name': 'Gaming'}],
}
"""
```

`model_dump_json` 메서드도 마찬가지이다.

### Model- 및 field-level include와 exclude
`model_dump`와 `model_dump_json` 메서드에 전달되는 명시적 인자 `exclude`와 `include` 외에도, `exclude: bool` 인자를 `Field` 생성자에 직접 전달할 수도 있다.

필드 생성자에서 `exclude`를 설정하면(`Field(..., exclude=True)`) `model_dump`와 `model_dump_json`의 `exclude`/`include`보다 우선적으로 적용된다.

```python
from pydantic import BaseModel, Field, SecretStr


class User(BaseModel):
    id: int
    username: str
    password: SecretStr = Field(..., exclude=True)


class Transaction(BaseModel):
    id: str
    value: int = Field(exclude=True)


t = Transaction(
    id='1234567890',
    value=9876543210,
)

print(t.model_dump())
#> {'id': '1234567890'}
print(t.model_dump(include={'id': True, 'value': True}))  
#> {'id': '1234567890'}
```

즉, 필드 생성자(`Field(..., exclude=True)`)에서 `exclude`를 설정해도 `model_dump`와 `model_dump_json`에서 `exclude_unset`, `exclude_none` 및 `exclude_default` 매개변수보다 우선순위를 갖지 않는다.

**Python 3.7 and above**

```python
from typing import Optional

from pydantic import BaseModel, Field


class Person(BaseModel):
    name: str
    age: Optional[int] = Field(None, exclude=False)


person = Person(name='Jeremy')

print(person.model_dump())
#> {'name': 'Jeremy', 'age': None}
print(person.model_dump(exclude_none=True))  
#> {'name': 'Jeremy'}
print(person.model_dump(exclude_unset=True))  
#> {'name': 'Jeremy'}
print(person.model_dump(exclude_defaults=True))  
#> {'name': 'Jeremy'}
```

**Python 3.10 and above**

```python
from pydantic import BaseModel, Field


class Person(BaseModel):
    name: str
    age: int | None = Field(None, exclude=False)


person = Person(name='Jeremy')

print(person.model_dump())
#> {'name': 'Jeremy', 'age': None}
print(person.model_dump(exclude_none=True))  
#> {'name': 'Jeremy'}
print(person.model_dump(exclude_unset=True))  
#> {'name': 'Jeremy'}
print(person.model_dump(exclude_defaults=True))  
#> {'name': 'Jeremy'}
```

## `model_copy(...)`

> **API Documentation**
>
> [`pydantic.main.BaseModel.model_copy`](https://docs.pydantic.dev/latest/api/base_model/#pydantic.main.BaseModel.model_copy)

`model_copy()`를 사용하면 모델을 복제(선택적 업데이트 포함)할 수 있으므로 고정된 모델로 작업할 때 특히 유용하다.

예 ...

```python
from pydantic import BaseModel


class BarModel(BaseModel):
    whatever: int


class FooBarModel(BaseModel):
    banana: float
    foo: str
    bar: BarModel


m = FooBarModel(banana=3.14, foo='hello', bar={'whatever': 123})

print(m.model_copy(update={'banana': 0}))
#> banana=0 foo='hello' bar=BarModel(whatever=123)
print(id(m.bar) == id(m.model_copy().bar))
#> True
# normal copy gives the same object reference for bar
print(id(m.bar) == id(m.model_copy(deep=True).bar))
#> False
# deep copy gives a new object reference for `bar`
```
