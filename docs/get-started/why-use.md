오늘날 Pydantic은 한 달에 1억 8백만 번 다운로드되며, 세계에서 가장 크고 유명한 조직에서 사용하고 있다.

6년 전 처음 출시된 이래로 많은 사람들이 Pydantic을 채택한 이유를 알기는 어렵지만, 몇 가지 추측을 해볼 수 있다.

## 스키마 유효성 검사를 지원하는 타입 힌트
Pydantic이 유효성을 검사하는 스키마는 일반적으로 Python 타입 힌트로 정의된다.

타입 힌트는 최신 Python을 작성하는 경우 이미 사용법을 알고 있기 때문에 이 작업에 매우 유용하다. 또한 타입 힌트를 사용한다는 것은 Pydantic이 mypy, pyright같은 정적 타이핑 도구와 pycharm과 vscode같은 IDE와 잘 통합된다는 것을 의미한다.

**Example - just type hints**

`(This example requires Python 3.9+)`

```python
from typing import Annotated, Dict, List, Literal, Tuple

from annotated_types import Gt

from pydantic import BaseModel


class Fruit(BaseModel):
    name: str  
    color: Literal['red', 'green']  
    weight: Annotated[float, Gt(0)]  
    bazam: Dict[str, List[Tuple[int, bool, float]]]  


print(
    Fruit(
        name='Apple',
        color='red',
        weight=4.2,
        bazam={'foobar': [(1, True, 0.1)]},
    )
)
#> name='Apple' color='red' weight=4.2 bazam={'foobar': [(1, True, 0.1)]}
```

> **자세히 알아보기**
>
> [documentation on supported types](../concepts/types.md)를 찾아 보자.


## 성능
Pydantic의 핵심 유효성 검사 로직은 대부분의 타입에 대한 유효성 검사는 Rust로 구현되는 별도의 패키지 [pydantic-core](https://github.com/pydantic/pydantic-core)에 구현되어 있다.

그 결과 Pydantic은 Python을 위한 가장 빠른 데이터 유효성 검사 라이브러리 중 하나이다.

**Performance Example - Pydantic vs. dedicated code**

```python
import json
import timeit
from urllib.parse import urlparse

import requests

from pydantic import HttpUrl, TypeAdapter

reps = 7
number = 100
r = requests.get('https://api.github.com/emojis')
r.raise_for_status()
emojis_json = r.content


def emojis_pure_python(raw_data):
    data = json.loads(raw_data)
    output = {}
    for key, value in data.items():
        assert isinstance(key, str)
        url = urlparse(value)
        assert url.scheme in ('https', 'http')
        output[key] = url


emojis_pure_python_times = timeit.repeat(
    'emojis_pure_python(emojis_json)',
    globals={
        'emojis_pure_python': emojis_pure_python,
        'emojis_json': emojis_json,
    },
    repeat=reps,
    number=number,
)
print(f'pure python: {min(emojis_pure_python_times) / number * 1000:0.2f}ms')
#> pure python: 5.32ms

type_adapter = TypeAdapter(dict[str, HttpUrl])
emojis_pydantic_times = timeit.repeat(
    'type_adapter.validate_json(emojis_json)',
    globals={
        'type_adapter': type_adapter,
        'HttpUrl': HttpUrl,
        'emojis_json': emojis_json,
    },
    repeat=reps,
    number=number,
)
print(f'pydantic: {min(emojis_pydantic_times) / number * 1000:0.2f}ms')
#> pydantic: 1.54ms

print(
    f'Pydantic {min(emojis_pure_python_times) / min(emojis_pydantic_times):0.2f}x faster'
)
#> Pydantic 3.45x faster
```

컴파일된 언어로 작성된 다른 성능 중심 라이브러리와 달리 Pydantic은 [기능적 유효성 검사기(functional validator)](./why-use.md#커스터마이제이션)를 통해 유효성 검사를 커스터마이할 수 있는 뛰어난 지원도 제공한다.

> **자세히 알아보기**
>
> [Pycon 2023에서 Samuel Colvin의 강연](https://youtu.be/pWZw7hYoRVU)은 `pydantic-core`가 어떻게 작동하고 Pydantic과 어떻게 통합되는지 설명하고 있다.

## 직렬화
Pydantic은 세 가지 방법으로 모델을 직렬화하는 기능을 제공한다.

- 연관된 Python 객체로 구성된 Python `dict`로
- "jsonable" 타입으로만 구성된 Python `dict`로
- JSON 문자열로

세 가지 모드 모두에서 특정 필드 제외, 설정되지 않은 필드 제외, 기본값 제외, `None` 값 제외를 통해 출력을 커스터마이징할 수 있다.

**Example - Serialization 3 ways**

```python
from datetime import datetime

from pydantic import BaseModel


class Meeting(BaseModel):
    when: datetime
    where: bytes
    why: str = 'No idea'


m = Meeting(when='2020-01-01T12:00', where='home')
print(m.model_dump(exclude_unset=True))
#> {'when': datetime.datetime(2020, 1, 1, 12, 0), 'where': b'home'}
print(m.model_dump(exclude={'where'}, mode='json'))
#> {'when': '2020-01-01T12:00:00', 'why': 'No idea'}
print(m.model_dump_json(exclude_defaults=True))
#> {"when":"2020-01-01T12:00:00","where":"home"}
```

> **자세히 알아보기**
>
> [documentation on serialization](https://docs.pydantic.dev/latest/concepts/serialization/)을 찾아 보자.

## JSON 스키마
모든 Pydantic 스키마에 대해 [JSON 스키마](https://json-schema.org/)를 생성할 수 있으므로 자체 문서화 API와 JSON 스키마를 지원하는 다양한 도구와의 통합이 가능하다.

**Example - JSON Schema**

```python
from datetime import datetime

from pydantic import BaseModel


class Address(BaseModel):
    street: str
    city: str
    zipcode: str


class Meeting(BaseModel):
    when: datetime
    where: Address
    why: str = 'No idea'


print(Meeting.model_json_schema())
"""
{
    '$defs': {
        'Address': {
            'properties': {
                'street': {'title': 'Street', 'type': 'string'},
                'city': {'title': 'City', 'type': 'string'},
                'zipcode': {'title': 'Zipcode', 'type': 'string'},
            },
            'required': ['street', 'city', 'zipcode'],
            'title': 'Address',
            'type': 'object',
        }
    },
    'properties': {
        'when': {'format': 'date-time', 'title': 'When', 'type': 'string'},
        'where': {'$ref': '#/$defs/Address'},
        'why': {'default': 'No idea', 'title': 'Why', 'type': 'string'},
    },
    'required': ['when', 'where'],
    'title': 'Meeting',
    'type': 'object',
}
"""
```

Pydantic은 [OpenAPI 3.1](https://www.openapis.org/blog/2021/02/18/openapi-specification-3-1-released)과 호환되는 표준의 최신 버전인 [JSON 스키마 버전 2020-12](https://json-schema.org/draft/2020-12/release-notes.html)를 생성한다.

> **자세히 알아보기**
>
> [documentation on JSON Schema](https://docs.pydantic.dev/latest/concepts/json_schema/)을 찾아 보자.

## 엄격한 모드와 데이터 강요(coercion)
기본적으로 Pydantic은 일반적인 잘못된 타입에 대해 관대하며 데이터를 올바른 타입으로 강제로 변환한다. 예를 들면, `int` 필드에 전달된 숫자 문자열을 `int`로 구문 분석한다.

Pydantic에는 입력 데이터가 스키마 또는 타입 힌트와 정확히 일치하지 않는 한 타입을 강제하지 않고 유효성 검사 오류를 발생시키는 "엄격한(Strict) 모드"라고도 하는 `strict=True` 모드도 있다.

그러나 JSON에는 `datetime`, `UUID` 또는 `bytes`와 같은 많은 일반적인 Python 타입과 일치하는 유형이 없기 때문에 엄격한 모드는 JSON 데이터의 유효성을 검사할 때 다소 쓸모가 없다.

이 문제를 해결하기 위해 Pydantic은 JSON을 한 번에 파싱하고 유효성 검사를 할 수 있다. 이를 통해 RFC3339(일명 ISO8601) 문자열과 같은 데이터를 `datetime` 객체로 현명하게 변환할 수 있다. JSON 구문 분석은 Rust로 구현되었으므로 성능도 매우 뛰어나다.

**Example - Strict mode that's actually useful**

```python
from datetime import datetime

from pydantic import BaseModel, ValidationError


class Meeting(BaseModel):
    when: datetime
    where: bytes


m = Meeting.model_validate({'when': '2020-01-01T12:00', 'where': 'home'})
print(m)
#> when=datetime.datetime(2020, 1, 1, 12, 0) where=b'home'
try:
    m = Meeting.model_validate(
        {'when': '2020-01-01T12:00', 'where': 'home'}, strict=True
    )
except ValidationError as e:
    print(e)
    """
    2 validation errors for Meeting
    when
      Input should be a valid datetime [type=datetime_type, input_value='2020-01-01T12:00', input_type=str]
    where
      Input should be a valid bytes [type=bytes_type, input_value='home', input_type=str]
    """

m_json = Meeting.model_validate_json(
    '{"when": "2020-01-01T12:00", "where": "home"}'
)
print(m_json)
#> when=datetime.datetime(2020, 1, 1, 12, 0) where=b'home'
```

> **자세히 알아보기**
>
> [documentation on strict mode](https://docs.pydantic.dev/latest/concepts/strict_mode/)을 찾아 보자.

## Dataclasses, TypedDicts 등
Pydantic은 스키마를 생성하고 유효성 검사와 직렬화를 수행하는 네 가지 방법을 제공한다.

1. [BaseModel](https://docs.pydantic.dev/latest/concepts/models/) - 인스턴스 메서드를 통해 사용할 수 있는 많은 공통 유틸리티가 포함된 Pydantic 자체의 슈퍼 클래스.
2. [pydantic.dataclasses.dataclass](https://docs.pydantic.dev/latest/concepts/dataclasses/) - 데이터 클래스가 초기화될 때 유효성 검사를 수행하는 표준 데이터 클래스를 둘러싼 래퍼이다.
3. [TypeAdapter](https://docs.pydantic.dev/latest/api/type_adapter/#pydantic.type_adapter.TypeAdapter) - 유효성 검사와 직렬화를 위해 모든 타입을 조정하는 일반적인 방법이다. 이를 통해 [TypedDict](https://docs.pydantic.dev/latest/api/standard_library_types/#typeddict)과 [NampedTuple](https://docs.pydantic.dev/latest/api/standard_library_types/#namedtuple)같은 타입은 물론 `int` 또는 `timedelta`와 같은 간단한 스칼라 값도 유효성을 검사할 수 있으며, 지원되는 [모든 타입](https://docs.pydantic.dev/latest/concepts/types/)은 `TypeAdapter`와 함께 사용할 수 있다.
4. [validate_call](https://docs.pydantic.dev/latest/concepts/validation_decorator/) - 함수를 호출할 때 유효성 검사를 수행하는 데코레이터이다.

**Example - schema based on TypedDict**

```python
from datetime import datetime

from typing_extensions import NotRequired, TypedDict

from pydantic import TypeAdapter


class Meeting(TypedDict):
    when: datetime
    where: bytes
    why: NotRequired[str]


meeting_adapter = TypeAdapter(Meeting)
m = meeting_adapter.validate_python(  
    {'when': '2020-01-01T12:00', 'where': 'home'}
)
print(m)
#> {'when': datetime.datetime(2020, 1, 1, 12, 0), 'where': b'home'}
meeting_adapter.dump_python(m, exclude={'where'})  

print(meeting_adapter.json_schema())  
"""
{
    'properties': {
        'when': {'format': 'date-time', 'title': 'When', 'type': 'string'},
        'where': {'format': 'binary', 'title': 'Where', 'type': 'string'},
        'why': {'title': 'Why', 'type': 'string'},
    },
    'required': ['when', 'where'],
    'title': 'Meeting',
    'type': 'object',
}
"""
```

## 커스터마이제이션
기능적 유효성 검사기와 직렬화기뿐만아니라 커스텀 타입을 위한 강력한 프로토콜을 통해 Pydantic의 작동 방식을 필드별 또는 타입별로 커스터마이징할 수 있다.

**Customisation Example - wrap validators**

```python
from datetime import datetime, timezone

from pydantic import BaseModel, field_validator


class Meeting(BaseModel):
    when: datetime

    @field_validator('when', mode='wrap')
    def when_now(cls, input_value, handler):
        if input_value == 'now':
            return datetime.now()
        when = handler(input_value)
        # in this specific application we know tz naive datetimes are in UTC
        if when.tzinfo is None:
            when = when.replace(tzinfo=timezone.utc)
        return when


print(Meeting(when='2020-01-01T12:00+01:00'))
#> when=datetime.datetime(2020, 1, 1, 12, 0, tzinfo=TzInfo(+01:00))
print(Meeting(when='now'))
#> when=datetime.datetime(2032, 1, 2, 3, 4, 5, 6)
print(Meeting(when='2020-01-01T12:00'))
#> when=datetime.datetime(2020, 1, 1, 12, 0, tzinfo=datetime.timezone.utc)
```

> **자세히 알아보기**
>
> [validators](https://docs.pydantic.dev/latest/concepts/validators/), [custom serialization](https://docs.pydantic.dev/latest/concepts/serialization/#custom-serializers)과 [custom types](https://docs.pydantic.dev/latest/concepts/types/#custom-types)에 대한 문서를 찾아 보자.

## 에코시스템
이 글을 쓰는 시점에 GitHub에는 214,100개의 리포지토리가, PyPI에는 8,119개의 패키지가 Pydantic에 의존하고 있다.

[자세히 알아보기](https://docs.pydantic.dev/latest/why/#ecosystem)

## Pydantic을 사용하는 조직
Pydantic을 사용하는 주목할 만한 기업과 조직들이 Pydantic을 사용하는 이유/방법에 대한 의견을 함께 소개한다.

[자세히 알아보기](https://docs.pydantic.dev/latest/why/#using-pydantic)
