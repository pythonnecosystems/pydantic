Pydantic은 Python에서 가장 널리 사용되는 데이터 유효성 검사 라이브러리입니다.

빠르고 확장 가능한 Pydantic은 linter/IDE/brain과 잘 연동된다. 순수한 표준 Python 3.7 이상에서 데이터가 어떻게 구성되어야 하는지 정의하고, Pydantic으로 유효성을 검사하자.

**Pydantic Example**

```python
from datetime import datetime
from typing import Tuple

from pydantic import BaseModel


class Delivery(BaseModel):
    timestamp: datetime
    dimensions: Tuple[int, int]


m = Delivery(timestamp='2020-01-02T03:04:05Z', dimensions=['10', '20'])
print(repr(m.timestamp))
#> datetime.datetime(2020, 1, 2, 3, 4, 5, tzinfo=TzInfo(UTC))
print(m.dimensions)
#> (10, 20)
```

## Pydantic을 사용하는 이유
- **타입 힌트 기반** - Pydantic을 사용하면 스키마 유효성 검사와 직렬화가 타입 어노테이션으로 제어되므로 알아야 할 것이 줄고, 작성할 코드가 적으며, IDE와 정적 분석 도구와 통합할 수 있다. [자세히 알아보기...](./why-use.md#스키마-유효성-검사를-지원하는-타입-힌트)
- **속도** - Pydantic의 핵심 유효성 검사 로직은 Rust로 작성되었다. 그 결과, Pydantic은 Python용 데이터 유효성 검사 라이브러리 중 가장 빠른 속도를 자랑한다. [자세히 알아보기...](./why-use.md#성능)
- **JSON 스키마** - Pydantic 모델을 JSON 스키마로 방출할 수 있어 다른 도구와 쉽게 통합할 수 있다. [자세히 알아보기...](./why-use.md#json-스키마)
- **엄격한** 모드와 **느슨한** 모드 - Pydantic은 데이터가 변환되지 않는 `strict=True` 모드 또는 적절한 경우 데이터를 정확한 타입으로 강제로 변환하는 `strict=False` 모드에서 실행할 수 있다. [자세히 알아보기...](./why-use.md#엄격한-모드와-데이터-강요coercion)
- **Dataclasses**, **TypedDict** 등 - Pydantic은 `dataclass`와 `TypedDict`를 비롯한 다양한 표준 라이브러리 타입의 유효성 검사를 지원한다. [자세히 알아보기...](./why-use.md#dataclasses-typeddicts-등)
- **커스터마이제이션** - Pydantic을 사용하면 커스텀 유효성 검사기와 직렬화기가 여러 가지 강력한 방법으로 데이터 처리 방식을 변경할 수 있다. [자세히 알아보기...](./why-use.md#커스터마이제이션)
- **에코시스템** - *FastAPI*, *huggingface*, *Django Ninja*, *SQLModel*, *LangChain* 등과 같이 널리 사용되는 라이브러리를 포함하여 PyPI의 약 8,000개 패키지가 Pydantic을 사용한다. [자세히 알아보기...](./why-use.md#에코시스템)
- **실전 테스트 완료** - Pydantic은 매월 7천만 회 이상 다운로드되었으며, 모든 FAANG 기업과 NASDAQ 25대 기업 중 20개 기업이 사용하고 있다. Pydantic으로 무언가를 시도하고 있다면 다른 누군가가 이미 해봤을 것이다. [자세히 알아보기...](./why-use.md#pydantic을-사용하는-조직)

[Pydantic을 설치하는 방법](./installation.md)은 다음과 같이 간단하다: `pip install pydantic`

## Pydantic 예
Pydantic이 작동하는 모습을 보려면 간단한 예에서 시작하여 `BaseModel`에서 상속받은 커스텀 클래스까지 만들어 보겠다.

**Validation Successful**

```python
from datetime import datetime

from pydantic import BaseModel, PositiveInt


class User(BaseModel):
    id: int  
    name: str = 'John Doe'  
    signup_ts: datetime | None  
    tastes: dict[str, PositiveInt]  


external_data = {
    'id': 123,
    'signup_ts': '2019-06-01 12:22',  
    'tastes': {
        'wine': 9,
        b'cheese': 7,  
        'cabbage': '1',  
    },
}

user = User(**external_data)  

print(user.id)  
#> 123
print(user.model_dump())  
"""
{
    'id': 123,
    'name': 'John Doe',
    'signup_ts': datetime.datetime(2019, 6, 1, 12, 22),
    'tastes': {'wine': 9, 'cheese': 7, 'cabbage': 1},
}
"""
```

유효성 검사를 실패하면 Pydantic은 무엇이 잘못되었는지에 대한 분석과 함께 오류를 발생시킨다.

**Validation Error**

```python
# continuing the above example...

from pydantic import ValidationError


class User(BaseModel):
    id: int
    name: str = 'John Doe'
    signup_ts: datetime | None
    tastes: dict[str, PositiveInt]


external_data = {'id': 'not an int', 'tastes': {}}  

try:
    User(**external_data)  
except ValidationError as e:
    print(e.errors())
    """
    [
        {
            'type': 'int_parsing',
            'loc': ('id',),
            'msg': 'Input should be a valid integer, unable to parse string as an integer',
            'input': 'not an int',
            'url': 'https://errors.pydantic.dev/2/v/int_parsing',
        },
        {
            'type': 'missing',
            'loc': ('signup_ts',),
            'msg': 'Field required',
            'input': {'id': 'not an int', 'tastes': {}},
            'url': 'https://errors.pydantic.dev/2/v/missing',
        },
    ]
    """
```

## Pydantic 사용자
수백 개의 조직과 패키지가 Pydantic을 사용하고 있다. Adobe, Amazon, Apple, ASML 등 전 세계 유명 기업과 조직이 Pydantic을 사용하고 있다.

Pydantic을 사용하는 오픈 소스 프로젝트의 보다 포괄적인 목록은 [github의 종속성 리스트](https://github.com/pydantic/pydantic/network/dependents)를 참조하거나 [awesome-pydantic](https://github.com/Kludex/awesome-pydantic)에서 Pydantic을 사용하는 멋진 프로젝트를 찾아 볼 수 있다.
