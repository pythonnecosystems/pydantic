설치는 매우 간단하다.

```bash
$ pip install pydantic
```

Pydantic에는 몇 가지 종속성이 있다.

- [`pydantic-core`](https://pypi.org/project/pydantic-core/): rust로 작성된 *pydantic*의 핵심 유효성 검사 로직.
- [`typing-extension`](https://pypi.org/project/typing-extensions/): 표준 라이브러리 `typing` 모듈의 백포트.
- [`annotated-types`](https://pypi.org/project/annotated-types/): `typing.Annotated`와 함께 사용할 재사용 가능한 제약 조건 타입.

Python 3.7 이상과 `pip`이 설치되어 있다면 바로 사용할 수 있다.

Pydantic은 [conda](https://www.anaconda.com/)에서도 [conda-forge](https://conda-forge.org/) 채널에서 사용할 수 있다.

```bash
$ conda install pydantic -c conda-forge
```

## 선택적 종속성 (Optional Dependencies)
Pydantic에는 다음과 같은 선택적 종속성이 있다.

- 이메일 유효성 검사가 필요한 경우 [이메일 유효성 검사기](https://github.com/JoshData/python-email-validator)를 추가할 수 있다.

Pydantic과 함께 선택적 종속성을 설치하려면,

```bash
pip install pydantic[email]
```

물론 `pip install email-validator`를 사용하여 요구 사항을 수동으로 설치할 수도 있다.

## 리포지터리로부터 설치
리포지토리에서 직접 Pydantic을 설치하는 것을 선호한다면,

```bash
pip install git+https://github.com/pydantic/pydantic@main#egg=pydantic
# or with extras
pip install git+https://github.com/pydantic/pydantic@main#egg=pydantic[email]
```
