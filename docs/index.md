# Welcome to Pydantic 페이지 오신 것을 환영합니다.

이 페이지는 [Pydantic](https://docs.pydantic.dev/latest/)을 편역한 것으로 Python의 Pydantic 패키지를 소개하기 위함이다.

## 차례

### 출발
- [소개](./get-started/welcome.md)
- [동기](./get-started/why-use.md)
- [설치](./get-started/installation.md)

### 개념
- [모델](./concepts/models.md)
- [필드](./concepts/field.md)
- [JSON 스키마](./concepts/json-schema.md)
- [JSON](./concepts/json.md)
- [타입](./concepts/types.md)
- [Unions](./concepts/unions.md)
- [Alias](./concepts/alias.md)
- [구성](./concepts/configuration.md)
- [직렬화](./concepts/serialization.md)
- [유효성 검사기](./concepts/validators.md)
- [Dataclasses](./concepts/dataclasses.md)
- [연기된 어노테이션](./concepts/postponed-annotation.md)
- [엄격 모드](./concepts/strict-mode.md)
- [타입 어댑터](./concepts//type-adapter.md)
- [유효성 검사 데코레이터](./concepts/validation-decorator.md)
- [변환 테이블](./concepts/conversion-table.md)
- [설정 관리](./concepts/settings-management.md)
- [성능](./concepts/performance.md)
- [Pydantic 플러그인](./concepts/pydantic-plugins.md)

### 예
- [Secrets](./examples/secrets.md)

### 오류 메시지

### 연동
- [Mypy](./integrations/mypy.md)

